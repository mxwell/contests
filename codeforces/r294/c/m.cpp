#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n, m;
  cin >> n >> m;
  int ans = 0;
  while (true) {
    if (n > m)
      swap(n, m);
    int c = min(n, m / 2);
    if (c == 0)
      break;
    ans += 1;
    n -= 1;
    m -= 2;
  }
  cout << ans << endl;
  return 0;
}
