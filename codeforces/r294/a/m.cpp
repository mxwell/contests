#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

bool F(const vector<pair<char, int>> &prices, char c, int &price) {
  int ch = (int) toupper(int(c));
  price = 0;
  for (auto p : prices) {
    if (p.first == ch) {
      price = p.second;
      return isupper(int(c));
    }
  }
  return true;
}

int main() {
  vector<pair<char, int> > prices = {
    {'Q', 9},
    {'R', 5},
    {'B', 3},
    {'N', 3},
    {'P', 1}
  };

  int x = 0, y = 0;
  for (int it = 0; it < 8; ++it) {
    string s;
    getline(cin, s);
    for (char c : s) {
      int price;
      if (F(prices, c, price)) {
        x += price;
      } else {
        y += price;
      }
    }
  }
  if (x > y)
    puts("White");
  else if (x == y)
    puts("Draw");
  else
    puts("Black");

  return 0;
}
