#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

vector<int> g[N];
int par[N][18];
int tin[N];
int tout[N];
int lh;
int wg[N];
int dep[N];

void Dfs(int v, int prev, int &timer, int depth) {
  ++timer;
  tin[v] = timer;
  par[v][0] = prev;
  for (int i = 1; i <= lh; ++i) {
    par[v][i] = par[par[v][i - 1]][i - 1];
  }
  wg[v] = 1;
  dep[v] = depth;
  for (int to : g[v]) {
    if (to != prev) {
      Dfs(to, v, timer, depth + 1);
      wg[v] += wg[to];
    }
  }
  ++timer;
  tout[v] = timer;
}

inline bool IsUpper(int a, int b) {
  return tin[a] <= tin[b] && tout[b] <= tout[a];
}

inline int GetLca(int a, int b) {
  if (IsUpper(a, b))
    return a;
  if (IsUpper(b, a))
    return b;
  for (int i = lh; i >= 0; --i) {
    if (!IsUpper(par[a][i], b))
      a = par[a][i];
  }
  return par[a][0];
}

int GoUp(int from, int len) {
  int r = 0;
  int v = from;
  while (len) {
    if (len & 1) {
      v = par[v][r];
    }
    len >>= 1;
    ++r;
  }
  return v;
}

int HalfUp(int from, int len) {
  if (len & 1)
    return 0;
  int u = GoUp(from, len / 2 - 1);
  int v = par[u][0];
  return wg[v] - wg[u];
}

int Calc(int x, int y, int n) {
  if (x == y)
    return n;
  if (IsUpper(x, y))
    return HalfUp(y, dep[y] - dep[x]);
  if (IsUpper(y, x))
    return HalfUp(x, dep[x] - dep[y]);
  int lca = GetLca(x, y);
  if (dep[x] == dep[y]) {
    int u = GoUp(x, dep[x] - dep[lca] - 1);
    int v = GoUp(y, dep[y] - dep[lca] - 1);
    return n - wg[u] - wg[v];
  }
  int len = (dep[x] - dep[lca]) + (dep[y] - dep[lca]);
  int low = dep[x] > dep[y] ? x : y;
  return HalfUp(low, len);
}

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i + 1 < n; ++i) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    g[x].push_back(y);
    g[y].push_back(x);
  }
  lh = 1;
  while ((1 << lh) <= n)
    ++lh;
  int timer = 1;
  Dfs(0, 0, timer, 1);
  int m;
  scanf("%d", &m);
  for (int it = 0; it < m; ++it) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    printf("%d\n", Calc(x, y, n));
  }
  return 0;
}
