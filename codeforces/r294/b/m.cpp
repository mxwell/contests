#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int compare(const vector<int> &a, const vector<int> &b) {
  int n = int(b.size());
  for (int i = 0; i < n; ++i) {
    if (a[i] != b[i])
      return a[i];
  }
  return a[n];
}

int main() {
  int n;
  scanf("%d", &n);
  vector<int> a(n), b(n - 1), c(n - 2);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  for (int i = 0; i < n - 1; ++i) {
    scanf("%d", &b[i]);
  }
  for (int i = 0; i < n - 2; ++i) {
    scanf("%d", &c[i]);
  }
  sort(a.begin(), a.end());
  sort(b.begin(), b.end());
  sort(c.begin(), c.end());
  printf("%d\n", compare(a, b));
  printf("%d\n", compare(b, c));
  return 0;
}
