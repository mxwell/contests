#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

li sa[N];

li Go(const string &s, char ch, int cost) {
  unordered_map<li, int> cnt;
  vector<pair<int, li>> ps;
  int n = int(s.size());
  for (int i = 0; i < n; ++i) {
    if (s[i] == ch) {
      li val = sa[i];
      ps.push_back({i, val});
      if (cnt.count(val) == 0) {
        cnt.insert({val, 1});
      } else {
        ++cnt[val];
      }
    }
  }
  li result = 0;
  for (int i = 0; i < int(ps.size()); ++i) {
    auto &p = ps[i];
    --cnt[p.second];
    result += cnt[p.second + cost];
    //if (i + 1 < int(ps.size()) && ps[i + 1].first == p.first + 1)
    //  --result;
  }
  return result;
}

int main() {
  int cost[26];
  for (int i = 0; i < 26; ++i) {
    scanf("%d", &cost[i]);
  }
  scanf("\n");
  string s;
  getline(cin, s);
  for (char &c : s)
    c = char(c - 'a');
  sa[0] = cost[int(s[0])];
  for (int i = 1; i < int(s.size()); ++i)
    sa[i] = sa[i - 1] + cost[int(s[i])];
  li ans = 0;
  for (char c = 0; c < 26; ++c)
    ans += Go(s, c, cost[int(c)]);
  cout << ans << endl;
  return 0;
}
