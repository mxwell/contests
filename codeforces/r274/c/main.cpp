#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 5100;
const int MODULO = 1000000007;

int d[N][N];
int z[N];

void add(int &to, int val) {
	to = (to + val);
	if (to >= MODULO)
		to -= MODULO;
}

void calc_sums(int step, int n) {
	int s = 0;
	int *src = d[step];
	for1 (i, n) {
		z[i] = s;
		add(s, src[i]);
	}
	z[n + 1] = s;
}

int get_sum(int lf, int rg) {
	int s = z[rg + 1];
	add(s, MODULO - z[lf]);
	return s;
}

int main() {
  	int n, a, b, k;
  	scanf("%d%d%d%d", &n, &a, &b, &k);
  	d[0][a] = 1;
  	calc_sums(0, n);

  	forn(step, k) {
  		int next = step + 1;
  		int pre_b = b - 1;
  		for1 (i, pre_b) {
  			int v = get_sum(1, i - 1);
  			int t = b - i;
  			int rg = i + (t - t / 2) - 1;
  			if (rg > i)
  				add(v, get_sum(i + 1, rg));
  			d[next][i] = v;
  		}
  		for (int i = b + 1; i <= n; ++i) {
  			int v = get_sum(i + 1, n);
  			int t = i - b;
  			int lf = i - (t - t / 2) + 1;
  			if (lf < i)
  				add(v, get_sum(lf, i - 1));
  			d[next][i] = v;
  		}
  		calc_sums(next, n);
  	}
  	int ans = 0;
  	for1 (i, n)
  		add(ans, d[k][i]);
  	printf("%d\n", ans);
    return 0;
}
