#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int n;
int a[N];

bool check(int t) {
	int lf = 0, rg = 0;
	while (rg < n) {
		int right = a[lf] + t;
		while (rg < n && a[rg] < right)
			++rg;
		if (rg < n) {
			if (a[rg] == right)
				return true;
		}
		++lf;
	}
	return false;
}

bool solve(int x, int y) {
	int u = 0, v = 0;
	int len = a[n - 1];
	forn (lf, n) {
		int add = a[lf] + x;
		if (add > len)
			break;
		int left = add - y;
		if (left >= 0) {
			while (u < n && a[u] < left)
				++u;
			if (u < n && a[u] == left) {
				printf("1\n%d\n", add);
				return true;
			}
		}
		if (add <= len && add + y <= len) {
			int right = add + y;
			while (v < n && a[v] < right)
				++v;
			if (v < n && a[v] == right) {
				printf("1\n%d\n", add);
				return true;
			}
		}
	}
	u = n - 1, v = n - 1;
	for (int lf = n - 1; lf >= 0; --lf) {
		int add = a[lf] - x;
		if (add < 0)
			break;
		int left = add - y;
		if (left >= 0) {
			while (u >= 0 && a[u] > left)
				--u;
			if (u >= 0 && a[u] == left) {
				printf("1\n%d\n", add);
				return true;
			}
		}
		int right = add + y;
		if (right <= len) {
			while (v >= 0 && a[v] > right)
				--v;
			if (v >= 0 && a[v] == right) {
				printf("1\n%d\n", add);
				return true;
			}
		}
	}
	return false;
}

int main() {
	int len, x, y;
	scanf("%d%d%d%d", &n, &len, &x, &y);
	forn (i, n)
		scanf("%d", a + i);
	if (check(x)) {
		if (check(y))
			puts("0");
		else
			printf("1\n%d\n", y);
	} else {
		if (check(y))
			printf("1\n%d\n", x);
		else {
			if (!solve(x, y) && !solve(y, x))
				printf("2\n%d %d\n", x, y);
		}
	}

    return 0;
}
