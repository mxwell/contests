#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 510;
const int SMAX = 1010;

struct box {
	int in, out;
	int w, s, v;
	int d[SMAX];
	vector<int> z;
	void init() {
		memset(d, -1, sizeof d);
		z = vector<int>(out - in + 1);
	}
	void read() {
		scanf("%d%d%d%d%d", &in, &out, &w, &s, &v);
		init();
	}
	void clear_z() {
		fill(z.begin(), z.end(), 0);
	}
	int pull_z(int &j, int to) {
		while (j < to) {
			z[j + 1] = max(z[j], z[j + 1]);
			++j;
		}
		return z[j];
	}
	void update_z(int j, int val) {
		z[j] = max(z[j], val);
	}
	bool operator < (const box &other) const {
		if (in == other.in)
			return out > other.out;
		return in < other.in;
	}
};

int n;
box bs[N];

int rec(int id, int s) {
	box &b = bs[id];
	int &result = b.d[s];
	if (result < 0) {
		result = b.v;
		int in = b.in;
		int out = b.out;
		b.clear_z();
		int j = 0;
		for (int i = id + 1; i <= n; ++i) {
			if (bs[i].in >= out)
				break;
			if (bs[i].out > out)
				continue;
			int collected = b.pull_z(j, bs[i].in - in);
			int ns = min(s - bs[i].w, bs[i].s);
			if (ns < 0)
				continue;
			b.update_z(bs[i].out - in, collected + rec(i, ns));
		}
		result = b.v + b.pull_z(j, out - in);
	}
	return result;
}

int main() {
	box &platform = bs[0];
	scanf("%d%d", &n, &platform.s);
	platform.in = 0;
	platform.out = 2 * n;
	platform.w = 0;
	platform.v = 0;
	platform.init();

	for1 (i, n)
		bs[i].read();

	sort(bs, bs + n + 1);

	printf("%d\n", rec(0, platform.s));

    return 0;
}
