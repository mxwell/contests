#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 2000100;
constexpr li MODULO = 1e9 + 7;

char buf[N];
int pi[N];

int main() {
  int n, m;
  scanf("%d%d\n", &n, &m);
  scanf("%s\n", buf);
  int slen = (int) strlen(buf);
  buf[slen] = '#';
  vector<int> vs(m);
  int j = 0;
  for (int i = 0; i < m; ++i) {
    scanf("%d", &vs[i]);
    --vs[i];
    int end = vs[i] + slen;
    for (j = max(j, vs[i]); j < end; ++j)
      buf[slen + 1 + j] = buf[slen - end + j];
  }
  for (int i = 1; i <= n + slen; ++i) {
    j = pi[i - 1];
    while (j > 0 && buf[i] != buf[j])
      j = pi[j - 1];
    if (buf[i] == buf[j])
      ++j;
    pi[i] = j;
  }
  for (int i = 0; i < m; ++i) {
    if (pi[slen + 1 + vs[i] + slen - 1] < slen) {
      puts("0");
      return 0;
    }    
  }
  li mult = 'z' - 'a' + 1;
  li ans = 1;
  for (int i = 0; i < n; ++i) {
    if (!buf[slen + 1 + i]) {
      ans = (ans * mult) % MODULO;
    }
  }
  cout << ans << endl;
  return 0;
}
