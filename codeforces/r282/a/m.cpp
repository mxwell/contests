#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

bool solve(string &s) {
	int b = 0;
	int count = 0;
	for (auto c : s)
		if (c == '(')
			++b;
		else {
			--b;
			if (c == '#')
				++count;
		}
	if (b < 0)
		return false;
	vector<int> ans;
	int disb = b;
	b = 0;
	for (auto c : s) {
		if (c == '(')
			++b;			
		else {
			--b;
			if (c == '#') {
				--count;
				if (count <= 0)
					ans.push_back(1 + disb), b -= disb;
				else
					ans.push_back(1);
			}
		}
		if (b < 0)
			return false;
	}
	for (int i : ans)
		printf("%d\n", i);
	return true;
}

int main() {
	string s;
	getline(cin, s);
	if (!solve(s))
		puts("-1");
    return 0;
}
