#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;
const int MODULO = 1e9 + 7;

inline void add(li &tg, li x) {
	tg = (tg + x) % MODULO;
}

inline void mult(li &tg, li x) {
	tg = (tg * x) % MODULO;
}

int main() {
	string s;
	getline(cin, s);
	int n = (int) s.size();
	string pat;
	getline(cin, pat);
	s = pat + "#" + s;
	vector<int> pos;
	vector<int> pi(s.size(), 0);
	int m = (int) pat.size();
	for (int i = 1; i < int(s.size()); ++i) {
		int j = pi[i - 1];
		while (j > 0 && s[i] != s[j])
			j = pi[j - 1];
		if (s[i] == s[j]) {
			++j;
		}
		pi[i] = j;
		if (j == m)
			pos.push_back(i - 2 * m + 1);
	}
	vector<li> d(n + 1, 0);
	d[0] = 1;
	li ans = 0;
	li presum = 0;
	int top = 0;
	li acc = 0;
	for (int j = 0; j < (int) pos.size(); ++j) {
		int p = pos[j];
		while (top < p) {
			add(presum, d[top++]);
			acc += presum;
		}
		li sum = acc;
		d[p + m - 1] = sum;
		int next = n;
		if (j + 1 < (int) pos.size())
			next = pos[j + 1] + m - 2;
		for (int k = p + m - 1; k <= next; ++k)
			d[k] = sum;
		mult(sum, next - (p + m - 1) + 1);
		add(ans, sum);
	}
	cout << ans << endl;

    return 0;
}
