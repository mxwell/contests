#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 300100;

vector<int> g[N];
int parent[N];
int ranq[N];
int dist[N];
int from[N];
int diam[N];

void MakeSet(int v) {
	parent[v] = v;
	ranq[v] = 0;
}

void AttachNode(int v, int par) {
	parent[v] = par;
	ranq[v] = 0;
}

int FindSet(const int v) {
	int &par = parent[v];
	if (v != par)
		par = FindSet(par);
	return par;
}

void UnionSets(int a, int b) {
	a = FindSet(a);
	b = FindSet(b);
	if (a != b) {
		if (ranq[a] < ranq[b])
			swap(a, b);
		parent[b] = a;
		if (ranq[a] == ranq[b])
			++ranq[a];
	}
}

int GetCenter(int v) {
	queue<int> q;
	dist[v] = 0;
	from[v] = -1;
	int max_dist = 0;
	int fst = v;
	q.push(v);
	while (!q.empty()) {
		int fm = q.front();
		q.pop();
		int forbidden = from[fm];
		int nval = dist[fm] + 1;
		for (int to : g[fm]) {
			if (to == forbidden)
				continue;
			dist[to] = nval;
			from[to] = fm;
			if (nval > max_dist) {
				max_dist = nval;
				fst = to;
			}
			q.push(to);
		}
	}
	if (fst == v) {
		diam[fst] = 0;
		return fst;
	}
	int save = fst;
	q.push(fst);
	dist[fst] = 0;
	from[fst] = -1;
	max_dist = 0;
	while (!q.empty()) {
		int fm = q.front();
		q.pop();
		int forbidden = from[fm];
		int nval = dist[fm] + 1;
		for (int to : g[fm]) {
			if (to == forbidden)
				continue;
			dist[to] = nval;
			from[to] = fm;
			if (nval > max_dist) {
				max_dist = nval;
				fst = to;
			}
			q.push(to);
		}
	}
	int radius = max_dist - max_dist / 2;
	q.push(save);
	if (dist[save] == radius) {
		diam[save] = max_dist;
		return save;
	}
	while (!q.empty()) {
		int fm = q.front();
		q.pop();
		for (int to : g[fm]) {
			if (from[to] == fm) {
				if (dist[to] == radius) {
					diam[to] = max_dist;
					return to;
				}
				q.push(to);
			}
		}

	}
	throw 1;
}

void MarkComponent(int v) {
	MakeSet(v);
	queue<int> q;
	q.push(v);
	from[v] = -1;
	while (!q.empty()) {
		int fm = q.front();
		q.pop();
		int forbidden = from[fm];
		for (int to : g[fm]) {
			if (to == forbidden)
				continue;
			from[to] = fm;
			AttachNode(to, v);
			q.push(to);
		}
	}
}

int main() {
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);
	for (int i = 0; i < m; ++i) {
		int x, y;
		scanf("%d%d", &x, &y);
		--x, --y;
		g[x].push_back(y);
		g[y].push_back(x);
	}
	memset(parent, -1, sizeof parent);
	for (int i = 0; i < n; ++i) {
		if (parent[i] != -1)
			continue;
		int c = GetCenter(i);
		MarkComponent(c);
	}
	for (int it = 0; it < q; ++it) {
		int type;
		scanf("%d", &type);
		if (type == 1) {
			int v;
			scanf("%d", &v);
			--v;
			v = FindSet(v);
			printf("%d\n", diam[v]);
		} else {
			int u, v;
			scanf("%d%d", &u, &v);
			--u, --v;
			u = FindSet(u);
			v = FindSet(v);
			if (u == v)
				continue;
			int du = diam[u];
			int dv = diam[v];
			UnionSets(u, v);
			v = FindSet(v);
			diam[v] = max(max(du, dv), (du - du / 2) + (dv - dv / 2) + 1);
		}
	}
    return 0;
}
