#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
typedef pair<li, li> pl;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

int main() {
	int n;
	scanf("%d", &n);
	vector<int> a(n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
	}
	sort(a.begin(), a.end());
	vector<pt> ca;
	for (int i = 0; i < n; ) {
		int j = i + 1;
		for (; j < n && a[j] == a[i]; ++j);
		ca.push_back({a[i], j - i});
		i = j;
	}
	int m = int(ca.size());
	reverse(ca.begin(), ca.end());
	vector<li> d(m, 0);
	li ans = 0;
	li prev = 0;
	for (int i = 0; i < m; ++i) {
		int cur = ca[i].first;
		prev = max(prev, d[i]);
		li val = prev + li(cur) * ca[i].second;
		ans = max(ans, val);
		for (int j = i + 1; j < m; ++j) {
			if (abs(ca[j].first - cur) > 1) {
				d[j] = max(d[j], val);
				break;
			}
		}
	}
	cout << ans << endl;

    return 0;
}
