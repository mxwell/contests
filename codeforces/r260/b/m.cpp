#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;
constexpr int AB = 'z' - 'a' + 1;

enum {
	UNKNOWN = 0,
	YES_CAN = 1,
	NO_CANNOT = 2
};

struct node {
	node *c[AB];
	int can_win;
	int can_lose;
};

struct Tree {
	int sz;
	node nodes[N];
	node *root;
	void init() {
		sz = 0;
		root = MakeNode();
	}
	node *MakeNode() {
		return &nodes[sz++];
	}
	void AddString(const string &s);
} tree;

void Tree::AddString(const string &s) {
	node *t = root;
	for (char raw : s) {
		int sym = raw - 'a';
		if (t->c[sym] == NULL) {
			t->c[sym] = MakeNode();
		}
		t = t->c[sym];
	}
}

bool CanWin(node *t) {
	int &tg = t->can_win;
	if (tg == UNKNOWN) {
		tg = NO_CANNOT;
		for (int i = 0; i < AB; ++i) {
			node *to = t->c[i];
			if (to && !CanWin(to)) {
				tg = YES_CAN;
				break;
			}
		}
	}
	return tg == YES_CAN;
}

bool CanLose(node *t) {
	int &tg = t->can_lose;
	if (tg == UNKNOWN) {
		tg = NO_CANNOT;
		bool flag = false;
		for (int i = 0; i < AB; ++i) {
			node *to = t->c[i];
			if (to)
				flag = true;
			if (to && !CanLose(to)) {
				tg = YES_CAN;
				break;
			}
		}
		if (!flag)
			tg = YES_CAN;
	}
	return tg == YES_CAN;
}

void answer(bool first) {
	puts(first ? "First" : "Second");
}

int main() {
	tree.init();
	int n, k;
	scanf("%d%d\n", &n, &k);
	string line;
	for (int i = 0; i < n; ++i) {
		getline(cin, line);
		tree.AddString(line);		
	}
	bool win = CanWin(tree.root);
	bool lose = CanLose(tree.root);
	answer(win && (lose || (k & 1) != 0));

    return 0;
}
