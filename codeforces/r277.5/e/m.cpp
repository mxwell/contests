#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<ld, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

int n, len;
ld d[N];
bool used[N];
int pos[N];
int bea[N];
int par[N];
ld fr[N][N];

bool f(ld r) {
	memset(used, 0, sizeof used);
	for (int i = 0; i < n; ++i) {
		ld val = d[i];
		for (int j = i + 1; j <= n; ++j) {
			ld cost = val + fr[i][j] - r * bea[j];
			if (!used[j] || d[j] > cost) {
				used[j] = true;
				par[j] = i;
				d[j] = cost;
			}
		}
	}
	return d[n] < 0;
}

int main() {
	scanf("%d%d", &n, &len);
	for (int i = 1; i <= n; ++i) {
		scanf("%d%d", &pos[i], &bea[i]);
	}
	for (int i = 0; i < n; ++i) {
		for (int j = i + 1; j <= n; ++j)
			fr[i][j] = sqrt(fabs(pos[j] - pos[i] - len));
	}
	ld lf = 0, rg = 1e9;
	for (int it = 0; it < 200; ++it) {
		ld mid = (lf + rg) / 2;
		if (f(mid))
			rg = mid;
		else
			lf = mid;
	}
	f(rg);
	vector<int> path;
	for (int i = n; i > 0; i = par[i])
		path.push_back(i);
	reverse(path.begin(), path.end());
	for (int i = 0; i < int(path.size()); ++i) {
		printf("%d ", path[i]);
	}
	puts("");
	
    return 0;
}
