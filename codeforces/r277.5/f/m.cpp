#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 505;

int n, m, modulo;
char field[N][N];
int d[2][N][N];

void add(int &dest, li val) {
	dest = (int) ((dest + val) % modulo);
}

int mult(li x, li y) {
	return (int) ((x * y) % modulo);
}

int main() {
	scanf("%d%d%d\n", &n, &m, &modulo);
	for (int i = 0; i < m; ++i)
		gets(field[i]);
	int c1 = 0, c2 = 0;
	for (int i = 0; i < n; ++i) {
		int tmp = 0;
		for (int j = 0; j < m; ++j) {
			tmp += field[j][i] == '1';
		}
		if (tmp == 1)
			++c1;
		else if (tmp == 0)
			++c2;
	}
	int cur = 0, next = 1;
	d[cur][c1][c2] = 1;
	for (int r = m; r < n; ++r) {
		memset(d[next], 0, sizeof d[next]);
		for (int i = 0; i <= n; ++i)
			for (int j = 0; i + j <= n; ++j) {
				int val = d[cur][i][j];
				if (!val)
					continue;
				/* 1 and 1 */
				if (i >= 2)
					add(d[next][i - 2][j], mult(val, i * (i - 1) / 2));
				/* 1 and 2 */
				if (i >= 1 && j >= 1)
					add(d[next][i - 1 + 1][j - 1], mult(val, i * j));
				/* 2 and 2 */
				if (j >= 2)
					add(d[next][i + 2][j - 2], mult(val, j * (j - 1) / 2));
			}
		swap(cur, next);
	}
	printf("%d\n", d[cur][0][0]);
	
    return 0;
}
