#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
  int a, b;
  scanf("%d%d", &a, &b);
  printf("%d %d\n", min(a, b), (max(a, b) - min(a, b)) / 2);
  return 0;
}
