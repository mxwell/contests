#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 5100;

vector<int> g[N];

inline void SetInVector(vector<int> &v, int pos, int val) {
  while ((int) v.size() <= pos)
    v.push_back(1e9);
  v[pos] = min(v[pos], val);
}

vector<int> Dfs(int v, int prev) {
  if (g[v].size() == 1)
    return {1, 0};
  vector<int> result[2];
  int cur = 0, next = 1;
  result[cur] = {0};
  for (int to : g[v]) {
    if (to == prev)
      continue;
    auto ch = Dfs(to, v);
    fill(result[next].begin(), result[next].end(), 1e9);
    for (int i = int(ch.size()) - 1; i >= 0; --i) {
      int n = int(result[cur].size());      
      for (int j = n - 1; j >= 0; --j) {
        int t = result[cur][j] + ch[i];
        SetInVector(result[next], i + j, t);
      }
    }
    swap(cur, next);
  }
  int m = int(result[cur].size());
  int half = m / 2;
  for (int i = 0; i <= half; ++i) {
    int &lf = result[cur][i];
    int &rg = result[cur][m - i - 1];
    if (lf + 1 < rg)
      rg = lf + 1;
    if (rg + 1 < lf)
      lf = rg + 1;
  }
  return result[cur];
}

int Solve(int source) {
  auto v = Dfs(source, -1);
  return v[v.size() / 2];
}

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n - 1; ++i) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    g[x].push_back(y);
    g[y].push_back(x);
  }
  if (n == 2) {
    puts("1");
    return 0;
  }
  int root = 0;
  while (g[root].size() < 2)
    ++root;
  printf("%d\n", Solve(root));
  return 0;
}
