#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 105;

int side[3][2];

char tb[N][N];
const char *kLetters = "ABC";

int print(int w) {
  printf("%d\n", w);
  for (int i = 0; i < w; ++i) {
    tb[i][w] = '\0';
    puts(tb[i]);  
  }
  return 0;
}

int main() {
  int mx = 0;
  int mx_id = 0;
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < 2; ++j) {
      scanf("%d", &side[i][j]);
      if (side[i][j] > mx) {
        mx = side[i][j];
        mx_id = i;
      }
    }
    if (side[i][0] < side[i][1]) {
      swap(side[i][0], side[i][1]);
    }
  }
  for (int i = 0; i < side[mx_id][1]; ++i)
    for (int j = 0; j < mx; ++j)
      tb[i][j] = kLetters[mx_id];
  int first = -1, second = -1;
  for (int i = 0; i < 3; ++i) {
    if (i == mx_id)
      continue;
    if (first == -1)
      first = i;
    else
      second = i;
  }
  for (int ri = 0; ri < 2; ++ri) {
    for (int rj = 0; rj < 2; ++rj) {
      if (side[first][0] + side[second][0] == mx
        && side[first][1] == side[second][1]
        && side[first][1] + side[mx_id][1] == mx) {
        for (int i = side[mx_id][1]; i < mx; ++i) {
          int j = 0;
          for (; j < side[first][0]; ++j)
            tb[i][j] = kLetters[first];
          for (; j < mx; ++j)
            tb[i][j] = kLetters[second];
        }
        return print(mx);
      }
      if (side[first][0] == mx
        && side[second][0] == mx
        && side[mx_id][1] + side[first][1] + side[second][1] == mx) {
        int i = side[mx_id][1];
        for (int k = 0; k < side[first][1]; ++k, ++i) {
          for (int j = 0; j < mx; ++j) {
            tb[i][j] = kLetters[first];  
          }
        }
        for (int k = 0; k < side[second][1]; ++k, ++i) {
          for (int j = 0; j < mx; ++j) {
            tb[i][j] = kLetters[second];
          }
        }
        assert(i == mx);
        return print(mx);
      }
      swap(side[second][0], side[second][1]);
    }
    swap(side[first][0], side[first][1]);
  }
  puts("-1");
  return 0;
}
