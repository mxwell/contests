#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int cnt[11];

int main() {
  int n, k;
  scanf("%d%d", &n, &k);
  int normal = 0;
  int s = 0;
  for (int i = 0; i < n; ++i) {
    int a;
    scanf("%d", &a);
    int r = a % 10;
    if (r) {
      ++cnt[10 - r];
    }
    normal += (100 - a) / 10;
    s += a / 10;
  }
  for (int i = 1; i <= 9; ++i) {
    int t = min(k / i, cnt[i]);
    s += t;
    k -= i * t;
  }
  s += min(normal, k / 10);
  printf("%d\n", s);
  return 0;
}
