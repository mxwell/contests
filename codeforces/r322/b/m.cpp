#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int a[N];

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);  
  }
  int t = 0;
  for (int i = n - 1; i >= 0; --i) {
    int c = a[i];
    if (t >= c) {
      a[i] = t + 1 - c;
    } else {
      t = c;
      a[i] = 0;
    }
  }
  for (int i = 0; i < n; ++i) {
    printf("%d ", a[i]);  
  }
  puts("");
  return 0;
}
