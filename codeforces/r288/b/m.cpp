#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  string s;
  getline(cin, s);
  int last = s[s.size() - 1] - '0';
  int best = -1;
  for (int i = 0; i + 1 < int(s.size()); ++i) {
    int d = s[i] - '0';
    if (d & 1)
      continue;
    if (d > last) {
      best = i;
    }
    if (d < last) {
      best = i;
      break;
    }
  }
  if (best == -1) {
    puts("-1");
  } else {
    swap(s[best], s[s.size() - 1]);
    puts(s.c_str());
  }
  return 0;
}
