#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 4100;

int in[N], out[N];
vector<int> g[N];
int gpos[N];
unordered_map<int, int> rel;
vector<string> ns;
bool used[N];

inline pt Convert(const string &s) {
  return {s[0] + 128 * s[1], s[1] + 128 * s[2]};
}

pt GetId(const string &s) {
  pt keys = Convert(s);
  if (rel.count(keys.first) == 0) {
    ns.push_back(s.substr(0, 2));
    rel.insert({keys.first, (int) rel.size()});
  }
  if (rel.count(keys.second) == 0) {
    ns.push_back(s.substr(1));
    rel.insert({keys.second, (int) rel.size()});
  }
  return {rel[keys.first], rel[keys.second]};
}

bool Check(int &source, int &dest, bool &add_fake) {
  source = 0, dest = (int) ns.size() - 1;
  int n = (int) rel.size();
  int v_in = 0, v_out = 0;
  for (int i = 0; i < n; ++i) {
    if (in[i] != out[i]) {
      if (in[i] == out[i] + 1) {
        ++v_in;
        dest = i;
      } else if (in[i] + 1 == out[i]) {
        ++v_out;
        source = i;
      } else {
        return false;
      }
    }
  }
  queue<int> q;
  q.push(source);
  used[source] = true;
  int visited = 1;
  while (!q.empty()) {
    int fm = q.front();
    q.pop();
    for (int to : g[fm]) {
      if (!used[to]) {
        ++visited;
        used[to] = true;
        q.push(to);
      }
    }
  }
  if (visited < n)
    return false;
  add_fake = v_in || v_out;
  return v_in < 2 && v_out < 2;
}

void Fleury(int source, int dest, bool add_fake, vector<int> &path) {
  if (add_fake)
    g[dest].push_back(source);
  vector<int> st;
  st.push_back(source);
  while (!st.empty()) {
    int v = st.back();
    bool flag = false;
    vector<int> &ch = g[v];
    for (int &i = gpos[v]; i < (int) ch.size(); ++i) {
      int &to = ch[i];
      if (to >= 0) {
        flag = true;
        st.push_back(to);
        to = -1;
        break;
      }
    }
    if (!flag) {
      path.push_back(v);
      st.pop_back();
    }
  }
  if (add_fake) {
    for (int i = 0; i + 1 < (int) path.size(); ++i) {
      if (path[i] == source && path[i + 1] == dest) {
        vector<int> p2;
        for (int j = i; j >= 0; --j)
          p2.push_back(path[j]);
        for (int j = (int) path.size() - 2; j > i; --j)
          p2.push_back(path[j]);        
        path = p2;
        break;        
      }
    }
  } else {
    reverse(path.begin(), path.end());
  }
}

int main() {
  int n;
  scanf("%d\n", &n);
  string s;
  for (int i = 0; i < n; ++i) {
    getline(cin, s);
    pt vs = GetId(s);
    g[vs.first].push_back(vs.second);
    ++in[vs.second];
    ++out[vs.first];
  }
  
  int from, to;
  bool add_fake;
  if (!Check(from, to, add_fake)) {
    puts("NO");
    return 0;
  }
  puts("YES");
  vector<int> path;
  Fleury(from, to, add_fake, path);
  string ans = ns[path[0]];
  for (int i = 1; i < (int) path.size(); ++i)
    ans.push_back(ns[path[i]][1]);

  printf("%s\n", ans.c_str());
  return 0;
}
