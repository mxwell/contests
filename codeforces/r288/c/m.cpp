#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 310;

bool used[1000];

void SetUsed(int i) {
  used[i + 500] = true;
}

bool GetUsed(int i) {
  return used[i + 500];
}

int main() {
  int m, t, r;
  scanf("%d%d%d", &m, &t, &r);
  set<int> q;
  int ans = 0;
  for (int i = 0; i < m; ++i) {
    int visit;
    scanf("%d", &visit);
    while (q.size() && visit >= *(q.begin())) {
      q.erase(q.begin());
    }
    for (int j = visit - 1; r > (int) q.size(); --j) {
      if (j + t + 1 <= visit) {
        puts("-1");
        return 0;
      }
      if (!GetUsed(j)) {
        SetUsed(j);
        ++ans;
        q.insert(j + t + 1);
      }
    }
  }
  printf("%d\n", ans);

  return 0;
}
