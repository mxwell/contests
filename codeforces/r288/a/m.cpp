#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 1010;

bool a[N][N];

inline bool Four(int x, int y) {
  return a[x][y] && a[x][y + 1] && a[x + 1][y] && a[x + 1][y + 1];
}

int main() {
  int n, m, k;
  scanf("%d%d%d", &n, &m, &k);
  for (int it = 1; it <= k; ++it) {
    int x, y;
    scanf("%d%d", &x, &y);
    ++x, ++y;
    a[x][y] = true;
    if (Four(x - 1, y - 1) || Four(x, y - 1) || Four(x - 1, y) || Four(x, y)) {
      printf("%d\n", it);
      return 0;
    }
  }
  puts("0");
  return 0;
}
