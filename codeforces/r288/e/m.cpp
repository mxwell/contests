#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n;
  scanf("%d", &n);
  vector<pt> r;
  for (int i = 0; i < n; ++i) {
    int x, y;
    scanf("%d%d", &x, &y);
    r.push_back({x, y});
  }
  vector<string> st = {string("")};
  for (int i = n - 1; i >= 0; --i) {
    if (r[i].first == 1) {
      st.push_back("()");
      continue;
    }
    int len = 1;
    int j;
    for (j = int(st.size()) - 1; j >= 0; --j) {
      len += int(st[j].size());
      if (r[i].first <= len && len <= r[i].second) {
        break;
      }
    }
    if (!(r[i].first <= len && len <= r[i].second)) {
      puts("IMPOSSIBLE");
      return 0;
    }
    string cur;
    while (int(st.size()) > j) {
      cur += st.back();
      st.pop_back();
    }
    st.push_back("(" + cur + ")");
  }
  string ans;
  while (!st.empty()) {
    ans += st.back();
    st.pop_back();
  }
  puts(ans.c_str());
  return 0;
}
