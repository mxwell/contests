#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int from, to, dif, duration;
  scanf("%d%d", &from, &to);
  scanf("%d%d", &duration, &dif);
  int ans = 0;
  int speed = from;
  for (int t = 0; t < duration; ++t) {
    //printf("%d\n", speed);
    ans += speed;
    int rem_time = duration - t - 2;
    //int min_speed = to - rem_time * d;
    int max_speed = to + rem_time * dif;
    speed = min(speed + dif, max_speed);
  }
  printf("%d\n", ans);
  return 0;
}
