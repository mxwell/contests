#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n;
  scanf("%d", &n);
  vector<int> ans;
  for (int i = (n & 1) ? n : n - 1; i >= 1; i -= 2)
    ans.push_back(i);
  for (int i = (n & 1) ? n - 1 : n; i >= 2; i -= 2)
    if (abs(ans.back() - i) > 1)
      ans.push_back(i);
  printf("%d\n", int(ans.size()));
  for (int i = 0; i < int(ans.size()); ++i) {
    printf("%d ", ans[i]);
  }
  puts("");
  return 0;
}
