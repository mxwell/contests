#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 200100;

li d[N];

int main() {
  int n;
  li A;
  scanf("%d%I64d", &n, &A);
  li s = 0;
  for (int i = 0; i < n; ++i) {
    scanf("%I64d", &d[i]);
    s += d[i];
  }
  for (int i = 0; i < n; ++i) {
    li fm = n - 1, to = s - d[i];
    li rto = max(1ll, A - fm), rfm = max(1ll, A - to);
    li ans = 0;
    if (rfm <= d[i]) {
      ans = d[i] - (min(rto, d[i]) - rfm + 1);
    }
    printf("%I64d ", ans);
  }
  puts("");
  return 0;
}
