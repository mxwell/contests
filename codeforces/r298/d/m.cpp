#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 200100;

vector<int> a[N];

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    int hands;
    scanf("%d", &hands);
    a[hands].push_back(i);
  }
  vector<int> ans;
  int cur = 0;
  for (int it = 0; it < n; ++it) {
    while (cur >= 0 && a[cur].size() == 0) {
      cur -= 3;
    }
    if (cur < 0) {
      puts("Impossible");
      return 0;
    }
    ans.push_back(a[cur].back());
    a[cur].pop_back();
    ++cur;
  }
  puts("Possible");
  for (int i = 0; i < int(ans.size()); ++i) {
    printf("%d ", ans[i]);
  }
  puts("");
  return 0;
}
