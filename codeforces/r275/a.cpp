#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	int n, k;
	scanf("%d%d", &n, &k);
	//printf("%d %d\n", n, k);
	int lf = 1, rg = n;
	int i;
	for (i = 1; i <= k; ++i) {
		int a;
		if (i & 1) {
			a = lf;
			++lf;
		} else {
			a = rg;
			--rg;
		}
		printf("%d ", a);
	}
	if (i & 1)
		for (int j = rg; j >= lf; --j)
			printf("%d ", j);
	else
		for (int j = lf; j <= rg; ++j)
			printf("%d ", j);
	puts("");
    return 0;
}
