#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int a[5 * N];
int t[5 * N];
pair<pt, int> qs[N];
int n;

void add(int id, int lf, int rg, int fm, int to, int val) {
	if (lf == fm && rg == to) {
		a[id] |= val;
		return;
	}
	int mid = (lf + rg) / 2;
	int tol = id * 2;
	int tor = tol + 1;
	if (fm <= mid)
		add(tol, lf, mid, fm, min(to, mid), val);
	if (to > mid)
		add(tor, mid + 1, rg, max(fm, mid + 1), to, val);
	return;
	//printf("%d - %d :: %d <= %d & %d [%d]\n", lf, rg,
	//	a[id], a[tol], a[tor], a[id] | (a[tol] & a[tor]));
	a[id] |= a[tol] & a[tor];
}

int query(int id, int lf, int rg, int fm, int to) {
	if (lf == fm && rg == to) {
		return t[id];
	}
	int mid = (lf + rg) / 2;
	int tol = id * 2;
	int tor = tol + 1;
	int res = 0;
	if (fm <= mid) {
		if (to > mid) {
			res = query(tol, lf, mid, fm, min(mid, to)) &
				   query(tor, mid + 1, rg, max(mid + 1, fm), to);
		} else {
			res = query(tol, lf, mid, fm, min(mid, to));
		}
	} else if (to > mid)
		res = query(tor, mid + 1, rg, max(mid + 1, fm), to);
	//printf("q %d - %d :: %d\n", lf, rg, res);
	return res;
}

int vals[N];
void dump(int id, int lf, int rg, int acc) {
	//printf("%d - %d :: (%d)+%d\n", lf, rg, acc, a[id]);
	acc |= a[id];
	if (lf == rg) {
		vals[lf] = acc;
		t[id] = acc;
		return;
	}
	int mid = (lf + rg) / 2;
	int tol = id * 2;
	int tor = tol + 1;
	dump(tol, lf, mid, acc);
	dump(tor, mid + 1, rg, acc);
	t[id] = t[tol] & t[tor];
}

int main() {
	int m;
	scanf("%d%d", &n, &m);
	forn (it, m) {
		int lf, rg, q;
		scanf("%d%d%d", &lf, &rg, &q);
		--lf, --rg;
		qs[it] = mp(pt(lf, rg), q);
		add(1, 0, n - 1, lf, rg, q);
	}
	dump(1, 0, n - 1, 0);
	forn (it, m) {
		pt &bound = qs[it].first;
		int q = qs[it].second;
		if (query(1, 0, n - 1, bound.first, bound.second) != q) {
			puts("NO");
			return 0;
		}
	}
	puts("YES");
	forn (i, n)
		printf("%d ", vals[i]);
	puts("");
    return 0;
}
