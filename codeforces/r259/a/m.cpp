#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

ld bpow(ld x, int y) {
  ld r = 1;
  while (y) {
    if (y & 1)
      r *= x, --y;
    else
      x *= x, y >>= 1;
  }
  return r;
}

int main() {
  int m, n;
  cin >> m >> n;
  ld ans = 0;
  ld fp_m = ld(m);
  for (int i = 1; i <= m; ++i)
    ans += i * (bpow(i / fp_m, n) - bpow((i - 1) / fp_m, n));
  cout.precision(9);
  cout << fixed << ans << endl;
  return 0;
}
