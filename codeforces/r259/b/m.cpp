#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 105;
constexpr int M = 1 << 16;

int a[N];

int pr[16];
int d[N][M];
int p[N][M];
int prime[60];
int num[60];
vector<int> g[M];

void init() {
  memset(prime, -1, sizeof prime);
  int id = 0;
  for (int i = 2; i < 59; ++i) {
    bool flag = true;
    for (int j = 2; j * j <= i; ++j)
      if (i % j == 0) {
        flag = false;
        if (prime[j] >= 0)
          num[i] |= 1 << prime[j];
        if (prime[i / j] >= 0)
          num[i] |= 1 << prime[i / j];
      }
    if (flag) {
      prime[i] = id;
      num[i] |= 1 << prime[i];
      pr[id++] = i;
    }
  }
  assert(id == 16);
  for (int m = 0; m < M; ++m) {
    for (int i = 2; i < 59; ++i) {
      if ((m & num[i]) == 0)
        g[m].push_back(i);
    }
  }
}

inline bool upd(int &tg, int val) {
  if (tg < 0 || tg > val) {
    tg = val;
    return true;
  }
  return false;
}

int main() {
  init();
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  memset(d, -1, sizeof d);
  d[0][0] = 0;
  p[0][0] = 0;
  for (int i = 0; i < n; ++i) {
    int *cur = d[i];
    int *next = d[i + 1];
    int *par = p[i + 1];
    for (int m = 0; m < M; ++m) {
      if (cur[m] < 0)
        continue;
      if (upd(next[m], cur[m] + a[i] - 1))
        par[m] = 1;
      if (a[i] == 1)
        continue;
      for (int x : g[m]) {
        int nm = m | num[x];
        if (upd(next[nm], cur[m] + abs(a[i] - x)))
          par[nm] = x;
      }
    }
  }
  int *cur = d[n];
  int pos = -1;
  int best = 1e9;
  for (int m = 0; m < M; ++m) {
    if (cur[m] >= 0 && cur[m] < best) {
      best = cur[m];
      pos = m;
    } 
  }
  assert(pos >= 0);
  vector<int> ans;
  for (int i = n; i > 0; --i) {
    int v = p[i][pos];
    ans.push_back(v);
    if (v != 1)
      pos &= ~num[v];
  }
  for (int i = n - 1; i >= 0; --i)
    printf("%d ", ans[i]);
  puts("");
  return 0;
}
