#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;
const int MODULO = 1e9 + 7;

vector<int> g[N];
bool black[N];
bool bl[N];
int d[2][N];

void add(int &tg, int val) {
	tg = (tg + val) % MODULO;
}

void mult(int &tg, li val) {
	li tmp = tg * val;
	tg = static_cast<int>(tmp % MODULO);
}

bool is_black(int v, int from = -1) {
	bool &res = bl[v];
	res = black[v];
	for (int to : g[v]) {
		if (to == from)
			continue;
		if (is_black(to, v))
			res = true;
	}
	return res;
}

int dfs(int v, int cut, int from = -1) {
	if (black[v])
		cut = 0;
	int &tg = d[cut][v];
	if (tg < 0) {
		tg = 1;
		vector<pt> vals;
		for (int to : g[v]) {
			if (to == from || !bl[to])
				continue;
			int tmp = 0;
			if (!black[to])
				add(tmp, dfs(to, 0, v));
			add(tmp, dfs(to, 1, v));
			vals.push_back(pt(tmp, to));
		}
		if (!cut) {
			for (pt val : vals)
				mult(tg, val.first);
		} else if (!vals.empty()) {
			int sz = static_cast<int>(vals.size());
			vector<int> suf(sz);
			int tmp = 1;
			for (int i = sz - 1; i >= 0; --i) {
				mult(tmp, vals[i].first);
				suf[i] = tmp;
			}
			tg = 0;
			tmp = 1;
			for (int i = 0; i < sz; ++i) {
				int cur = dfs(vals[i].second, 1, v);
				mult(cur, tmp);
				if (i + 1 < sz)
					mult(cur, suf[i + 1]);
				add(tg, cur);
				mult(tmp, vals[i].first);
			}
		}
	}
	//printf("v %d, cut %d d = %d, from %d\n", v, cut, tg, from);
	return tg;
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 1; i < n; ++i) {
		int j;
		scanf("%d", &j);
		g[i].push_back(j);
		g[j].push_back(i);
	}
	int root = -1;
	for (int i = 0; i < n; ++i) {
		int c;
		scanf("%d", &c);
		if (c) {
			black[i] = true;
			if (root < 0)
				root = i;
		}
	}
	is_black(root);
	memset(d, -1, sizeof d);
	printf("%d\n", dfs(root, 0));
    return 0;
}
