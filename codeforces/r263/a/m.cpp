#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 300010;

int a[N];

int main() {
	int n;	
	scanf("%d", &n);
	li s = 0;
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
		s += a[i];
	}
	sort(a, a + n);
	li ans = s;
	for (int i = 0; i + 1 < n; ++i) {
		ans += s;
		s -= a[i];		
	}
	cout << ans << endl;
    return 0;
}
