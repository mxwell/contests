#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 200010;

int n;
int t[N];

void add(int pos, int val) {
	for (int i = pos; i < n; i |= i + 1)
		t[i] += val;
}

int prefix(int rg) {
	int res = 0;
	for (int i = rg; i >= 0; i = (i & (i + 1)) - 1)
		res += t[i];
	return res;
}

int sum(int lf, int rg) {
	int res = prefix(rg);
	if (lf > 0)
		res -= prefix(lf - 1);
	return res;
}

int value(int pos) {
	return sum(pos, pos);
}

int main() {
	int m;
	scanf("%d%d", &n, &m);
	for (int i = 0; i < n; ++i) {
		add(i, 1);
	}
	int fm = 0, to = n - 1;
	int pivot = fm;
	for (int it = 0; it < m; ++it) {
		int type;
		scanf("%d", &type);
		if (type == 1) {
			int p;
			scanf("%d", &p);
			int npivot, len, src_step, dest_step;
			if (p <= (to - fm) + 1 - p) {
				len = p;
				if (pivot == fm)
					npivot = fm + len, src_step = -1, dest_step = 1, fm += len;
				else
					npivot = to - len, src_step = 1, dest_step = -1, to -= len;
			} else {
				len = (to - fm) + 1 - p;
				if (pivot == fm)
					npivot = to - len, src_step = 1, dest_step = -1, to -= len;
				else
					npivot = fm + len, src_step = -1, dest_step = 1, fm += len;
			}
			for (int i = 1; i <= len; ++i) {
				int src = npivot + src_step * i;
				int dest = npivot + dest_step * (i - 1);
				add(dest, value(src));
			}
			pivot = npivot;
		} else {
			int lf, rg;
			scanf("%d%d", &lf, &rg);
			--rg;
			if (pivot == fm) {
				lf += fm, rg += fm;
			} else {
				lf = to - lf, rg = to - rg;
			}
			if (lf > rg)
				swap(lf, rg);
			printf("%d\n", sum(lf, rg));
		}
	}


    return 0;
}
