#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int maxcats;
bool cat[N];
vector<int> g[N];

int dfs(int v, int prev, int d) {
  if (d > maxcats)
    return 0;
  if (g[v].size() == 1 && g[v][0] == prev) {
    return 1;
  }
  if (g[v].empty()) {
    return 1;
  }
  int res = 0;
  for (int to : g[v]) {
    if (to == prev)
      continue;
    if (cat[to])
      res += dfs(to, v, d + 1);
    else
      res += dfs(to, v, 0);
  }
  return res;
}

int main() {
  int n;
  scanf("%d%d", &n, &maxcats);
  for (int i = 0; i < n; ++i) {
    int x;
    scanf("%d", &x);
    cat[i] = x == 1;  
  }
  for (int j = 1; j < n; ++j) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    g[x].push_back(y);
    g[y].push_back(x);
  }
  printf("%d\n", dfs(0, -1, cat[0] ? 1 : 0));
  return 0;
}
