#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
  int n, d;
  scanf("%d%d", &n, &d);
  vector<pt> fs(n);
  for (int i = 0; i < n; ++i) {
      scanf("%d%d", &fs[i].first, &fs[i].second);
  }
  sort(fs.begin(), fs.end());
  li ans = 0;
  int rg = 0;
  li s = 0;
  for (int i = 0; i < n; ++i) {
    while (rg < n && fs[i].first + d > fs[rg].first) {
      s += fs[rg].second;
      ++rg;
    }
    ans = max(ans, s);
    s -= fs[i].second;
  }
  cout << ans << endl;
  return 0;
}
