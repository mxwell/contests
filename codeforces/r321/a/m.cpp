#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
  int n;
  scanf("%d", &n);
  int len = 0, ans = 1, prev = 0;
  for (int i = 0; i < n; ++i) {
    int a;
    scanf("%d", &a);
    if (a >= prev) {
      ++len;
      ans = max(ans, len);
    } else {
      len = 1;
    }
    prev = a;
  }
  printf("%d\n", ans);
  return 0;
}
