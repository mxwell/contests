#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 18;

int a[N];
int g[N][N];
li d[1 << N][N];

int main() {
  int n, m, nrules;
  scanf("%d%d%d", &n, &m, &nrules);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]); 
  }
  for (int i = 0; i < nrules; ++i) {
    int x, y, add;
    scanf("%d%d%d", &x, &y, &add);
    --x, --y;
    g[x][y] = add;
  }
  for (int i = 0; i < n; ++i) {
    d[1 << i][i] = a[i];  
  }
  li ans = 0;
  for (int mask = 0, mask_end = 1 << n; mask < mask_end; ++mask) {
    int bitlen = 0;
    li cur = 0;
    for (int i = 0; i < n; ++i) {
      li val = d[mask][i];
      cur = max(cur, val);
      if (mask & (1 << i)) {
        ++bitlen;
        for (int j = 0; j < n; ++j) {
          int bit = 1 << j;
          if ((mask & bit) == 0) {
            li &tg = d[mask | bit][j];
            tg = max(tg, val + a[j] + g[i][j]);
          }
        }
      }
    }
    if (bitlen == m) {
      ans = max(ans, cur);
    }
  }
  cout << ans << endl;
  return 0;
}
