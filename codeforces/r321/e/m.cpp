#include <bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;
const li H = 1031;
const li MOD = 104111401;

li ph[N];
li hashes[10][N];

inline li calc_hash_short(li left, li right) {
  return (left * H + right) % MOD;
}

inline li calc_hash(li left, li right, int right_len) {
  return (left * ph[right_len] + right) % MOD;
}

void init() {
  ph[0] = calc_hash_short(0, 1);
  for (int i = 1; i < N; ++i) {
    ph[i] = calc_hash_short(ph[i - 1], 0);
  }
  for (int d = 0; d < 10; ++d) {
    li c = d + '0';
    li *row = hashes[d];
    row[0] = calc_hash_short(0, c);
    for (int i = 1; i < N; ++i) {
      row[i] = calc_hash_short(row[i - 1], c);
    }
  }
}

li tree[N * 5];
int cached[N * 5];

void build(int id, int lf, int rg, string &orig) {
  if (lf == rg) {
    tree[id] = calc_hash_short(0, orig[lf]);
    return;
  }
  int mid = (lf + rg) / 2;
  int tlf = id * 2;
  int trg = tlf + 1;
  build(tlf, lf, mid, orig);
  build(trg, mid + 1, rg, orig);
  tree[id] = calc_hash(tree[tlf], tree[trg], rg - mid);
}

li get_hash(int id, int lf, int rg, int from, int to) {
  if (lf == from && rg == to) {
    return tree[id];
  }
  li res = 0;
  int mid = (lf + rg) / 2;
  int tlf = id * 2;
  int trg = tlf + 1;
  int pw = 0;
  if (cached[id]) {
    int digit = cached[id] - 1;
    tree[tlf] = hashes[digit][mid - lf];
    cached[tlf] = cached[id];
    tree[trg] = hashes[digit][rg - (mid + 1)];
    cached[trg] = cached[id];
    cached[id] = 0;
  }
  if (to > mid) {
    pw = to - mid;
    res = get_hash(trg, mid + 1, rg, max(mid + 1, from), to);
  }
  if (from <= mid) {
    res = calc_hash(get_hash(tlf, lf, mid, from, min(mid, to)), res, pw);
  }
  return res;
}

void modify(int id, int lf, int rg, int from, int to, int digit) {
  if (lf == from && rg == to) {
    tree[id] = hashes[digit][rg - lf];
    cached[id] = digit + 1;
    return;
  }
  int mid = (lf + rg) / 2;
  int tlf = id * 2;
  int trg = tlf + 1;
  if (cached[id]) {
    int old = cached[id] - 1;
    tree[tlf] = hashes[old][mid - lf];
    cached[tlf] = cached[id];
    tree[trg] = hashes[old][rg - (mid + 1)];
    cached[trg] = cached[id];
    cached[id] = 0;
  }
  if (from <= mid)
    modify(tlf, lf, mid, from, min(mid, to), digit);
  if (to > mid)
    modify(trg, mid + 1, rg, max(mid + 1, from), to, digit);
  tree[id] = calc_hash(tree[tlf], tree[trg], rg - mid);
}

inline li calc(li base, int len, int count) {
  li res = 0;
  li cur = calc_hash_short(0, base);
  int bit = 1;
  int end = 32 - __builtin_clz(unsigned(count));
  for (int i = 0; i < end; ++i) {
    if (count & bit) {
      res = calc_hash(res, cur, len);
    }
    cur = calc_hash(cur, cur, len);
    len <<= 1;
    bit <<= 1;
  }
  return res;
}

int main() {
  init();
  int n, m, k;
  scanf("%d%d%d\n", &n, &m, &k);
  string orig;
  getline(cin, orig);
  build(1, 0, n - 1, orig);
  m += k;
  for (int it = 0; it < m; ++it) {
    int type, lf, rg, val;
    scanf("%d%d%d%d", &type, &lf, &rg, &val);
    --lf, --rg;
    if (type == 1) {
      modify(1, 0, n - 1, lf, rg, val);
    } else {
      li pre = get_hash(1, 0, n - 1, lf, lf + val - 1);
      int periods = (rg - lf + 1) / val;
      pre = calc(pre, val, periods);
      li found = get_hash(1, 0, n - 1, lf, lf + periods * val - 1);
      if (pre != found) {
        puts("NO");
        continue;
      }
      int tail_len = (rg - lf + 1) - periods * val;
      if (tail_len) {
        li head = get_hash(1, 0, n - 1, lf, lf + tail_len - 1);
        li tail = get_hash(1, 0, n - 1, rg - tail_len + 1, rg);
        if (head != tail) {
          puts("NO");
          continue;
        }
      }
      puts("YES");
    }
  }
  return 0;
}