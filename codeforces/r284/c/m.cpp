#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 105;
const int G = N * 32;

int a[N];
vector<int> g[G];

int factor(int x, vector<pt> &f) {
	int res = 0;
	if ((x & 1) == 0) {
		int c = 0;
		while ((x & 1) == 0)
			x >>= 1, ++c;
		res += c;
		f.push_back(pt(2, c));
	}
	for (int i = 3; i * i <= x; i += 2)
		if (x % i == 0) {
			int c = 1;
			x /= i;
			while (x % i == 0)
				x /= i, ++c;
			res += c;
			f.push_back(pt(i, c));
		}
	if (x > 1) {
		f.push_back(pt(x, 1));
		++res;
	}
	return res;
}

void add_edge(int fm, int to) {
	g[fm].push_back(to);
	//g[to].push_back(fm);
}

bool used[G];
int par[G];

bool tryk(int v) {
	if (used[v])
		return false;
	used[v] = true;
	for (int to : g[v])
		if (par[to] == -1 || tryk(par[to])) {
			par[to] = v;
			return true;
		}		
	return false;
}

int main() {
	int n, m;
	scanf("%d%d", &n, &m);
	vector<int> prec(n + 1, 0);
	vector<vector<pt> > fs(n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
		int c = factor(a[i], fs[i]);
		prec[i + 1] = prec[i] + c;
	}
	for (int i = 0; i < m; ++i) {
		int x, y;
		scanf("%d%d", &x, &y);
		--x, --y;
		if (x & 1)
			swap(x, y);
		size_t xi = 0, yi = 0;
		int xclone = prec[x], yclone = prec[y];
		while (xi < fs[x].size() && yi < fs[y].size()) {
			int xval = fs[x][xi].first, yval = fs[y][yi].first;
			if (xval < yval)
				xclone += fs[x][xi].second, ++xi;
			else if (xval > yval)
				yclone += fs[y][yi].second, ++yi;
			else {
				int xn = fs[x][xi].second;
				int yn = fs[y][yi].second;
				for (int j = 0; j < xn; ++j) {
					for (int k = 0; k < yn; ++k) {
						add_edge(xclone + j, yclone + k);
					}
				}
				xclone += fs[x][xi].second, ++xi;
				yclone += fs[y][yi].second, ++yi;
			}
		}
	}
	memset(par, -1, sizeof par);
	for (int v = prec[n]; v >= 0; --v) {
		memset(used, 0, sizeof used);
		tryk(v);
	}
	int ans = 0;
	for (int v = prec[n]; v >= 0; --v) {
		ans += par[v] != -1;
	}
	printf("%d\n", ans);
    return 0;
}
