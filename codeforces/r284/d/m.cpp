#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100010;
const int S = 60;

int a[N];
int t[N * 5][S];

void make_node(int *node, int k) {
	for (int i = 0; i < S; ) {
		node[i++] = 2;
		for (int j = 1; j < k; ++j) {
			node[i++] = 1;
		}
	}
}

void up(int *node, int *left, int *right) {
	for (int i = 0; i < S; ++i) {
		int tmp = left[i];
		node[i] = tmp + right[(i + tmp) % S];
	}
}

void build(int id, int lf, int rg) {
	if (lf == rg) {
		make_node(t[id], a[lf]);
		return;
	}
	int m = (lf + rg) / 2;
	int u = id << 1;
	int v = u + 1;
	build(u, lf, m);
	build(v, m + 1, rg);
	up(t[id], t[u], t[v]);
}

void update(int id, int lf, int rg, int pos, int val) {
	if (lf == rg) {
		make_node(t[id], val);
		return;
	}
	int m = (lf + rg) / 2;
	int u = id << 1;
	int v = u + 1;
	if (pos <= m)
		update(u, lf, m, pos, val);
	else
		update(v, m + 1, rg, pos, val);
	up(t[id], t[u], t[v]);
}

int get(int id, int lf, int rg, int fm, int to, int k) {
	if (lf == fm && rg == to)
		return t[id][k];
	int m = (lf + rg) / 2;
	int u = id << 1;
	int v = u + 1;
	int res = 0;
	if (fm <= m)
		res += get(u, lf, m, fm, min(m, to), k);
	if (to > m)
		res += get(v, m + 1, rg, max(m + 1, fm), to, (res + k) % S);
	return res;
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);		
	}
	build(1, 0, n - 1);
	int m;
	scanf("%d\n", &m);
	for (int it = 0; it < m; ++it) {
		char c;
		int x, y;
		scanf("%c%d%d\n", &c, &x, &y);
		if (c == 'A') {
			--x, --y;
			printf("%d\n", get(1, 0, n - 1, x, y - 1, 0));
		} else {
			--x;
			update(1, 0, n - 1, x, y);
		}
	}
    return 0;
}
