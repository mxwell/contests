#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<li, li> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

struct lin {
	li a, b, c;
	lin() {}
	lin(li a, li b, li c) : a(a), b(b), c(c) {}
};

li gcd(li a, li b) {
	return a ? gcd(b % a, a) : b;
}

lin from_pts(const pt& u, const pt& v) {
	li a = u.second - v.second;
	li b = v.first - u.first;
	li c = -(a * u.first + b * u.second);
	li g = gcd(a, b);
	g = gcd(g, c);
	return lin(a / g, b / g, c / g);
}

pt read_pt() {
	int x, y;
	cin >> x >> y;
	return pt(x, y);
}

ld det(ld a, ld b, ld c, ld d) {
	return a * d - b * c;
}

bool betw(li fm, li to, ld x) {
	if (fm > to)
		swap(fm, to);
	return fm < x + EPS && x - EPS < to;
}

int main() {
	pt fm = read_pt(), to = read_pt();
	int n;
	cin >> n;
	lin m = from_pts(fm, to);
	int ans = 0;
	for (int i = 0; i < n; ++i) {
		int a, b, c;
		cin >> a >> b >> c;
		lin n(a, b, c);
		ld vp = det(m.a, m.b, n.a, n.b);
		if (fabs(vp) < EPS)
			continue;
		ld x = -det(m.c, m.b, n.c, n.b) / vp;
		ld y = -det(m.a, m.c, n.a, n.c) / vp;
		ans += betw(fm.first, to.first, x) && betw(fm.second, to.second, y);
	}
	cout << ans << endl;
    return 0;
}
