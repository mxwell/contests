#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 5010;

ld d[2][N];

ld bpow(ld x, int y) {
	ld r = 1;
	while (y) {
		if (y & 1) {
			r *= x;
			--y;
		} else {
			x *= x;
			y >>= 1;
		}
	}
	return r;
}

int main() {
	int n, dur;
	scanf("%d%d", &n, &dur);
	int cur = 0, next = 1;
	d[cur][0] = 1;
	ld ans = 0;
	int total = 0;
	for (int i = 0; i < n; ++i) {
		int pr, lim;
		scanf("%d%d", &pr, &lim);
		total += lim;
		ld p = pr / 100.0;
		ld q = 1 - p;
		ld s = 0;
		ld qlim1 = bpow(q, lim - 1);
		ld qlim2 = lim >= 2 ? bpow(q, lim - 2) : 1;
		for (int j = 0; j <= dur; ++j) {
			d[next][j] = s * p;
			if (j >= lim)
				d[next][j] += d[cur][j - lim] * qlim1;
			if (lim > 1) {
				if (j >= lim - 1) {
					s -= d[cur][j - lim + 1] * qlim2;
				}
				s = s * q + d[cur][j];
			}			
		}
		s = 1;
		for (int j = dur; j > dur - lim; --j)
			ans += i * d[cur][j] * s, s *= q;
		swap(cur, next);
	}
	for (int j = 0; j <= dur; ++j)
		ans += d[cur][j] * n;
	if (total <= dur) {
		printf("%d\n", n);
		return 0;
	}
	cout.precision(9);
	cout << fixed << ans << endl;
    return 0;
}
