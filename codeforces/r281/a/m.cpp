#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int a[2][N];

int main() {
  string home, attendant;
  getline(cin, home);
  getline(cin, attendant);
  int n;
  scanf("%d\n", &n);
  for (int it = 0; it < n; ++it) {
    int t, num;
    char team, card;
    scanf("%d %c %d %c\n", &t, &team, &num, &card);
    int team_id = team == 'h' ? 0 : 1;
    int &val = a[team_id][num];
    int save = val;
    val += card == 'y' ? 1 : 2;
    if (save < 2 && val >= 2) {
      printf("%s %d %d\n", team_id == 0 ? home.c_str() : attendant.c_str(),
        num, t);
    }
  }
  return 0;
}
