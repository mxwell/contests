#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n;
  scanf("%d", &n);
  vector<int> a, b;
  li sa = 0, sb = 0;
  bool last_a = false;
  for (int i = 0; i < n; ++i) {
    int x;
    scanf("%d", &x);
    if (x > 0)
      a.push_back(x), sa += x, last_a = true;
    else
      b.push_back(-x), sb -= x, last_a = false;
  }
  if (sa > sb ||
      (sa == sb && (a > b || (a == b && last_a))))
    puts("first");
  else
    puts("second");
  return 0;
}
