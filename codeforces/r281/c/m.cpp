#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

pt best;
int diff;

void Update(int a, int b) {
  if (a - b > diff || (a - b == diff && best.first < a)) {
    best = {a, b};
    diff = a - b;
  }
}

int main() {
  int n;
  scanf("%d", &n);
  vector<int> a(n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  int m;
  scanf("%d", &m);
  vector<int> b(m);
  for (int i = 0; i < m; ++i) {
    scanf("%d", &b[i]);
  }
  sort(a.begin(), a.end());
  sort(b.begin(), b.end());
  diff = -1e9;
  Update(n * 2, m * 2);
  Update(n * 3, m * 3);
  size_t ai = 0, bi = 0;
  int x = 0;
  while (ai < a.size() && bi < b.size()) {
    bool taken = false;
    if (ai < a.size())
      x = a[ai], taken = true;
    if (bi < b.size()) {
      if (b[bi] < x || !taken)
        x = b[bi], taken = true;
    }
    assert(taken);
    while (ai < a.size() && a[ai] <= x)
      ++ai;
    while (bi < b.size() && b[bi] <= x)
      ++bi;
    Update(int(ai * 2 + (a.size() - ai) * 3),
           int(bi * 2 + (b.size() - bi) * 3));
  }
  printf("%d:%d\n", best.first, best.second);
  return 0;
}
