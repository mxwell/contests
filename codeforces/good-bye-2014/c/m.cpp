#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);

int u2[510];

int main() {
	int n, m;
	scanf("%d%d", &n, &m);
	vector<int> a(n);
	vector<pt> w(n);
	for (int i = 0; i < n; ++i) {
		int x;
		scanf("%d", &x);
		a[i] = x;
		w[i] = pt(1e9, x);
	}
	vector<int> b(m);
	for (int i = 0; i < m; ++i) {
		scanf("%d", &b[i]);
		--b[i];
		if (w[b[i]].first > 1e8)
			w[b[i]].first = i;
	}
	sort(w.begin(), w.end());
	vector<int> used(n, -1);
	li ans = 0;
	int mark = 1;
	for (int it = 0; it < m; ++it) {
		int id = b[it];
		int s = 0;
		if (used[id] >= 0) {
			for (int i = it - 1; i > used[id]; --i) {
				if (u2[b[i]] < mark)
					s += a[b[i]];
				u2[b[i]] = mark;
			}
			++mark;
		} else {
			for (int i = 0; i < it; ++i) {
				if (u2[b[i]] < mark)
					s += a[b[i]];
				u2[b[i]] = mark;
			}
			++mark;
		}
		ans += s;
		used[id] = it;
	}
	cout << ans << endl;
    return 0;
}
