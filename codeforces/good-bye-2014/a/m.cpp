#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 30100;

int a[N];

int main() {
	int n, t;
	scanf("%d%d", &n, &t);
	--t;
	for (int i = 0; i < n - 1; ++i) {
		scanf("%d", &a[i]);
	}
	for (int p = 0; p < n; p += a[p]) {
		if (p == t) {
			puts("YES");
			return 0;
		}
		if (p + 1 >= n)
			break;
	}
	puts("NO");
    return 0;
}
