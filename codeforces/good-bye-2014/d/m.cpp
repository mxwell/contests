#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int n;
vector<int> es;
vector<pt> g[N];
bool used[N];
int sub[N];
li stat[N];

int dfs(int v, int eid) {
	used[v] = true;
	int &res = sub[v];
	for (pt p : g[v]) {
		int to = p.first;
		if (!used[to]) {
			res += dfs(to, p.second);
		}
	}
	++res;
	if (eid >= 0)
		stat[eid] = li(res) * (n - res);
	return res;
}

int main() {
	scanf("%d", &n);
	int m = n - 1;
	for (int i = 0; i < m; ++i) {
		int x, y, len;
		scanf("%d%d%d", &x, &y, &len);		
		--x, --y;
		es.push_back(len);
		g[x].push_back(pt(y, i));
		g[y].push_back(pt(x, i));
	}
	dfs(0, -1);
	li ans = 0;
	for (int i = 0; i < m; ++i) {
		ans += stat[i] * es[i];		
	}
	int q;
	li d = n * (n - 1ll);
	scanf("%d", &q);
	for (int it = 0; it < q; ++it) {
		int eid, len;
		scanf("%d%d", &eid, &len);
		--eid;
		ans -= stat[eid] * (es[eid] - len);
		es[eid] = len;
		//printf("ans %lf, d %d\n", (double) ans, (int) d);
		printf("%.9lf\n", (double) (ld(ans) * 6 / d));
	}
	
    return 0;
}
