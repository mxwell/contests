#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 310;

int p[N];
char g[N][N];
bool occ[N];
bool used[N];

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &p[i]);		
	}
	scanf("\n");
	for (int i = 0; i < n; ++i) {
		gets(g[i]);
	}
	for (int i = 0; i < n; ++i) {
		queue<int> q;
		q.push(i);
		memset(used, 0, sizeof used);
		used[i] = true;
		int best = -1;
		if (!occ[i])
			best = i;
		while (!q.empty()) {
			int fm = q.front();
			q.pop();
			for (int j = 0; j < n; ++j) {
				if (g[fm][j] == '1' && !used[j]) {
					if (!occ[j]) {
						if (best == -1 || p[best] > p[j])
							best = j;
					}
					used[j] = true;
					q.push(j);
				}
			}
		}
		occ[best] = true;
		printf("%d ", p[best]);
	}
	puts("");

    return 0;
}
