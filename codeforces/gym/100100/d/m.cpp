#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 132000;

struct node {
  int prio;
  int data;
  int count;
  int key;
  int rg;
  node *left, *right;
  node () {}
};

typedef node* pnode;

inline int GetCount(pnode t) {
  return t ? t->count : 0;
}

inline int GetRg(pnode t) {
  return t ? t->rg : 0;
}

inline void UpdateCount(pnode t) {
  if (t) {
    t->count = GetCount(t->left) + GetCount(t->right) + 1;
    int rg = 1;
    if (t->left)
      rg = max(rg, GetRg(t->left) + 1 + GetCount(t->right));
    if (t->right)
      rg = max(rg, t->right->rg);
    rg = max(rg, t->key + GetCount(t->right));
    t->rg = rg;
  }
}

inline void UpdateKey(pnode t, int acc_key) {
  t->key = max(t->key, max(acc_key + GetCount(t->left), GetRg(t->left) + 1));
}

void Split(pnode t, int acc_key, int key, pnode &left, pnode &right) {
  if (!t) {
    left = right = NULL;
  } else {
    UpdateKey(t, acc_key);
    if (key > t->key)
      Split(t->right, t->key + 1, key, t->right, right), left = t;
    else
      Split(t->left, acc_key, key, left, t->left), right = t;
    UpdateCount(t);
  }
}

void Merge(pnode left, pnode right, int acc_key, pnode &out) {
  if (!left) {
    UpdateKey(right, acc_key);
    out = right;
  } else if (!right) {
    UpdateKey(left, acc_key);
    out = left;
  } else if (left->prio < right->prio) {
    UpdateKey(left, acc_key);
    Merge(left->right, right, left->key + 1, left->right);
    out = left;
  } else {
    UpdateKey(right, acc_key);
    Merge(left, right->left, acc_key, right->left);
    out = right;
  }
  UpdateCount(out);
}

int printed;
void PrintRec(pnode t, int acc_key, vector<int> &ans) {
  if (!t)
    return;
  PrintRec(t->left, acc_key, ans);
  int key = max(t->key, max(acc_key + GetCount(t->left), GetRg(t->left) + 1));
  ++printed;
  while (printed < key) {
    ans.push_back(0);
    ++printed;
  }
  ans.push_back(t->data);
  PrintRec(t->right, key + 1, ans);
}

void Print(pnode t, vector<int> &ans) {
  printed = 0;
  PrintRec(t, 1, ans);
}

void Insert(pnode &t, pnode item) {
  pnode t1, t2;
  Split(t, 1, item->key, t1, t2);
  Merge(item, t2, 1, t2);
  Merge(t1, t2, 1, t);
}

struct treap {
  node nodes[N];
  int nodes_top;
  void Init() {
    vector<int> ps(N);
    for (int i = 0; i < N; ++i) {
      ps[i] = i;
    }
    random_shuffle(ps.begin(), ps.end());
    for (int i = 0; i < N; ++i) {
      nodes[i].prio = ps[i];
    }
    nodes_top = 0;
  }
  pnode MakeNode(int data, int id) {
    pnode t = &nodes[nodes_top++];
    t->data = data;
    t->key = t->rg = id;
    return t;
  }
} tree;

int main() {
  freopen("key.in", "r", stdin);
  freopen("key.out", "w", stdout);
  tree.Init();
  int n, m;
  scanf("%d%d", &n, &m);
  pnode root = NULL;
  for (int it = 1; it <= n; ++it) {
    int x;
    scanf("%d", &x);
    pnode item = tree.MakeNode(it, x);
    if (root)
      Insert(root, item);
    else
      root = item;
  }
  vector<int> ans;
  Print(root, ans);
  printf("%d\n", int(ans.size()));
  for (size_t i = 0; i < ans.size(); ++i)
    printf("%d ", ans[i]);
  return 0;
}
