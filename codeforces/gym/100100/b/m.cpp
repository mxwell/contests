#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

struct node {
  int id;
  int key;
  int priority;
  node *par, *left, *right;
  node () {};
};

typedef node* pnode;

int nodes_top;
node nodes[N];
int ans_p[N];
int ans_lf[N];
int ans_rg[N];

pnode CreateNode(int id, int x, int y) {
  pnode t = &nodes[nodes_top++];
  t->id = id;
  t->key = x;
  t->priority = y;
  return t;
}

pnode CreateNode(tuple<int, int, int> t) {
  return CreateNode(get<2>(t), get<0>(t), get<1>(t));
}

void Collect(pnode t, int parent) {
  if (!t)
    return;
  int id = t->id;
  ans_p[id - 1] = parent;
  if (t->left) {
    Collect(t->left, id);
    ans_lf[id - 1] = t->left->id;
  }
  if (t->right) {
    Collect(t->right, id);
    ans_rg[id - 1] = t->right->id;
  }
}

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  int n;
  scanf("%d", &n);
  /* key, priority, id */
  vector<tuple<int, int, int>> v;
  for (int i = 0; i < n; ++i) {
    int x, y;
    scanf("%d%d", &x, &y);
    v.push_back(make_tuple(x, y, i + 1));
  }
  sort(v.begin(), v.end());
  pnode last = CreateNode(v[0]);
  for (int i = 1; i < n; ++i) {
    pnode cur = CreateNode(v[i]);
    int y = cur->priority;
    pnode saved = last;
    while (last && last->priority > y) {
      saved = last;
      last = last->par;
    }
    if (last) {
      if (last->right) {
        cur->left = last->right;
        last->right->par = cur;
      }
      last->right = cur;
      cur->par = last;
    } else {
      cur->left = saved;
      saved->par = cur;
    }
    last = cur;
  }
  while (last->par)
    last = last->par;
  puts("YES");
  Collect(last, 0);
  for (int i = 0; i < n; ++i) {
    printf("%d %d %d\n", ans_p[i], ans_lf[i], ans_rg[i]);
  }
  return 0;
}
