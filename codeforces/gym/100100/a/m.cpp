#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

struct node {
  int priority;
  int data;
  int count;
  node *left, *right;
  node () {}
};

typedef node* pnode;

int GetCount(pnode t) {
  return t ? t->count : 0;
}

void UpdateCount(pnode t) {
  if (t)
    t->count = GetCount(t->left) + GetCount(t->right) + 1;
}

void Split(pnode t, int acc_key, int key, pnode &left, pnode &right) {
  if (!t) {
    left = right = NULL;
  } else {
    int t_key = acc_key + GetCount(t->left);
    if (t_key < key) {
      Split(t->right, t_key + 1, key, t->right, right);
      left = t;
    } else {
      Split(t->left, acc_key, key, left, t->left);
      right = t;
    }
    UpdateCount(t);
  }
}

void Merge(pnode &t, pnode left, pnode right) {
  if (!left)
    t = right;
  else if (!right)
    t = left;
  else {
    if (left->priority < right->priority) {
      Merge(right->left, left, right->left);
      t = right;
    } else {
      Merge(left->right, left->right, right);
      t = left;
    }
  }
  UpdateCount(t);
}

void PushBack(pnode &root, pnode item) {
  Merge(root, root, item);
}

void PrintRec(pnode t) {
  if (!t)
    return;
  PrintRec(t->left);
  printf("%d ", t->data);
  PrintRec(t->right);
}

void Print(pnode t) {
  PrintRec(t);
  puts("");
}

void MoveToHead(pnode &t, int fm, int to) {
  pnode t1, t2, t3;
  Split(t, 0, fm, t1, t2);
  Split(t2, 0, to - fm + 1, t2, t3);
  Merge(t, t2, t1);
  Merge(t, t, t3);
}

struct treap {
  int size;
  node nodes[N];
  int ps_top;
  vector<int> ps;
  void Init(int pool_size) {
    size = 0;
    ps.resize(pool_size);
    for (int i = 0; i < pool_size; ++i) {
      ps[i] = i;
    }
    random_shuffle(ps.begin(), ps.end());
    ps_top = 0;
  }
  pnode MakeNode(int data) {
    pnode t = &nodes[size++];
    t->data = data;
    t->count = 0;
    t->priority = ps[ps_top++];
    return t;
  }
};

treap tree;

int main() {
  freopen("movetofront.in", "r", stdin);
  freopen("movetofront.out", "w", stdout);
  int n, m;
  scanf("%d%d", &n, &m);
  tree.Init(n + m);
  pnode root = tree.MakeNode(1);
  for (int i = 2; i <= n; ++i)
    PushBack(root, tree.MakeNode(i));
  while (m-- > 0) {
    int fm, to;
    scanf("%d%d", &fm, &to);
    --fm, --to;
    MoveToHead(root, fm, to);
  }
  Print(root);
  return 0;
}
