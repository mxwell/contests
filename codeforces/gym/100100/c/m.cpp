#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int INF = 1e9 + 7;
constexpr int N = 300100;
constexpr int MODULO = 1e9;

struct node {
  int key;
  int prio;
  node *left, *right;
  node () {}
};

typedef node* pnode;

void Split(pnode t, int key, pnode &left, pnode &right) {
  if (!t)
    left = right = NULL;
  else if (key < t->key)
    Split(t->left, key, left, t->left), right = t;
  else
    Split(t->right, key, t->right, right), left = t;
}

void Insert(pnode &t, pnode item) {
  if (!t)
    t = item;
  else if (t->prio > item->prio)
    Split(t, item->key, item->left, item->right), t = item;
  else if (item->key < t->key)
    Insert(t->left, item);
  else
    Insert(t->right, item);
}

int FindNext(pnode t, int key) {
  if (!t)
    return INF;
  if (t->key == key)
    return key;
  if (key < t->key)
    return min(t->key, FindNext(t->left, key));
  return FindNext(t->right, key);
}

struct treap {
  node nodes[N];
  int nodes_top;
  vector<int> prios;
  int prios_top;
  void Init(int psize) {
    prios.resize(psize);
    for (int i = 0; i < psize; ++i) {
      prios[i] = i;
    }
    random_shuffle(prios.begin(), prios.end());
    prios_top = 0;
    nodes_top = 0;
  }
  pnode CreateNode(int x) {
    pnode t = &nodes[nodes_top++];
    t->key = x;
    t->prio = prios[prios_top++];
    return t;
  }
} tree;

int main() {
  freopen("next.in", "r", stdin);
  freopen("next.out", "w", stdout);
  int n;
  scanf("%d\n", &n);
  tree.Init(n);
  int prevNext = INF;
  pnode root = NULL;
  while (n-- > 0) {
    char type;
    int arg;
    scanf("%c%d\n", &type, &arg);
    if (type == '+') {
      if (prevNext < INF) {
        arg = (arg + prevNext) % MODULO;
        prevNext = INF;
      }
      pnode item = tree.CreateNode(arg);
      if (root)
        Insert(root, item);
      else
        root = item;
    } else {
      prevNext = FindNext(root, arg);
      if (prevNext >= INF)
        prevNext = -1;
      printf("%d\n", prevNext);
    }
  }
  return 0;
}
