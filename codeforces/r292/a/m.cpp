#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n;
  scanf("%d\n", &n);
  vector<int> distr[] = {
    {}, {}, {2}, {3}, {2, 2, 3}, /* 0 - 4 */
    {5}, {3, 5}, {7}, {2, 2, 2, 7}, /* 5 - 8 */
    {2, 3, 3, 7}
  };
  string s;
  getline(cin, s);
  vector<int> ans;
  for (char c : s) {
    int d = c - '0';
    if (d < 2)
      continue;
    for (int x : distr[d])
      ans.push_back(x);
  }
  sort(ans.rbegin(), ans.rend());
  for (int x : ans)
    printf("%d", x);
  puts("");
  return 0;
}
