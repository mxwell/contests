#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 200100;

int dist[N];
int height[N];
li predist[N];
li prefix[N * 5];
li suffix[N * 5];
li tree[N * 5];
int glast;

void init(int _n) {
  int n2 = _n * 2;
  predist[0] = dist[0];
  for (int i = 1; i < n2; ++i)
    predist[i] = predist[i - 1] + dist[i];
}

li GetDist(int fm, int to) {
  if (fm == to)
    return 0;
  li res = predist[to - 1];
  if (fm)
    res -= predist[fm - 1];
  return res;
}

void BuildPrefix(int id, int lf, int rg) {
  if (lf == rg) {
    prefix[id] = height[lf];
  } else if (lf + 1 == rg) {
    prefix[id] = max(li(height[lf]), li(height[rg]) + dist[lf]);
  } else {
    int mid = (lf + rg) / 2;
    int tolf = id * 2;
    int torg = tolf + 1;
    BuildPrefix(tolf, lf, mid);
    BuildPrefix(torg, mid, rg);
    prefix[id] = max(prefix[tolf], GetDist(lf, mid) + prefix[torg]);
  }
}

li GetPrefix(int id, int lf, int rg, int fm, int to) {
  if (lf == fm && rg == to)
    return prefix[id];
  if (lf + 1 == rg) {
    if (fm == rg)
      return height[rg];
    if (to == lf)
      return height[lf];
    throw 1;
  }
  int mid = (lf + rg) / 2;
  int tolf = id * 2;
  int torg = tolf + 1;
  if (to <= mid)
    return GetPrefix(tolf, lf, mid, fm, to);
  if (fm >= mid)
    return GetPrefix(torg, mid, rg, fm, to);
  li res = GetPrefix(tolf, lf, mid, fm, min(to, mid));
  res = max(res, GetDist(fm, mid) + GetPrefix(torg, mid, rg, mid, to));
  return res;
}


void BuildSuffix(int id, int lf, int rg) {
  if (lf == rg) {
    suffix[id] = height[lf];
  } else if (lf + 1 == rg) {
    suffix[id] = max(li(height[rg]), li(height[lf]) + dist[lf]);
  } else {
    int mid = (lf + rg) / 2;
    int tolf = id * 2;
    int torg = tolf + 1;
    BuildSuffix(tolf, lf, mid);
    BuildSuffix(torg, mid, rg);
    suffix[id] = max(suffix[torg], suffix[tolf] + GetDist(mid, rg));
  }
}

li GetSuffix(int id, int lf, int rg, int fm, int to) {
  if (lf == fm && rg == to)
    return suffix[id];
  if (lf + 1 == rg) {
    if (fm == rg) {
      return height[rg];
    }
    if (to == lf) {
      return height[lf];
    }
    throw 1;
  }
  int mid = (lf + rg) / 2;
  int tolf = id * 2;
  int torg = tolf + 1;
  if (to <= mid) {
    return GetSuffix(tolf, lf, mid, fm, to);
  }
  if (fm >= mid) {
    return GetSuffix(torg, mid, rg, fm, to);
  }
  li res = GetSuffix(torg, mid, rg, max(fm, mid), to);
  res = max(res, GetSuffix(tolf, lf, mid, fm, mid) + GetDist(mid, to));
  return res;
}

void BuildTree(int id, int lf, int rg) {
  if (lf == rg) {
    tree[id] = 0;
  } else if (lf + 1 == rg) {
    tree[id] = li(height[lf]) + dist[lf] + li(height[rg]);
  } else {
    int mid = (lf + rg) / 2;
    int tolf = id * 2;
    int torg = tolf + 1;
    BuildTree(tolf, lf, mid);
    BuildTree(torg, mid, rg);
    li &res = tree[id];
    res = max(tree[tolf], tree[torg]);
    if (mid + 1 <= rg) {
      res = max(res, GetSuffix(tolf, lf, mid, lf, mid) + dist[mid] + GetPrefix(1, 0, glast, mid + 1, rg));
    }
  }
}

li GetOnTree(int id, int lf, int rg, int fm, int to) {
  if (lf == fm && rg == to)
    return tree[id];
  int mid = (lf + rg) / 2;
  int tolf = id * 2;
  int torg = tolf + 1;
  if (to <= mid)
    return GetOnTree(tolf, lf, mid, fm, to);
  if (fm >= mid)
    return GetOnTree(torg, mid, rg, fm, to);
  li res = GetOnTree(tolf, lf, mid, fm, mid);
  res = max(res, GetOnTree(torg, mid, rg, mid, to));
  res = max(res, GetSuffix(tolf, lf, mid, fm, mid) + dist[mid] + GetPrefix(1, 0, glast, mid + 1, to));
  return res;
}

int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &dist[i]);
    dist[i + n] = dist[i];
  }
  for (int i = 0; i < n; ++i) {
    scanf("%d", &height[i]);
    height[i] *= 2;
    height[i + n] = height[i];
  }

  init(n);
  glast = 2 * n - 1;
  BuildSuffix(1, 0, glast);
  BuildPrefix(1, 0, glast);
  BuildTree(1, 0, glast);
  for (int it = 0; it < m; ++it) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    int u, v;
    if (x <= y) {
      u = y + 1, v = n + x - 1;
      assert(u + 1 <= v);
    } else {
      u = y + 1, v = x - 1;
      assert(u + 1 <= v);
    }
    printf("%I64d\n", GetOnTree(1, 0, glast, u, v));
  }
  return 0;
}
