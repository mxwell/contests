#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 2100;

char buf[N];
bool field[N][N];
char used[N][N];

const int dx[] = {-1, 0, 0, 1};
const int dy[] = {0, -1, 1, 0};

const char *sym = "v><^";
const char *sym2 = "^<>v";

int SayNo() {
  puts("Not unique");
  exit(0);
  return 0;
}

void Check(int n, int m, queue<pt> &q, int i, int j) {
  if (field[i][j] || used[i][j])
    return;
  int nr = 0;
  for (int t = 0; t < 4; ++t) {
    int u = i + dx[t];
    int v = j + dy[t];
    if (!(0 <= u && u < n && 0 <= v && v < m))
      continue;
    if (field[u][v] || used[u][v])
      continue;
    ++nr;
  }
  if (nr == 1) {
    q.push({i, j});
  }
  if (nr == 0)
    SayNo();
}

int main() {
  int n, m;
  scanf("%d%d\n", &n, &m);
  int total = 0;
  for (int i = 0; i < n; ++i) {
    gets(buf);
    for (int j = 0; j < m; ++j) {
      if (buf[j] == '*')
        field[i][j] = true;
      else
        ++total;
    }
  }
  queue<pt> q;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      if (!field[i][j]) {
        int nr = 0;
        for (int t = 0; t < 4; ++t) {
          int u = i + dx[t],
              v = j + dy[t];
          if (!(0 <= u && u < n && 0 <= v && v < m))
            continue;
          if (!field[u][v])
            ++nr;
        }
        if (nr == 1) {
          q.push({i, j});
        }
      }
    }
  }
  int count = 0;
  while (!q.empty()) {
    pt p = q.front();
    q.pop();
    if (used[p.first][p.second])
      continue;
    bool ok = false;
    for (int t = 0; t < 4; ++t) {
      int u = p.first + dx[t];
      int v = p.second + dy[t];
      if (!(0 <= u && u < n && 0 <= v && v < m))
        continue;
      if (field[u][v]) {
        continue;
      }
      if (used[u][v]) {
        continue;
      }
      used[p.first][p.second] = sym[t];
      used[u][v] = sym2[t];
      for (int tt = 0; tt < 4; ++tt) {
        int x = u + dx[tt];
        int y = v + dy[tt];
        if (0 <= x && x < n && 0 <= y && y < m)
          Check(n, m, q, x, y);
      }
      count += 2;
      ok = true;
      break;
    }
    if (!ok) {
      return SayNo();
    }
  }
  if (count != total) {
    return SayNo();
  }

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      printf("%c", field[i][j] ? '*' : used[i][j]);
    }
    puts("");
  }

  return 0;
}
