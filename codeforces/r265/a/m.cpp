#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	int n, _p;
	scanf("%d%d\n", &n, &_p);
	string s;
	getline(cin, s);
	int p = _p + 'a';
	for (int i = n - 1; i >= 0; --i) {
		for (int next = s[i] + 1; next < p; ++next) {
			if ((i == 0 || s[i - 1] != next) && (i < 2 || s[i - 2] != next)) {
				s[i] = next;
				bool flag = i + 1 >= n;
				for (int j = i + 1; j < n; ++j) {
					flag = false;
					for (int k = 'a'; k < p; ++k) {
						if (k != s[j - 1] && (j < 2 || k != s[j - 2])) {
							s[j] = k;
							flag = true;
							break;
						}
					}
					if (!flag) {
						break;
					}
				}
				if (flag) {
					cout << s << endl;
					return 0;
				}
			}
		}
	}
	cout << "NO" << endl;
    return 0;
}
