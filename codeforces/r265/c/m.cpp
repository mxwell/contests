#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;
const int MODULO = 1e9 + 7;

void do_mul(li &tg, li m) {
	tg = (tg * m) % MODULO;
}

void do_add(li &tg, li a) {
	tg = (tg + a) % MODULO;
}

vector<li> rem, mult;

void conv(const string &s, li &r, li &m) {
	r = 0;
	m = 1;
	for (size_t j = 0; j < s.size(); ++j) {
		int d = s[j] - '0';
		do_mul(m, mult[d]);
		do_mul(r, mult[d]);
		do_add(r, rem[d]);
	}
} 

int main() {
	string s;
	getline(cin, s);	
	int n;
	scanf("%d\n", &n);
	vector<char> qfm;
	vector<string> qto;
	for (int it = 0; it < n; ++it) {
		string q;
		getline(cin, q);
		qfm.push_back(q[0] - '0');
		qto.push_back(q.substr(3));
	}
	for (int i = 0; i < 10; ++i) {
		rem.push_back(i);
		mult.push_back(10);
	}
	li r, m;
	for (int i = n - 1; i >= 0; --i) {
		conv(qto[i], r, m);
		int from = qfm[i];
		rem[from] = r;
		mult[from] = m;
	}
	conv(s, r, m);
	cout << r << endl;
    return 0;
}
