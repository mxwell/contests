#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int perm[6][3];

void init() {
	vector<int> p(3);
	for (int i = 0; i < 3; ++i) {
		p[i] = i;
	}
	int j = 0;
	do {
		for (int i = 0; i < 3; ++i) {
			perm[j][i] = p[i];
		}
		++j;
	} while (next_permutation(p.begin(), p.end()));
	assert(j == 6);
}

int c[8][3];
int d[8][3];
li val[3];
int cnt[3];
int memt[8];
li memv[8][8];

inline li dist(int u, int v) {
	li res = 0;
	for (int i = 0; i < 3; ++i) {
		res += sqr((li) d[u][i] - d[v][i]);
	}
	return res;
}

inline bool add(li v) {
	if (!v)
		return false;
	bool flag = false;
	int empty = -1;
	for (int i = 0; i < 3; ++i) {
		if (cnt[i]) {
			if (val[i] == v) {
				++cnt[i];
				flag = true;
				break;
			}
		} else {
			if (empty == -1)
				empty = i;
		}
	}
	if (!flag) {
		if (empty == -1)
			return false;
		val[empty] = v;
		cnt[empty] = 1;
	}
	return true;
}

bool check() {
	vector<pair<li, int> > v;
	for (int i = 0; i < 3; ++i) {
		v.push_back(make_pair(val[i], cnt[i]));
	}
	sort(v.begin(), v.end());
	/*
	for (int i = 0; i < 3; ++i) {
		cout << v[i].first << ',' << v[i].second << ' ';
	}
	cout << endl;
	*/
	li x2 = v[0].first;
	return v[2].first == 3 * x2 && v[1].first == 2 * x2 && v[0].second == 12 && v[1].second == 12 && v[2].second == 4;
}

inline void rm(li v) {
	for (int i = 0; i < 3; ++i)
		if (cnt[i] && val[i] == v) {
			--cnt[i];
			break;
		}
}

void rec(int id) {
	if (id >= 8) {
		if (check()) {
			puts("YES");
			for (int i = 0; i < 8; ++i) {
				for (int j = 0; j < 3; ++j) {
					printf("%d ", d[i][j]);
				}
				puts("");
			}
			exit(0);
		}
		return;
	}
	int *from = c[id];
	int *to = d[id];
	for (int i = 0; i < 6; ++i) {
		int *p = perm[i];
		for (int j = 0; j < 3; ++j) {
			to[p[j]] = from[j];
		}
		int &top = memt[id];
		li *memval = memv[id];
		top = 0;
		bool flag = true;
		for (int k = 0; k < id; ++k) {
			li v = dist(k, id);
			if (!add(v)) {
				flag = false;
				break;
			}
			memval[top++] = v;
		}
		if (flag)
			rec(id + 1);
		while (top > 0) {
			rm(memval[--top]);
		}
	}
}

int main() {
	init();
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 3; ++j) {
			scanf("%d", &c[i][j]);
		}
	}
	rec(0);
	puts("NO");
    return 0;
}
