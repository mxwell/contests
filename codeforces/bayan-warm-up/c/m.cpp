#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

char a[N][N];
char b[N][N];

bool filled(int lf, int rg, int tp, int bt) {
	for (int i = tp; i <= bt; ++i)
		for (int j = lf; j <= rg; ++j)
			if (a[i][j] != 'X')
				return false;
	return true;
}

bool paint(int lf, int rg, int tp, int bt) {
	for (int i = tp; i <= bt; ++i)
		for (int j = lf; j <= rg; ++j) {
			if (a[i][j] != 'X')
				return false;
			b[i][j] = 'X';
		}
	return true;
}

bool trace(int n, int m, int r, int c, int h, int w) {
	while (true) {
		if (!paint(c, c + w - 1, r, r + h - 1))
			return false;
		/* right */
		int rg = c + w - 1;
		while (rg + 1 < m && a[r][rg + 1] == 'X')
			++rg;
		if (rg > c + w - 1) {
			c += min(w, rg - (c + w - 1));
			continue;
		}
		/* down */
		int bt = r + h - 1;
		while (bt + 1 < n && a[bt + 1][c] == 'X')
			++bt;
		if (bt > r + h - 1) {
			r += min(h, bt - (r + h - 1));
			continue;
		}
		break;
	}
	return true;
}

bool compare(int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			bool af = a[i][j] == 'X';
			bool bf = b[i][j] == 'X';
			if (af != bf)
				return false;
		}
	}
	return true;
}

int check(int n, int m) {
	pt ft(-1, -1);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			if (a[i][j] == 'X') {
				ft = pt(i, j);
				break;
			}
		}
		if (ft.first != -1)
			break;
	}
	int h = 1;
	for (int i = ft.first + 1; i < n; ++i)
		if (a[i][ft.second] == 'X')
			++h;
		else
			break;
	int rg = ft.second;
	for (int j = rg + 1; j < m; ++j)
		if (a[ft.first][j] == 'X')
			rg = j;
		else
			break;
	int w = 1;
	if (ft.first + h < n) {
		int bt = ft.first + h;
		int blf = ft.second;
		for (int j = blf + 1; j <= rg; ++j) {
			if (a[bt][j] != 'X')
				blf = j;
			else
				break;
		}
		if (rg - blf > 0) {
			w = rg - blf;
		}
	}
	memset(b, 0, sizeof b);
	if (!trace(n, m, ft.first, ft.second, h, w))
		return 1e9;
	if (!compare(n, m))
		return 1e9;
	return h * w;
}

bool first_check(int n, int m) {
	int pstart = -1, pend = -1;
	for (int i = 0; i < n; ++i) {
		bool on = false, off = false;
		int start = -1, end = -1;
		for (int j = 0; j < m; ++j) {
			if (a[i][j] == 'X' && (j == 0 || a[i][j - 1] != 'X')) {
				if (on)
					return false;
				on = true;
				start = j;
			}
			if (a[i][j] == 'X' && (j + 1 >= m || a[i][j + 1] != 'X')) {
				if (off)
					return false;
				off = true;
				end = j;
			}
		}
		if (on) {
			if (start < pstart)
				return false;
			if (end < pend)
				return false;
			pstart = start;
			pend = end;
		}
	}
	pstart = -1, pend = -1;
	for (int j = 0; j < m; ++j) {
		bool on = false, off = false;
		int start = -1, end = -1;
		for (int i = 0; i < n; ++i) {
			if (a[i][j] == 'X') {
				if (i == 0 || a[i - 1][j] != 'X') {
					if (on)
						return false;
					on = true;
					start = i;
				}
				if (i + 1 >= n || a[i + 1][j] != 'X') {
					if (off)
						return false;
					off = true;
					end = i;
				}
			}
		}
		if (on) {
			if (start < pstart)
				return false;
			if (end < pend)
				return false;
			pstart = start;
			pend = end;
		}
	}
	return true;
}

void tr(int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			b[i][j] = a[i][j];
		}
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			a[j][i] = b[i][j];
		}
	}
}

int main() {
	int n, m;
	scanf("%d%d\n", &n, &m);
	for (int i = 0; i < n; ++i) {
		gets(a[i]);
	}
	if (!first_check(n, m)) {
		puts("-1");
		return 0;
	}
	int ans = check(n, m);
	tr(n, m);
	ans = min(ans, check(m, n));
	if (ans <= n * m) {
		printf("%d\n", ans);
		return 0;
	}
	puts("-1");	
    return 0;
}
