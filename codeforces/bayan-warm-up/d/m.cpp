#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100010;

int a[N];
int t[N][20];

int gcd(int a, int b) {
	return a ? gcd(b % a, a) : b;
}

void init(int n) {
	for (int i = 0; i < n; ++i) {
		t[i][0] = a[i];
	}
	for (int j = 1; (1 << j) <= n; ++j) {
		int len = 1 << j;
		int sub = len >> 1;
		int end = n - len + 1;
		for (int i = 0; i < end; ++i) {
			t[i][j] = gcd(t[i][j - 1], t[i + sub][j - 1]);
		}
	}
}

int get(int fm, int to) {
	int len = to - fm + 1;
	int c = 1;
	int j = 0;
	while (c * 2 <= len) {
		c *= 2;
		++j;
	}
	return gcd(t[fm][j], t[to - c + 1][j]);
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
	}
	init(n);
	int m;
	scanf("%d", &m);
	vector<int> q;
	q.reserve(m);
	map<int, li> stat;
	for (int it = 0; it < m; ++it) {
		int x;
		scanf("%d", &x);
		q.push_back(x);
		stat.insert(make_pair(x, 0));
	}
	for (int fm = 0; fm < n; ++fm) {
		int to = n - 1;
		while (to >= fm) {
			int g = get(fm, to);
			int lf = fm, rg = to;
			while (lf + 1 < rg) {
				int mid = (lf + rg) / 2;
				int v = get(fm, mid);
				if (v == g)
					rg = mid;
				else
					lf = mid;
			}
			for (int i = lf; i <= rg; ++i) {
				if (get(fm, i) == g) {
					stat[g] += to - i + 1;
					to = i - 1;
					break;
				}
			}
		}
	}
	for (int g : q) {
		printf("%I64d\n", stat[g]);
	}

    return 0;
}
