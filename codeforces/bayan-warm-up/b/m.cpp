#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 22;

bool used[N][N];

pt dir(char c) {
	switch (c) {
	case '<':
		return pt(0, -1);
	case '>':
		return pt(0, 1);
	case '^':
		return pt(-1, 0);
	case 'v':
		return pt(1, 0);
	default:
		throw 1;
	}
}

int main() {
	int n, m;
	scanf("%d%d\n", &n, &m);	
	string h, v;
	getline(cin, h);
	getline(cin, v);
	for (int si = 0; si < n; ++si) {
		for (int sj = 0; sj < m; ++sj) {
			memset(used, 0, sizeof used);
			used[si][sj] = true;
			queue<pt> q;
			q.push(pt(si, sj));
			int count = 1;
			while (!q.empty()) {
				pt fm = q.front();
				q.pop();
				for (int it = 0; it < 2; ++it) {
					pt move = it == 0 ? dir(h[fm.first]) : dir(v[fm.second]);
					pt nx = pt(fm.first + move.first, fm.second + move.second);
					if (0 <= nx.first && nx.first < n && 0 <= nx.second && nx.second < m && !used[nx.first][nx.second]) {
						used[nx.first][nx.second] = true;
						++count;
						q.push(nx);
					}
				}
			}			
			if (count < n * m) {
				puts("NO");
				return 0;
			}
		}
	}
	puts("YES");

    return 0;
}
