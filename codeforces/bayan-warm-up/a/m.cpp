#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

pt place(int id) {
	if (id < 4) {
		return pt(id + 1, 1);
	}
	id -= 4;
	int c = id / 3;
	c = 3 + (c * 2);
	int r = id % 3;
	if (r < 2)
		r += 1;
	else
		r += 2;
	return pt(r, c);
}

int main() {
	vector<string> t;
	t.push_back("+------------------------+");
	t.push_back("|O.O.O.#.#.#.#.#.#.#.#.|D|)");
	t.push_back("|O.O.O.#.#.#.#.#.#.#.#.|.|");
	t.push_back("|O.......................|");
	t.push_back("|O.O.#.#.#.#.#.#.#.#.#.|.|)");
	t.push_back("+------------------------+");
	int k;
	cin >> k;
	for (int p = 0; p < 34; ++p) {
		pt pos = place(p);
		t[pos.first][pos.second] = p < k ? 'O' : '#';
	}
	for (int i = 0; i < 6; ++i)
		puts(t[i].c_str());
    return 0;
}
