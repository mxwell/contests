#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 30000;
constexpr int M = 350;

int d[N + 1][M];
int st[N + 1];
int g_ans;

inline void Update(int to, int last, int val) {
  if (to <= N) {
    int &tg = d[to][last];
    val += st[to];
    if (tg < 0 || tg < val) {
      tg = val;
      g_ans = max(g_ans, val);
    }
  }
}

int main() {
  int n, first;
  scanf("%d%d", &n, &first);
  for (int i = 0; i < n; ++i) {
    int p;
    scanf("%d", &p);
    ++st[p];
  }
  memset(d, -1, sizeof d);
  g_ans = d[first][250] = st[first];
  for (int i = first; i < N; ++i) {
    for (int j = 1; j < M; ++j) {
      if (d[i][j] >= 0) {
        int val = d[i][j];
        int len = first + j - 250;
        if (len > 1)
          Update(i + len - 1, j - 1, val);
        Update(i + len, j, val);
        Update(i + len + 1, j + 1, val);
      }
    }
  }
  printf("%d\n", g_ans);
  return 0;
}
