#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

vector<int> g[N];
vector<int> r[N];
int paint[N];
int color[N];

bool Dfs(int v) {
  if (color[v] == 2)
    return false;
  color[v] = 1;
  for (int to : g[v]) {
    if (color[to] == 1)
      return true;
    if (color[to] == 2)
      continue;
    if (Dfs(to))
      return true;
  }
  color[v] = 2;
  return false;
}

int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  for (int i = 0; i < m; ++i) {
    int x, y;
    scanf("%d%d", &x, &y);
    --x, --y;
    g[x].push_back(y);
    r[y].push_back(x);
  }
  int ans = 0;
  int paint_id = 1;
  for (int i = 0; i < n; ++i) {
    if (!paint[i]) {
      paint[i] = paint_id;
      queue<int> q;
      q.push(i);
      vector<int> cur_list;
      cur_list.push_back(i);
      while (!q.empty()) {
        int fm = q.front();
        q.pop();
        for (int to : g[fm]) {
          if (!paint[to]) {
            cur_list.push_back(to);
            paint[to] = paint_id;
            q.push(to);
          }
        }
        for (int to : r[fm]) {
          if (!paint[to]) {
            cur_list.push_back(to);
            paint[to] = paint_id;
            q.push(to);
          }
        }
      }

      ans += int(cur_list.size()) - 1;
      for (int p : cur_list) {
        if (Dfs(p)) {
          ++ans;
          break;
        }
      }

      ++paint_id;
    }
  }
  printf("%d\n", ans);
  return 0;
}
