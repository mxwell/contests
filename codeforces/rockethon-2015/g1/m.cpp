#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int p[N];
int cnt[N];
int total;

void rec(int n, int k) {
  if (k <= 0) {
    int t = 0;
    for (int i = 1; i < n; ++i) {
      int val = p[i];
      for (int j = 0; j < i; ++j)
        t += val < p[j];
    }
    ++cnt[t];
    ++total;
    return;
  }
  for (int i = 0; i < n; ++i)
    for (int j = i; j < n; ++j) {
      int len = (j - i + 1);
      int half = len / 2;
      for (int t = 0; t < half; ++t) {
        swap(p[i + t], p[j - t]);
      }
      rec(n, k - 1);
      for (int t = 0; t < half; ++t) {
        swap(p[i + t], p[j - t]);
      }
    }  
}

int main() {
  int n, k;
  scanf("%d%d", &n, &k);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &p[i]);
  }
  rec(n, k);
  li s = 0;
  for (int i = 0; i < N; ++i)
    s += cnt[i] * i;
  cout.precision(9);
  cout << fixed << ld(s) / total << endl;
  return 0;
}
