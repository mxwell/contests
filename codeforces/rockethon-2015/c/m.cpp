#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 10010;
constexpr int CNUM = 5;

int n;
int lf[CNUM], rg[CNUM];
int a[CNUM][N];

/* inclusive */
ld GetOthers(int mask, int to) {
  ld res = 1;
  for (int i = 0; i < n; ++i)
    if (mask & (1 << i))
      res *= a[i][to];
  return res;
}

ld GetOthers3(int mask, int to) {
  ld res = 1;
  bool reached = false;
  for (int i = 0; i < n; ++i)
    if (mask & (1 << i)) {
      if (lf[i] > to)
        return 0;
      if (rg[i] >= to)
        reached = true;
      res *= min(to, rg[i]) - lf[i] + 1;
    }
  if (!reached)
    return 0;
  return res;
}

ld GetOthers2(int mask, int to) {
  ld res = 0;
  for (int i = 0; i < n; ++i)
    if ((mask & (1 << i)) && lf[i] <= to && to <= rg[i]) {
      ld c = 1;
      for (int j = 0; j < n; ++j)
        if (mask & (1 << j)) {
          if (j < i)
            c *= a[j][to];
          else if (j > i)
            c *= a[j][to - 1];
        }
      res += c;
    }
  return res;
}

int main() {
  scanf("%d", &n);
  ld den = 1;
  for (int i = 0; i < n; ++i) {
    scanf("%d%d", &lf[i], &rg[i]);
    den *= (rg[i] - lf[i] + 1);
    int v = 0;
    for (int j = lf[i]; j <= rg[i]; ++j)
      a[i][j] = ++v;
    for (int j = rg[i] + 1; j < N; ++j)
      a[i][j] = v;
  }

  ld total = 0;
  int mend = 1 << n;
  for (int m = 1; m < mend; ++m) {
    int from = 0, to = N;
    int complem = 0;
    int bits = 0;
    for (int i = 0; i < n; ++i) {
      if (m & (1 << i)) {
        from = max(from, lf[i]);
        to = min(to, rg[i]);
        ++bits;
      } else {
        complem |= 1 << i;
      }
    }
    if (from > to)
      continue;
    ld v = 0;
    if (bits == 1) {
      for (int j = 1; j < to; ++j) {
        int mult = (to - max(j + 1, from) + 1);
        ld cnt = GetOthers2(complem, j);
        //printf("  + %d * %f * %d\n", j, double(cnt), mult);
        v += j * cnt * mult;
      }
    } else {
      if (!complem) {
        for (int j = from; j <= to; ++j)
          v += j;
      } else {
        for (int j = from; j <= to; ++j) {
          ld cnt = GetOthers(complem, j - 1);
          //printf("  + %d * %f\n", j, double(cnt));
          v += j * cnt; // ?
        }
      }
    }
    //printf("mask %x, %d - %d, +%f\n", m, from, to, (double) v);
    total += v;
  }
  //cout << total << endl;
  //cout << den << endl;
  cout.precision(9);
  cout << fixed << total / den << endl;

  return 0;
}
