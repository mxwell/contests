#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int a, b, x, y;
  scanf("%d%d%d%d", &a, &b, &x, &y);
  puts(a > b ? "First" : "Second");
  return 0;
}
