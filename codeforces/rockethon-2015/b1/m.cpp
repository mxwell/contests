#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

void Print(const vector<int> p) {
  int n = (int) p.size();
  for (int i = 0; i < n; ++i) {
    printf("%d ", p[i]);
  }
  puts("");
}

void FindBest(int n, int m) {
  vector<int> p(n);
  for (int i = 1; i <= n; ++i) {
    p[i - 1] = i;
  }
  vector<int> best;
  int best_val = 0;
  int count = 0;
  do {
    int val = 0;
    for (int i = 0; i < n; ++i) {
      int minv = 1e9;
      for (int j = i; j < n; ++j) {
        minv = min(minv, p[j]);
        val += minv;
      }
    }
    if (best_val < val) {
      best_val = val;
      count = 1;
      if (count == m)
        best = p;
    } else if (best_val == val) {
      ++count;
      Print(p);
      if (count == m)
        best = p;
    }
  } while (next_permutation(p.begin(), p.end()));
  for (int i = 0; i < n; ++i) {
    printf("%d ", best[i]);
  }
  puts("");
  cerr << count << endl;
}

void Solve(int n, li t) {
  vector<bool> used(n + 1, false);
  int num = 1;
  for (int i = n; i > 0; --i) {
    li d = 1;
    if (i - 2 > 0)
      d <<= i - 2;
    if (t <= d) {
      printf("%d ", num);
      used[num] = true;
    } else {
      t -= d;
    }
    ++num;
  }
  for (int i = n; i > 0; --i)
    if (!used[i])
      printf("%d ", i);
  puts("");
}

int main() {
  int n;
  li m;
  cin >> n >> m;
  Solve(n, m);
  return 0;
}
