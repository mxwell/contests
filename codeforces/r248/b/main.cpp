#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

int a[N][N];
int htrees[N][N];
int vtrees[N][N];

void tupd(int trees[][N], int tid, int pos, int len, int val) {
	int *t = trees[tid];
	for (; pos < len; pos |= pos + 1)
		t[pos] += val;
}

int tsum(int trees[][N], int tid, int lf, int rg) {
	int *t = trees[tid];
	int res = 0;
	for (; rg >= 0; rg = (rg & (rg + 1)) - 1)
		res += t[rg];
	for (--lf; lf >= 0; lf = (lf & (lf + 1)) - 1)
		res -= t[lf];
	return res;
}

int f(int trees[][N], int rows, int cols, int x, int y, int step) {
	int res = 0;
	int top = 0, bot = rows - 1;
	for (int col = y; 0 <= col && col < cols; col += step) {
		if (tsum(trees, col, x, x) == 0)
		//if (a[x][col] == 0)
			break;
		int lf = 0, rg = x - top;
		while (lf + 1 < rg) {
			int mid = (lf + rg) / 2;
			int s = tsum(trees, col, x - mid, x);
			if (s < mid + 1)
				rg = mid;
			else
				lf = mid;
		}
		for (; rg >= lf; --rg)
			if (tsum(trees, col, x - rg, x) == rg + 1)
				break;
		top = x - rg;
		lf = 0, rg = bot - x;
		while (lf + 1 < rg) {
			int mid = (lf + rg) / 2;
			int s = tsum(trees, col, x, x + mid);
			if (s < mid + 1)
				rg = mid;
			else
				lf = mid;
		}
		for (; rg >= lf; --rg)
			if (tsum(trees, col, x, x + rg) == rg + 1)
				break;
		bot = x + rg;
		res = max(res, (abs(col - y) + 1) * (bot - top + 1));
	}
	return res;
}

int main() {
	int n, m, q;
	scanf("%d%d%d", &n, &m, &q);  
	forn (i, n)
		forn (j, m) {
			int t;
			scanf("%d", &t);
			if (t > 0) {
				a[i][j] = t;
				tupd(htrees, j, i, n, 1);
				tupd(vtrees, i, j, m, 1);
			}
		}
	forn (it, q) {
		int type, x, y;
		scanf("%d%d%d", &type, &x, &y);
		--x, --y;
		if (type == 1) {
			int val = 1;
			if (a[x][y] == 1)
				val = -1;
			a[x][y] += val;
			tupd(htrees, y, x, n, val);
			tupd(vtrees, x, y, m, val);
		} else {
			printf("%d\n", max(
				max(f(htrees, n, m, x, y, -1), f(htrees, n, m, x, y, 1)),
				max(f(vtrees, m, n, y, x, -1), f(vtrees, m, n, y, x, 1))));
		}
	}
    return 0;
}
