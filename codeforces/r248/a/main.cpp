#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100010;

int n, m;
int a[N];
vector<int> aj[N];

void upd(int fm, int to) {
	if (fm != to) {
		aj[fm].pb(to);
		aj[to].pb(fm);
	}
}

int main() {
	scanf("%d%d", &n, &m);
	li orig = 0;
	forn (i, m) {
		int cur;
		scanf("%d", &cur);
		a[i] = cur;
		if (i > 0) {
			int prev = a[i - 1];
			upd(cur, prev);
			orig += abs(cur - prev);
		}
	}
	li ans = orig;
	forn (i, m) {
		int old = a[i];
		li old_s = 0;
		li minv = -1;
		vector<int> &ad = aj[old];
		int len = ad.size();
		if (ad.size() == 0)
			continue;
		sort(all(ad));
		li total = 0;
		forn (j, len) {
			old_s += abs(ad[j] - old);
			total += ad[j];
		}
		li s = 0;
		forn (j, len) {
			s += ad[j];
			if (j + 1 < len && ad[j] == ad[j + 1]) {
				continue;
			}
			li cur = total - 2 * s;
			int to = ad[j];
			cur += li(to) * (j + 1 - (len - j - 1));
			if (minv == -1 || minv > cur)
				minv = cur;
		}
		int to = int(total / len);
		s = 0;
		int j = 0;
		while (j < len && ad[j] <= to) {
			s += ad[j];
			++j;
		}
		li cur = total - 2 * s + li(to) * (2 * j - len);
		if (minv == -1 || minv > cur)
			minv = cur;
		int t = j - 1;
		li ts = s;
		for (int k = to - 1; k >= max(to - 2, 0); --k) {
			while (t >= 0 && ad[t] > k) {
				ts -= ad[t];
				--t;
			}
			cur = total - 2 * ts + li(k) * (2 * (t + 1) - len);
			minv = min(minv, cur);
		}
		t = j;
		ts = s;
		for (int k = to + 1; k <= min(n, to + 2); ++k) {
			while (t < len && ad[t] <= k) {
				ts += ad[t];
				++t;
			}
			cur = total - 2 * ts + li(k) * (2 * t - len);
			minv = min(minv, cur);
		}
		ans = min(ans, orig - old_s + minv);
		ad.clear();
	}
	cout << ans << endl;

    return 0;
}
