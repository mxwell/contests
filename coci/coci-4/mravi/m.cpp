#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

vector<pt> g[N];
int rat[N];
bool super[N];
int goal[N];
bool used[N];

ld dfs(int v) {
	used[v] = true;
	if (goal[v] >= 0)
		return goal[v];
	vector<pt> &ch = g[v];
	ld res = 0;
	for (int i = 0; i < int(ch.size()); ++i) {
		int to = ch[i].first;
		if (used[to])
			continue;
		int e = ch[i].second;
		ld tmp = dfs(to);
		if (super[e])
			tmp = sqrt(tmp);
		res = max(res, (tmp * 100) / rat[e]);
	}
	return res;
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n - 1; ++i) {
		int x, y;
		scanf("%d%d", &x, &y);
		--x, --y;
		scanf("%d", &rat[i]);
		int t;
		scanf("%d", &t);
		super[i] = t;
		g[x].push_back(pt(y, i));
		g[y].push_back(pt(x, i));
	}
	for (int i = 0; i < n; ++i) {
		scanf("%d", &goal[i]);
	}
	printf("%.6lf\n", (double) dfs(0));
	
    return 0;
}
