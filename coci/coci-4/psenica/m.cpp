#include<bits/stdc++.h>
  
using namespace std;

typedef long long li;
typedef pair<int, li> pt;  

void answer(bool mirko, vector<pt> &p, int lf, int rg) {
	puts(mirko ? "Mirko" : "Slavko");
	printf("%d %d\n", p[lf].first, p[rg].first);
	exit(0);
}

int main() {
	int n;
	scanf("%d", &n);
	vector<int> a(n);
	for (int i = 0; i < n; ++i) {
		int x;
		scanf("%d", &x);
		a[i] = x;
	}
	sort(a.begin(), a.end());
	vector<pt> p;
	for (int i = 0; i < n; ) {
		int cur = a[i];
		int cnt = 1;
		++i;
		while (i < n && a[i] == cur)
			++i, ++cnt;
		p.push_back(pt(cur, cnt));
	}
	int lf = 0, rg = (int) p.size() - 1;
	if (p.size() < 3)
		answer(false, p, lf, rg);
	while (lf + 1 < rg) {
		li take = min(p[lf].second, p[rg].second);
		p[lf].second -= take;
		p[lf + 1].second += take;
		p[rg].second -= take;
		p[rg - 1].second += take;
		bool result = p[lf].second < p[rg].second;
		if (p[lf].second <= 0)
			++lf;
		if (p[rg].second <= 0)
			--rg;
		if (lf + 1 >= rg) {
			answer(result, p, lf, rg);
		}
	}
	
    return 0;
}
