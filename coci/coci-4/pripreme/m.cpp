#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 300100;

int dur[N];

int main() {
	int n;
	scanf("%d", &n);
	li total = 0;
	for (int i = 0; i < n; ++i) {
		scanf("%d", &dur[i]);
		total += dur[i];
	}
	sort(dur, dur + n);
	reverse(dur, dur + n);
	int s = dur[0];
	for (int i = 1; i < n; ++i) {
		s -= dur[i];
		if (s <= 0)
			break;
	}
	cout << total + max(0, s) << endl;
    return 0;
}
