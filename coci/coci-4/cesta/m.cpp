#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	string s;
	getline(cin, s);
	int zero = -1;
	int total = 0;
	for (int i = 0; i < int(s.size()); ++i) {
		int d = s[i] - '0';
		if (d)
			total += d;
		else
			zero = i;
	}
	if (zero == -1 || total % 3 != 0) {
		puts("-1");
	} else {
		s.erase(zero, 1);
		sort(s.rbegin(), s.rend());
		printf("%s0\n", s.c_str());
	}

    return 0;
}
