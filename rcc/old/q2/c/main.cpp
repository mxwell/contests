#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

li asum(li n) {
	return (n + 1) * n / 2;
}

void calc_parts(li n, li m, vector<li> &c) {
	if (m < n)
		swap(n, m);
	li a = asum(n - 1);
	li b = n * m - a - n;
	if (a > 0)
		c.pb(a);
	if (b > 0)
		c.pb(b);
}

int main() {
	int tests;
	cin >> tests;
	forn (it, tests) {
		li n, m, x, y;
		cin >> n >> m >> x >> y;
		vector<li> c;
		calc_parts(x - 1, y - 1, c);
		calc_parts(x - 1, m - y, c);
		calc_parts(n - x, y - 1, c);
		calc_parts(n - x, m - y, c);
		sort(all(c));
		cout << c.size();
		forn (i, c.size()) {
			cout << ' ' << c[i];
		}
		cout << endl;
	}
    return 0;
}
