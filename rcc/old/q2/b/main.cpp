#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int n, m, k;

void solve() {
	int n1 = min(3, n);
	int m1 = min(3, m);
	int cells = n1 * m1;
	for (int x = 0; x + 2 < cells; ++x) {
		int xi = x / m1;
		int xj = x % m1;
		li cols = 1;
		li rows = 1;
		for (int y = x + 1; y + 1 < cells; ++y) {
			int yi = y / m1;
			int yj = y % m1;
			int cols1 = cols;
			int rows1 = rows;
			if (yi != xi)
				++rows;
			if (yj != xj)
				++cols;
			for (int z = y + 1; z < cells; ++z) {
				int zi = z / m1;
				int zj = z % m1;
				int cols2 = cols;
				int rows2 = rows;
				if (zi != xi && zi != yi)
					++rows;
				if (zj != xj && zj != yj)
					++cols;
				li c = li(n) * m - li(n - rows) * (m - cols) - 3;
				/*rows * m + cols * n - 3;
				c -= rows - 1;
				c -= cols - 1;
				*/
				//cout << c << endl;
				if (c == k) {
					++xi, ++xj;
					++yi, ++yj;
					++zi, ++zj;
					//cout << rows << ' ' << cols << endl;
					printf("%d %d %d %d %d %d\n",
						xi, xj, yi, yj, zi, zj);
					return;
				}
				cols = cols2;
				rows = rows2;
			}
			cols = cols1;
			rows = rows1;
		}
	}
	puts("IMPOSSIBLE");
}

int main() {
	int tests;
	scanf("%d", &tests);
	forn (it, tests) {
		scanf("%d%d%d", &n, &m, &k);
		solve();		
	}
    return 0;
}
