#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	int tests;
	scanf("%d", &tests);
	forn (it, tests) {
		int n, m, p, q, t;
		scanf("%d%d%d%d%d", &n, &m, &p, &q, &t);
		int res = INF;
		int x = t / p;
		int y = t / q;
		forn (i, n + 1) {
			if (i * p > t)
				break;
			int a = n - i;
			int rest_time = t - i * p;
			int b = m - (rest_time / q);
			if (b < 0)
				b = 0;
			int cnt = (a + x - 1) / x + (b + y - 1) / y;
			res = min(res, cnt);
		}
		printf("%d\n", res + 1);
	}
    return 0;
}
