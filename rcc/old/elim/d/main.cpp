#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 5100;
const int MOD = 1000 * 1000 * 1000 + 7;

int n;
int z[N][N];

void solve() {
	z[0][0] = 1;
	for (int m = 1; m < n; ++m) {
		int s = 0;
		if ((n - m) & 1)
			for (int r = 0; r < m; ++r) {
				s = (s + z[m - 1][r]) % MOD;
				z[m][r + 1] = s;
				//printf("z[%d][%d] = %d\n", m, r + 1, s);
			}
		else
			for (int r = m - 1; r >= 0; --r) {
				s = (s + z[m - 1][r]) % MOD;
				z[m][r] = s;
				//printf("z[%d][%d] = %d\n", m, r, s);
			}
	}
	int ans = 0;
	forn (i, n)
		ans = (ans + z[n - 1][i]) % MOD;
	printf("%d\n", ans);
}

int main() {
	scanf("%d", &n);
	solve();
	  
    return 0;
}
