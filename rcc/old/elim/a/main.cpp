#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 110;

char a[N][N];

int main() {
	int tests;
	scanf("%d", &tests);
	for1 (it, tests) {
		int n, m, t, c;
		scanf("%d%d%d%d\n", &n, &m, &t, &c);
		forn (i, n) {
			gets(a[i]);
		}
		int ans = 0;
		for (int j = 0; j < m; ) {
			int maxVal = 0;
			forn (i, n) {
				int len = 0;
				for (int k = j; k < m; ++k)
					if (a[i][k] == '1')
						++len;
					else
						break;
				maxVal = max(maxVal, len);
			}
			ans += maxVal * t;
			j += maxVal;
			if (j < m)
				ans += c;
		}
		printf("%d\n", ans);
	}
  
    return 0;
}
