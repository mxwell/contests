#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 1005;
constexpr int LIM = 1e7;

bool sv[LIM];

vector<int> gen(int k) {
  sv[0] = sv[1] = true;
  vector<int> res(k);
  int pos = 0;
  int i;
  for (i = 2; i + i < LIM; ++i) {
    if (!sv[i]) {
      res[pos++] = i;
      if (pos >= k)
        return res;
      for (int j = i + i; j < LIM; j += i)
        sv[j] = true;
    }
  }
  for (; i < LIM; ++i) {
    if (!sv[i]) {
      res[pos++] = i;
      if (pos >= k)
        return res;
    }
  }
  if (pos < k) {
    //puts("no primes");
    res.resize(pos);
  }
  return res;
}

int no() {
  puts("-1");
  return 0;
}

int main() {
  freopen("parade.in", "rt", stdin);
  freopen("parade.out", "wt", stdout);
  int k, n;
  cin >> k >> n;
  //printf("%d\n", n);
  if (k > sqr(n))
    return no();
  vector<int> ps = gen(k);
  if (k != int(ps.size()))
    return no();
  if (k == sqr(n)) {
    int pos = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        printf("%d ", ps[pos++]);
      }
      puts("");
    }
    return 0;
  }
  if (k <= n) {
    vector<int> row;
    for (int i = 0; i < k; ++i)
      row.push_back(ps[i]);
    for (int i = k; i < n; ++i)
      row.push_back(ps[0]);
    for (int i = 0; i < n; ++i) {
      int pos = i;
      for (int j = 0; j < n; ++j) {
        printf("%d ", row[pos++]);
        if (pos >= n)
          pos = 0;
      }
      puts("");
    }
    return 0;
  }
  vector<vector<int>> ans(n, vector<int>(n));
  vector<int> row(ps.begin(), ps.begin() + n);
  for (int i = 0; i < n; ++i) {
    int j = i;
    int pos = 0;
    for (; j < n; ++j)
      ans[i][pos++] = row[j];
    for (j = 0; j < i; ++j)
      ans[i][pos++] = row[j];
  }
  int s = n;
  for (int i = 0; i < n && s < k; ++i) {
    for (int j = 0; j < n && s < k; ++j)
      ans[i][j] = ps[s++];
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      printf("%d ", ans[i][j]);
    }
    puts("");
  }
  return 0;
}
