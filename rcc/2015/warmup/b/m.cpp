#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

typedef tuple<int, int, int> ev;

enum { MONEY, LOAD, SHIP };

int main() {
  int c1, c2;
  scanf("%d%d", &c1, &c2);
  int n;
  scanf("%d", &n);
  vector<ev> tline;
  for (int i = 0; i < n; ++i) {
    int a, t;
    scanf("%d%d", &a, &t);
    tline.push_back(ev(t, MONEY, a));
  }
  int m;
  scanf("%d", &m);
  for (int i = 0; i < m; ++i) {
    int lf, rg;
    scanf("%d%d", &lf, &rg);
    tline.push_back(make_tuple(lf, LOAD, i));
    tline.push_back(make_tuple(rg, SHIP, i));
  }
  vector<bool> used(m, false);
  sort(tline.begin(), tline.end());
  int money = 0;
  int ans = 0;
  for (ev &e : tline) {
    if (get<1>(e) == MONEY) {
      money += get<2>(e);
    } else if (get<1>(e) == LOAD) {
      if (money >= c1) {
        ++ans;
        money -= c1;
        used[get<2>(e)] = true;
      }
    } else {
      if (!used[get<2>(e)] && money >= c2) {
        ++ans;
        money -= c2;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
