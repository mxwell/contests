#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

int main() {
  int n, k;
  scanf("%d%d", &n, &k);
  vector<int> a(n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  sort(a.begin(), a.end());
  vector<bool> used(n);
  for (int i = 0; i < n; ++i) {
    bool prev = i == 0 || a[i - 1] != a[i];
    if (prev) {
      printf("%d ", a[i]);
      used[i] = true;
      --k;
      if (k <= 0)
        break;
    }
  }
  if (k > 0) {
    for (int i = 0; i < n; ++i)
      if (!used[i]) {
        printf("%d ", a[i]);
        --k;
        if (k <= 0)
          break;
      }
  }
  printf("\n");
  return 0;
}
