#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
template<class T> inline T Abs(const T& x) { return x > 0 ? x : -x; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 500100;

int a[N];
int pres[N];

void init(int n) {
  pres[0] = a[0];
  for (int i = 1; i < n; ++i) {
    pres[i] = a[i] + pres[i - 1];
  }
}

inline int GetSum(int fm, int to) {
  int res = pres[to];
  if (fm)
    res -= pres[fm - 1];
  return res;
}

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  sort(a, a + n);
  init(n);
  int q;
  scanf("%d", &q);
  li acc = 0;
  for (int it = 0; it < q; ++it) {
    int x;
    scanf("%d", &x);
    acc += x;
    li ans = 0;
    if (acc < 0) {
      int pos = (int) (upper_bound(a, a + n, int(-acc)) - a);
      if (pos > 0)
        ans += Abs(GetSum(0, pos - 1) + acc * pos);
      if (pos < n)
        ans += Abs(GetSum(pos, n - 1) + acc * (n - pos));
    } else if (acc >= 0) {
      int pos = (int) (lower_bound(a, a + n, int(-acc)) - a);
      if (pos > 0)
        ans += Abs(GetSum(0, pos - 1) + acc * pos);
      if (pos < n)
        ans += Abs(GetSum(pos, n - 1) + acc * (n - pos));
    }
    printf("%lld\n", ans);
  }
  return 0;
}
