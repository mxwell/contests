#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

li a[N];
li sums[N];

int main() {
  int tests;
  scanf("%d", &tests);
  while (tests-- > 0) {
    int n;
    li m;
    li ans = 0;
    scanf("%d%lld", &n, &m);
    for (int i = 0; i < n; ++i) {
      scanf("%lld", &a[i]);
      if (a[i] >= m)
        a[i] %= m;
    }
    multiset<li> right;
    li s = 0;
    for (int i = 0; i < n; ++i) {
      s += a[i];
      while (s >= m)
        s -= m;
      sums[i] = s;
      ans = max(ans, max(a[i], sums[i]));
      right.insert(s);
    }    
    for (int i = 0; i + 1 < n; ++i) {
      right.erase(right.find(sums[i]));
      li cur = a[i];
      li val = (sums[i] + m - 1 - cur) % m;
      cur += m - sums[i];
      cur %= m;
      auto it = right.lower_bound(val);
      if (it != right.end() && *it == val) {
          ans = max(ans, (cur + *it) % m);
      } else if (it != right.begin()) {
        --it;
        ans = max(ans, (cur + *it) % m);
      }
      val = (sums[i] + m - 1) % m;
      it = right.lower_bound(val);
      if (it != right.end() && *it == val) {
        ans = max(ans, (cur + *it) % m);
      } else if (it != right.begin()) {
        --it;
        ans = max(ans, (cur + *it) % m);
      }
    }
    printf("%lld\n", ans);
  }
  return 0;
}
