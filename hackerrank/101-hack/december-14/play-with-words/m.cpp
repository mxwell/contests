#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 3010;

string line;
int d[N][N];

int calc(int fm, int to) {
	int &tg = d[fm][to];
	if (tg == -1) {
		if (fm == to) {
			tg = 1;
		} else if (fm + 1 == to) {
			tg = 1 + (line[fm] == line[to]);
		} else {
			if (line[fm] == line[to]) {
				tg = max(tg, 2 + calc(fm + 1, to - 1));
			}
			tg = max(tg, calc(fm + 1, to));
			tg = max(tg, calc(fm, to - 1));
		}
	}
	return tg;
}

int main() {
	getline(cin, line);
	memset(d, -1, sizeof d);
	int n = (int) line.size();
	calc(0, n - 1);
	int ans = 0;
	for (int i = 1; i < n; ++i) {
		ans = max(ans, calc(0, i - 1) * calc(i, n - 1));
	}
	cout << ans << endl;
    return 0;
}
