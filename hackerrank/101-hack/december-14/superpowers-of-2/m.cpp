#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

li phi(li n) {
	li ans = n;
	if ((n & 1) == 0) {
		while ((n & 1) == 0)
			n >>= 1;
		ans -= ans >> 1;
	}
	for (li i = 3; i * i <= n; i += 2)
		if (n % i == 0) {
			n /= i;
			while (n % i == 0)
				n /= i;
			ans -= ans / i;
		}
	if (n > 1)
		ans -= ans / n;
	return ans;
}

li binp(li x, li y, li m) {
	li ans = 1;
	while (y)
		if (y & 1) {
			ans = (ans * x) % m;
			--y;
		} else {
			x = (x * x) % m;
			y >>= 1;
		}
	return ans;
}

int main() {
	li a, m;
	cin >> a >> m;
	if (a < 63) {
		cout << binp(2, 1ll << a, m) << endl;
	} else {
		int p = phi(m);
		int c = binp(2, a, p) + p;
		cout << binp(2, c, m) << endl;
	}

    return 0;
}
