#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;
constexpr li MODULO = 1e9 + 7;

bool Check(int v, int mask, int used) {
  if (v < 0) {
    return true;
  }
  int mv = 1 << v;
  if (used & mv)
    return Check(v - 1, mask, used);
  if (mask & mv) {
    for (int i = v - 1; i >= 0; --i) {
      int mi = 1 << i;
      if ((used & mi) == 0) {
        if (Check(v - 1, mask, used | mi)) {
          return true;
        }
      }
    }
  }
  return false;
}

void Nv(int n) {
  int mend = 1 << (n - 2);
  int ans = 0;
  for (int mask = 0; mask < mend; ++mask) {
    int actual = (1 << (n - 1)) | (mask << 1);
    if (Check(n - 1, actual, 0)) {
      //printf("good: 0x%X\n", actual);
      ++ans;
    }
  }
  printf("%d\n", ans);
}

void factor(int n, vector<int> &to) {
  if (n % 2 == 0) {
    while ((n & 1) == 0) {
      to.push_back(2);
      n >>= 1;
    }
  }
  for (int i = 3; i * i <= n; i += 2) {

  }
}

int bpow(li x, int y) {
  li res = 1;
  while (y)
    if (y & 1) {
      res = (res * x) % MODULO;
      --y;
    } else {
      x = (x * x) % MODULO;
      y >>= 1;
    }
  return int(res % MODULO);
}

void MultLi(li &tg, li x) {
  tg = (tg * x) % MODULO;
}

li d[N];
void init() {
  d[0] = 1;
  for (int i = 1; i < N; ++i) {
    d[i] = d[i - 1];
    int x = i - 1;
    MultLi(d[i], 4 * x + 6);
    MultLi(d[i], bpow(x + 2, MODULO - 2));
  }
}

int main() {
  init();
  int tests;
  scanf("%d", &tests);
  while (tests-- > 0) {
    int n;
    scanf("%d", &n);
    //Nv(n);
    cout << ((n % 2 == 0) ? d[n / 2 - 1] : 0) << endl;      
  }
  return 0;
}
