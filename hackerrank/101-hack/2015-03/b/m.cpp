#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int tests;
  scanf("%d\n", &tests);
  while (tests-- > 0) {
    string s;
    getline(cin, s);
    int n = int(s.size());
    int rc = 0, bc = 0;
    int runs = 0;
    char prev = 0;
    for (char &ch : s) {
      if (ch == 'R')
        ++rc;
      else
        ++bc;
      if (ch != prev) {
        prev = ch;
        ++runs;
      }
    }
    if (rc == 0 || bc == 0) {
      printf("%d\n", runs);
      continue;
    }
    if (rc == 1 || bc == 1) {
      puts("2");
      continue;
    }
    int ans = runs;
    for (int i = 0; i < n; ++i) {
      char ch = s[i];
      bool before = i == 0 || s[i - 1] != ch;
      bool after = i + 1 >= n || s[i + 1] != ch;
      if (before && after) {
        if (!(i == 0 || i + 1 >= n))
          ans = min(ans, runs - 2);
        else
          ans = min(ans, runs - 1);
      }
    }
    printf("%d\n", ans);
  }
  return 0;
}
