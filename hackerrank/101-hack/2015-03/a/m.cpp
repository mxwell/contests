#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

int a[N];

void Solve() {
  int n;
  scanf("%d", &n);
  int s = 0;
  int miv = 1e9;
  bool used = false;
  for (int i = 0; i < n; ++i) {
    scanf("%d", &a[i]);
  }
  for (int i = 0; i < n; ++i) {
    int x = a[i];
    miv = min(miv, x);
    s += x;
    if (s < 0) {
      if (!used) {
        s -= 2 * miv;
        used = true;
      }
      if (s < 0) {
        printf("%d\n", i + 1);
        return;
      }
    }
  }
  puts("She did it!");
}

int main() {
  int tests;
  scanf("%d", &tests);
  while (tests-- > 0) {
    Solve();
  }
  return 0;
}
