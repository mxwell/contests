#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 1000100;

int n;
li m;
li myval[N];

inline li MultLi(li x, li y) {
  return (x * y) % m;
}

li tree[N * 5];

void BuildTree(int id, int lf, int rg) {
  if (lf == rg) {
    tree[id] = MultLi(lf, 1);
  } else {
    int mid = (lf + rg) / 2;
    int tolf = id * 2;
    int torg = tolf + 1;
    BuildTree(tolf, lf, mid);
    BuildTree(torg, mid + 1, rg);
    tree[id] = MultLi(tree[tolf], tree[torg]);
  }
}

li GetMultiRec(int id, int lf, int rg, int fm, int to) {
  if (lf == fm && rg == to)
    return tree[id];
  int mid = (lf + rg) / 2;
  int tolf = id * 2;
  int torg = tolf + 1;
  li result = 1;
  if (fm <= mid)
    result = MultLi(result, GetMultiRec(tolf, lf, mid, fm, min(to, mid)));
  if (to > mid)
    result = MultLi(result, GetMultiRec(torg, mid + 1, rg, max(fm, mid + 1), to));
  return result;
}

li GetMulti(int fm, int to) {
  return GetMultiRec(1, 0, n, fm, to);
}

void init() {
  BuildTree(1, 0, n);
  myval[0] = 1;
  int half = n / 2;
  for (int i = 1; i <= half; ++i) {
    myval[i] = MultLi(myval[i - 1], GetMulti(i + 1, n - i + 1));
  }
}

int main() {
  int ts;
  scanf("%d", &ts);
  while (ts-- > 0) {
    int q, local_m;
    scanf("%d%d%d", &n, &local_m, &q);
    m = local_m;
    init();
    int r;
    for (int i = 0; i < q; ++i) {
      scanf("%d", &r);
      printf("%lld\n", myval[min(r, n - r)]);
    }
  }

  return 0;
}
