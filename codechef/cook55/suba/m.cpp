#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100100;

int a[N];
int start[N];
li pres[N];
li best[N];
bool present[N];

inline bool Closing(char c) {
  return c == ')' || c == ']' || c == '}' || c == '>';
}

inline bool Opening(char c, char match) {
  return (c == '(' && match == ')') ||
         (c == '[' && match == ']') ||
         (c == '{' && match == '}') ||
         (c == '<' && match == '>');
}

void init(int n) {
  pres[0] = a[0];
  for (int i = 1; i < n; ++i) {
    pres[i] = a[i] + pres[i - 1];
  }
}

li GetSum(int fm, int to) {
  if (fm)
    return pres[to] - pres[fm - 1];
  return pres[to];
}

li ProcessString(const string &s) {
  int n = int(s.size());
  li ans = 0;
  memset(start, -1, n * sizeof(int));
  for (int i = 0; i < n; ++i) {
    if (Closing(s[i])) {
      li &cur = best[i];
      cur = 0;
      present[i] = false;
      int prev = i - 1;
      while (prev >= 0 && start[prev] >= 0) {
        prev = start[prev] - 1;
      }
      if (prev >= 0 && Opening(s[prev], s[i])) {
        li base = GetSum(prev, i);
        cur = base;
        present[i] = true;
        int np = prev - 1;
        while (np >= 0 && start[np] >= 0) {
          if (present[np])
            cur = max(cur, base + best[np]);
          base += GetSum(start[np], np);
          prev = start[np];
          np = start[np] - 1;
        }
        start[i] = prev;
      }
      ans = max(ans, cur);
    }
  }
  return ans;
}

int main() {
  int ts;
  scanf("%d", &ts);
  string s;
  while (ts-- > 0) {
    int n;
    scanf("%d\n", &n);
    getline(cin, s);
    for (int i = 0; i < n; ++i) {
      scanf("%d", &a[i]);
    }
    init(n);
    cout << ProcessString(s) << endl;
  }
  return 0;
}
