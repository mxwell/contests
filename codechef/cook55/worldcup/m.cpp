#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;
constexpr li MODULO = 1e9 + 7;

li d[310][2000][10];

void AddLi(li &tg, li x) {
  tg = (tg + x) % MODULO;
}

li Rec(int r, int b, int let) {
  li &tg = d[b][r][let];
  if (tg < 0) {
    tg = 0;
    if (b == 0) {
      if (r == 0)
        tg = 1;
    } else {
      AddLi(tg, Rec(r, b - 1, let));
      if (r >= 4) {
        AddLi(tg, Rec(r - 4, b - 1, let));
        if (r >= 6)
          AddLi(tg, Rec(r - 6, b - 1, let));
      }
      if (let) {
        AddLi(tg, Rec(r, b - 1, let - 1));
      }
    }
  }
  return tg;
}

int main() {
  int ts;
  scanf("%d", &ts);
  memset(d, -1, sizeof d);
  while (ts-- > 0) {
    int r, b, let;
    scanf("%d%d%d", &r, &b, &let);
    if (r > b * 6) {
      puts("0");
      continue;
    }
    printf("%d\n", int(Rec(r, b, let) % MODULO));
  }
  return 0;
}
