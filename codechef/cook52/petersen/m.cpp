#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 10;

int g[N][3];
char mark[N];

void g_set(int id, int a, int b, int c) {
	int i = 0;
	g[id][i++] = a;
	g[id][i++] = b;
	g[id][i++] = c;
}

void init() {
	for (int i = 0; i < N; ++i) {
		mark[i] = 'A' + (i % 5);
	}
	g_set(0, 4, 1, 5);
	g_set(1, 0, 2, 6);
	g_set(2, 1, 3, 7);
	g_set(3, 2, 4, 8);
	g_set(4, 3, 0, 9);
	g_set(5, 7, 8, 0);
	g_set(6, 8, 9, 1);
	g_set(7, 9, 5, 2);
	g_set(8, 5, 6, 3);
	g_set(9, 6, 7, 4);
}

bool go(const string &p, int s, vector<int> &ans) {
	int v = s;
	int n = (int) p.size();
	ans.clear();
	ans.push_back(v);
	for (int i = 1; i < n; ++i) {
		bool ok = false;
		for (int j = 0; j < 3; ++j) {
			int to = g[v][j];
			if (mark[to] == p[i]) {
				ok = true;
				v = to;
				ans.push_back(v);
				break;
			}			
		}		
		if (!ok)
			return false;
	}
	return true;
}

int main() {
	init();
	int tests;
	scanf("%d\n", &tests);
	vector<int> ans;
	for (int it = 0; it < tests; ++it) {
		string s;
		getline(cin, s);
		bool ok = false;
		for (int i = 0; i < N; ++i) {
			if (mark[i] == s[0] && go(s, i, ans)) {
				for (int j = 0; j < (int) ans.size(); ++j)
					printf("%d", ans[j]);
				puts("");
				ok = true;
				break;
			}
		}
		if (!ok)
			puts("-1");
	}	
    return 0;
}
