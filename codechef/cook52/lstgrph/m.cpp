#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 200100;

vector<pt> g[N];
bool used[N];
int val[N];
int belong[N];

int main() {
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 0; i < m; ++i) {
		int x, y, mark;
		scanf("%d%d%d", &x, &y, &mark);
		--x, --y;
		g[x].push_back(make_pair(y, mark));
		g[y].push_back(make_pair(x, mark));
	}
	vector<int> comps;
	for (int s = 0; s < n; ++s) {
		if (used[s])
			continue;
		used[s] = true;
		belong[s] = s;
		comps.push_back(s);
		queue<int> q;
		q.push(s);
		val[s] = 0;
		while (!q.empty()) {
			int nx = q.front();
			q.pop();
			int cv = val[nx];
			vector<pt> &ch = g[nx];
			for (int i = 0; i < (int) ch.size(); ++i) {
				pt &to = ch[i];
				if (used[to.first]) {
					if (val[to.first] != (cv ^ to.second)) {
						puts("-1");
						return 0;
					}
				} else {
					val[to.first] = cv ^ to.second;
					used[to.first] = true;
					belong[to.first] = s;
					q.push(to.first);
				}
			}
		}
	}
	int last = comps.back();
	--k;
	for (int i = 0; i < n; ++i) {
		if (belong[i] == last) {
			val[i] ^= k;
		}
	}
	for (int i = 0; i < n; ++i) {
		printf("%d ", val[i]);
	}
	puts("");
    return 0;
}
