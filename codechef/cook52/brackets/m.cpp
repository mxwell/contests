#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	int n;
	scanf("%d\n", &n);
	for (int i = 0; i < n; ++i) {
		string s;
		getline(cin, s);
		int b = 0;
		int maxb = 0;
		for (int i = 0, end = s.size(); i < end; ++i) {
			if (s[i] == '(')
				++b;
			else
				--b;
			maxb = max(maxb, b);
		}
		string ans;
		for (int i = 0; i < maxb; ++i) {
			ans.push_back('(');
		}
		for (int i = 0; i < maxb; ++i) {
			ans.push_back(')');
		}
		puts(ans.c_str());
	}
    return 0;
}
