#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

pt Diff(const pt &u, const pt &v) {
  return {u.first - v.first, u.second - v.second};
}

inline li VectorProduct(pt u, pt v) {
  return u.first * li(v.second) - u.second * li(v.first);
}

inline li ScalarProduct(pt u, pt v) {
  return u.first * li(v.first) + u.second * li(v.second);
}

void Inc(int &rg, int m) {
  ++rg;
  if (rg >= m)
    rg -= m;
}

#define DBG1

int Calc(const vector<pair<ld, pt>> &ps, int m) {
  pt c{0, 0};
  int res = 1e9;
  int rg = 1;
  for (int i = 1; i < m; ++i) {
    if (fabs(ps[i].first - ps[0].first) > EPS) {
      rg = i;
      break;
    }
  }
  for (int lf = 0; lf < m; ++lf) {
    int counter = 0;
    pt u = ps[lf].second;
    pt uc = Diff(c, u);
    while (VectorProduct(Diff(ps[rg].second, c), uc) > 0) {
      Inc(rg, m);
      ++counter;
      if (counter > 2 * m) {
        printf("cycled: lf %d, rg %d, product %lld\n", lf, rg, VectorProduct(Diff(ps[rg].second, c), uc));
        exit(0);
      }
    }
#ifdef DBG
    printf("lf, rg, cnt %d, %d, (vp %d)\n", lf, rg, int(VectorProduct(Diff(ps[rg].second, c), uc)));
#endif
    int cur;
    if (lf < rg) {
      cur = rg - lf - 1;
    } else {
      cur = m - 1 - lf + rg;
    }
    res = min(res, cur);
  }
  return res;
}

li gcd(li x, li y) {
  while (x) {
    y %= x;
    swap(x, y);
  }
  return y;
}

struct Line {
  li a, b, c;
  Line() {}
  Line(pt u, pt v) {
    a = u.second - v.second;
    b = v.first - u.first;
    c = -(a * u.first + b * u.second);
    li g = gcd(c, gcd(a, b));
    a /= g;
    b /= g;
    c /= g;
  }
  void Print() {
    printf("Line: %lld %lld %lld\n", a, b, c);
  }
};

bool Collinear(const vector<pt> &ps, int n) {
  Line line(ps[0], ps[1]);
  //line.Print();
  for (int i = 2; i < n; ++i) {
    Line cur(ps[0], ps[i]);
    //cur.Print();
    if (!(cur.a == line.a && cur.b == line.b && cur.c == line.c))
      return false;
  }
  return true;
}

vector<int> Solve(const vector<pt> &ps, int n) {
  if (n <= 3) {
    return vector<int>(n, 0);
  }
  vector<int> result(n);

  for (int i = 0; i < n; ++i) {
    vector<pair<ld, pt>> os;
    pt c = ps[i];
    for (int j = 0; j < n; ++j) {
      if (j == i)
        continue;
      pt shifted = Diff(ps[j], c);
      ld angle = atan2(ld(shifted.second), ld(shifted.first));
      os.push_back({ angle, shifted });
    }
    sort(os.begin(), os.end());
#ifdef DBG
    printf("center in (%d,%d)\n", c.first, c.second);
    printf("points:");
    for (auto &t : os) {
      printf(" (%d,%d)", t.second.first + c.first, t.second.second + c.second);
    }
    puts("");
#endif 
    result[i] = Calc(os, n - 1);
  }
  return result;
}

int main() {
  int ts;
  scanf("%d", &ts);
  for (int it = 1; it <= ts; ++it) {
    int n;
    scanf("%d", &n);
    vector<pt> ps;
    for (int i = 0; i < n; ++i) {
      int x, y;
      scanf("%d%d", &x, &y);
      ps.push_back({x, y});
    }
    vector<int> ans = Solve(ps, n);
    printf("Case #%d:\n", it);
    for (int i = 0; i < n; ++i) {
      printf("%d\n", ans[i]);
    }
    cerr << "done " << it << endl;
  }
  return 0;
}
