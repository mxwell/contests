#include <cstdio>
#include <iostream>
#include <string>
#include <map>
#include <utility>
#include <vector>

using namespace std;

int main() {
	int tests;
	cin >> tests;
	string trash;
	for (int i = 0; i < tests; ++i) {
		int n;
		cin >> n;
		getline(cin, trash);
		map<string, int> engines;
		for (int j = 0; j < n; ++j)	{
			string s;
			getline(cin, s);
			engines.insert(make_pair(s, engines.size()));
		}
		/*cerr << "there are " << n << " engine(s)" << endl;*/
		int m;
		cin >> m;
		getline(cin, trash);
		vector<int> seq(m);
		vector<vector<int> > occ(n);
		for (int j = 0; j < m; ++j) {
			string s;
			getline(cin, s);
			int id = engines[s];
			seq[j] = id;
			/*cerr << j << "] " << id << endl;*/
			occ[id].push_back(j);
		}
		vector<int> occ_pos(n, 0);
		vector<int> ans(m + 1, -1);
		for (int j = 0; j < m; ++j) {
			int prev = 0;
			if (j > 0) {
				if (ans[j] == -1)
					continue;
				prev = ans[j] + 1;
			}
			int cur = seq[j];
			for (int k = 0; k < n; ++k)
				if (k != cur) {
					int next_occ = m;
					while (occ_pos[k] < (int) occ[k].size()) {
						int val = occ[k][occ_pos[k]];
						if (val > j) {
							next_occ = val;
							break;
						}
						++occ_pos[k];
					}
					/*cerr << "using engine #" << k 
						<< " till query #" << next_occ << endl;*/
					if (ans[next_occ] == -1 || ans[next_occ] > prev)
						ans[next_occ] = prev;
				}
		}
		if (ans[m] == -1)
			ans[m] = 0;
		cout << "Case #" << i + 1 << ": " << ans[m] << endl;
	}
	return 0;
}