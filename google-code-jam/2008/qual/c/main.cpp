#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef double ld;
typedef pair<ld, ld> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

ld get_cross(ld r, ld x) {
	assert(x > -EPS);
	if (x > r)
		return -2;
	return sqrt(sqr(r) - sqr(x));
}

bool between(ld fm, ld to, ld x) {
	if (fm > to)
		swap(fm, to);
	return fm - EPS < x && x < to + EPS;
}

ld tr_area(const pt &a, const pt &b, const pt &c) {
	ld res = fabs((b.first - a.first) * (c.second - a.second) 
		- (b.second - a.second) * (c.first - a.first)) / 2;
	return res;
}

ld sector_area(ld r, ld ang) {
	ld res = ang * sqr(r) / 2;
	return res;
}

ld get_area(ld r, ld lf, ld rg, ld tp, ld bt) {
	ld ax, ay, bx, by;
	ld lf_y = get_cross(r, lf);
	ld rg_y = get_cross(r, rg);
	ld tp_x = get_cross(r, tp);
	ld bt_x = get_cross(r, bt);
	ld h = fabs(tp - bt);
	ld w = fabs(rg - lf); 
	ax = -2;
	if (lf_y > -1 && between(bt, tp, lf_y)) {
		ax = lf;
		ay = lf_y;
	} else if (tp_x > -1 && between(lf, rg, tp_x)) {
		ax = tp_x;
		ay = tp;
	}
	bx = -2;
	if (rg_y > -1 && between(bt, tp, rg_y)) {
		bx = rg;
		by = rg_y;
	} else if (bt_x > -1 && between(lf, rg, bt_x)) {
		bx = bt_x;
		by = bt;
	}
	assert(ax * bx > -EPS);
	if (ax < -1) {
		if (sqr(lf) + sqr(bt) > sqr(r))
			return 0;
		return fabs(h * w);
	}
	ld ang_a = atan2(ay, ax);
	ld ang_b = atan2(by, bx);
	ld s_area = sector_area(r, fabs(ang_a - ang_b));
	pt center = mp(0.0, 0.0);
	s_area -= tr_area(center, mp(ax, ay), mp(ax, by));
	s_area -= tr_area(center, mp(ax, by), mp(bx, by));
	return fabs(h * (ax - lf) + w * (by - bt) - (ax - lf) * (by - bt) + s_area);
}

void testing() {
	printf("tr_area: %lf\n", tr_area(mp(0, 0), mp(3, 0), mp(0, 4)));
	printf("sector_area: %lf\n", sector_area(4, PI / 2));
}

int main() {
	int tests;
	scanf("%d", &tests);
	for1(it, tests) {
		ld f, R, r, t, g;
		scanf("%lf%lf%lf%lf%lf", &f, &R, &t, &r, &g);
		ld leftmost = r;
		ld rightmost = R - t;
		ld step = g + r * 2;
		ld r_inner = R - t - f;
		ld miss_area = 0;
		if (g / 2 > f - EPS) {
			for (ld lf = leftmost; lf < rightmost; lf += step) {
				for (ld bt = leftmost; bt < rightmost; bt += step) {
					if (sqr(lf + f) + sqr(bt + f) > sqr(rightmost))
						continue;
					miss_area += get_area(r_inner, lf + f, lf + g - f, bt + g - f, bt + f);
				}
			}
		}
		ld quarter_area = PI * sqr(R) / 4;
		printf("Case #%d: %.6lf\n", it, 1 - miss_area / quarter_area);
	}
  
    return 0;
}
