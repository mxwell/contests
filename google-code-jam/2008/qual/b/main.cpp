#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int conv(int h, int m) {
	return h * 60 + m;
}

int read_time() {
	int h, m;
	scanf("%d:%d", &h, &m);
	return conv(h, m);
}

pt read_train() {
	int dep = read_time();
	int arr = read_time();
	return mp(dep, arr);
}

bool spec_comp(const pt &u, const pt &v) {
	if (u.second == v.second)
		return u.first < v.first;
	return u.second < v.second;
}

int go_along(vector<int> &a) {
	int res = 0;
	int acc = 0;
	forn (i, a.size()) {
		int &cur = a[i];
		if (cur < 0) {
			if (acc < -cur) {
				res += -cur - acc;
				acc = 0;
			} else {
				acc -= -cur;
			}
		} else {
			acc += cur;
		}
	}
	return res;
}

pt another_way(int turn, vector<pt> &fa, vector<pt> &fb) {
	int daylen = 24 * 60;
	vector<int> a(daylen, 0), b(daylen, 0);
	forn (i, fa.size()) {
		--a[fa[i].first];
		int t = turn + fa[i].second;
		if (t < (int) b.size())
			++b[t];
	}
	forn (i, fb.size()) {
		--b[fb[i].first];
		int t = turn + fb[i].second;
		if (t < (int) a.size())
			++a[t];
	}
	return mp(go_along(a), go_along(b));
}

#ifdef LOCAL_RUN
int main() {
	int tests;
	scanf("%d", &tests);
	for1(it, tests) {
		int turn;
		scanf("%d", &turn);
		int na, nb;
		scanf("%d%d\n", &na, &nb);
		vector<pt> fa(na), fb(nb);
		forn (i, na)
			fa[i] = read_train();
		forn (i, nb)
			fb[i] = read_train();
		pt alt = another_way(turn, fa, fb);
		sort(all(fa));
		sort(all(fb));
		/*
		forn (i, fa.size())
			printf("%d -> %d\n", fa[i].first, fa[i].second);
		puts("--");
		forn (i, fb.size())
			printf("%d -> %d\n", fb[i].first, fb[i].second);
		puts("--");
		*/
		queue<pt> q;
#define FROM_A 0
#define FROM_B 1
		int ai = 0, bi = 0;
		while (ai < (int) fa.size() || bi < (int) fb.size()) {
			if (ai < (int) fa.size()) {
				if (bi < (int) fb.size()) {
					if (fa[ai].first <= fb[bi].first) {
						q.push(mp(FROM_A, ai++));
					} else {
						q.push(mp(FROM_B, bi++));
					}
				} else {
					q.push(mp(FROM_A, ai++));
				}
			} else if (bi < (int) fb.size()) {
				q.push(mp(FROM_B, bi++));
			}
		}
		int ans_a = 0, ans_b = 0;
		multiset<int> at_a, at_b;
		while (!q.empty()) {
			pt nx = q.front();
			q.pop();
			int dep = nx.first == FROM_A ?
					fa[nx.second].first : fb[nx.second].first;
			int ready = (nx.first == FROM_A ?
					fa[nx.second].second : fb[nx.second].second) + turn;
			bool found = false;
			if (nx.first == FROM_A) {
				if (!at_a.empty()) {
					auto a = at_a.begin();
					if (*a <= dep) {
						//printf("A - %d: using train ready since %d\n", dep % 60, *a % 60);
						at_a.erase(a);
						found = true;
					}
				}
				if (!found) {
					/*
					printf("A - %d: no train\n", dep % 60);
					if (!at_a.empty()) {
						printf("\t at A: %d\n", *(at_a.begin()) % 60);
					}
					*/
					++ans_a;
				}
				at_b.insert(ready);
				//printf("%d: train will be ready at B in %d\n", dep % 60, ready % 60);
			} else {
				if (!at_b.empty()) {
					auto b = at_b.begin();
					if (*b <= dep) {
						//printf("B - %d: using train ready since %d\n", dep % 60, *b % 60);
						at_b.erase(b);
						found = true;
					}
				}
				if (!found) {
					/*
					printf("B - %d: no train\n", dep % 60);
					if (!at_b.empty()) {
						printf("\t at B: %d\n", *(at_b.begin()) % 60);
					}
					*/
					++ans_b;
				}
				at_a.insert(ready);
				//printf("%d: train will be ready at A in %d\n", dep % 60, ready % 60);
			}
		}
		printf("Case #%d: %d %d\n", it, ans_a, ans_b);
		if (!(alt.first == ans_a && alt.second == ans_b)) {
			puts("Error");
		}
	}
  
    return 0;
}
#endif