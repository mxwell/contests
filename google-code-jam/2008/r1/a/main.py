tests = int(raw_input())

def run_case(it):
  n = int(raw_input())
  x = [int(num) for num in raw_input().split()]
  x.sort()
  y = [int(num) for num in raw_input().split()]
  y.sort()
  y.reverse()
  ans = 0
  for (i, j) in zip(x, y):
    ans += i * j
  print "Case #%d: %d" % (it, ans)

for it in range(0, tests):
  run_case(it + 1)