import math


def cnk(n, k):
  res = 1
  for i in range(max(k, n - k) + 1, n + 1):
    res *= i
  for i in range(1, min(k, n - k) + 1):
    res /= i
  return res


def bpow(x, y):
  res = 1
  while y > 0:
    if (y & 1) > 0:
      res *= x
      y -= 1
    else:
      x *= x
      y /= 2
  return res


def mult(b):
  lf = max(1, int(b * math.sqrt(5)) - 100)
  rg = (lf + 100) * 2
  for it in range(0, 200):
    mid = (lf + rg) / 2
    if b * b * 5 < mid * mid:
      rg = mid
    else:
      lf = mid
  #print "%d x sqrt(5) = lf, rg: %d %d" % (b, lf, rg)
  return lf


def run_case(it):
  n = int(raw_input())
  odd, even = 0, 0
  for i in range(1, n + 1, 2):
    odd += cnk(n, i) * bpow(3, n - i) * bpow(5, i / 2)
  #print "odd %d" % odd
  for i in range(0, n + 1, 2):
    even += cnk(n, i) * bpow(3, n - i) * bpow(5, i / 2)
  #print "even %d" % even
  ans = (even + mult(odd))
  #print "raw %d" % ans
  ans %= 1000
  print "Case #%d: %03d" % (it, ans)


def add(x, y):
  return (x + y) % 1000

def mult(x, y):
  return (x * y) % 1000


def matrix_mult(a, b):
  c = [[0, 0], [0, 0]]
  for i in range(0, 2):
    for j in range(0, 2):
      for k in range(0, 2):
        c[i][j] = add(c[i][j], mult(a[i][k], b[k][j]))
  return c


def get_A(n):
  a = [[3, 5], [1, 3]]
  res = [[1, 0], [0, 1]]
  while n > 0:
    if (n & 1) > 0:
      res = matrix_mult(res, a)
      n -= 1
    else:
      a = matrix_mult(a, a)
      n /= 2
  return res


def run_hard(it):
  n = int(raw_input())
  An = get_A(n)
  #print An
  an = An[0][0]
  bn = An[1][0]
  xn = mult(2, an)
  xn = (xn + 999) % 1000
  print "Case #%d: %03d" % (it, xn)


def run():
  tests = int(raw_input())
  for it in range(0, tests):
    run_hard(it + 1)

if __name__ == '__main__':
  run()