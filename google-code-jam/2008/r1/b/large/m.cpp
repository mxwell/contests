#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 2010;

int state[N];
vector<pt> g[N];

bool Solve() {
  int n, m;
  scanf("%d%d", &n, &m);
  memset(state, 0, sizeof state);
  for (int i = 0; i < m; ++i) {
    int k;
    scanf("%d", &k);
    g[i].clear();
    for (int j = 0; j < k; ++j) {
      int x, y;
      scanf("%d%d", &x, &y);
      g[i].push_back({x - 1, y});
    }
  }
  while (true) {
    bool changed = false;
    for (int i = 0; i < m; ++i) {
      bool ok =  false;
      int malted = -1;
      for (pt &p : g[i]) {
        if (state[p.first] == p.second) {
          ok = true;
          break;
        }
        if (p.second)
          malted = p.first;
      }
      if (!ok) {
        if (malted < 0)
          return false;
        state[malted] = 1;
        changed = true;
      }
    }
    if (!changed)
      break;
  }
  for (int i = 0; i < n; ++i)
    printf("%d ", state[i]);
  puts("");
  return true;
}

int main() {
  int tests;
  scanf("%d", &tests);
  for (int it = 1; it <= tests; ++it) {
    printf("Case #%d: ", it);
    if (!Solve())
      puts("IMPOSSIBLE");
  }
  return 0;
}
