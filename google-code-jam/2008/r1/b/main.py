

def check_demands(mask, demands):
  for row in demands:
    flag = False
    for p in row:
      bit = (mask >> p[0]) & 1
      if bit == p[1]:
        flag = True
        break
    if not flag:
      return False
  return True


def bit_len(mask):
  ans = 0
  while mask > 0:
    if mask & 1:
      ans += 1
    mask >>= 1
  return ans


def run_case(it):
  n = int(raw_input())
  m = int(raw_input())
  demands = []
  for i in range(0, m):
    row = []
    vals = [int(x) for x in raw_input().split()]
    for j in range(0, vals[0]):
      pos = 1 + j * 2
      row.append((vals[pos] - 1, vals[pos + 1]))
    demands.append(row)

  mend = 1 << n
  ans = -1
  count = 0
  for mask in range(0, mend):
    if check_demands(mask, demands):
      cur = bit_len(mask)
      if ans < 0 or count > cur:
        ans = mask
        count = cur
  if ans < 0:
    print "Case #%d: IMPOSSIBLE" % it
    return
  flavors = [str((ans >> x) & 1) for x in range(0, n)]
  print "Case #%d: %s" % (it, ' '.join(flavors))

def run():
  tests = int(raw_input())
  for it in range(0, tests):
    run_case(it + 1)

run()