#include <bits/stdc++.h>

using namespace std;

template<class T> inline T sqr(const T& a) {
  return a * a;
}

template<class T> inline T middle(const T &a, const T &b) {
  return (a + b) / 2;
}

template<class T> inline int len(const T &c) {
  return static_cast<int>(c.size());
}

typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;

const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

int bff[N];
vector<int> g[N];
int d[N];

int Up(int v, int no) {
  int res = 0;
  for (int to : g[v]) {
    if (to == no) {
      continue;
    }
    res = max(res, Up(to, no));
  }
  return res + 1;
}

void HandleCase() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    int x;
    scanf("%d", &x);
    --x;
    bff[i] = x;
    g[x].push_back(i);
  }
  memset(d, -1, sizeof d);
  int ans = 0;
  int total = 0;
  for (int i = 0; i < n; ++i) {
    int cur = 1;
    int j;
    int prev = i;
    d[i] = i;
    for (j = bff[i]; j != i && d[j] < i; j = bff[j]) {
      d[j] = i;
      ++cur;
      prev = j;
    }
    if (j == i) {
      ans = max(ans, cur);
      fprintf(stderr, "%d - %d - .. - %d = %d\n", i, bff[i], prev, cur);
      if (cur == 2 && i < bff[i]) {
        int bi = Up(i, bff[i]);
        int bo = Up(bff[i], i);
        fprintf(stderr, "%d <-> %d: branches %d, %d\n", i, bff[i], bi, bo);
        ans = max(ans, bi + bo);
        total += bi + bo;
      }
    }
  }
  if (ans < total) {
    fprintf(stderr, "total %d > ans %d\n", total, ans);
    ans = total;
  }
  printf("%d\n", ans);

  for (int i = 0; i < n; ++i) {
    g[i].clear();  
  }
}

int main() {
  int tests;
  scanf("%d", &tests);
  for (int test = 1; test <= tests; ++test) {
    printf("Case #%d: ", test);
    HandleCase();
  }
  return 0;
}
