// BEGIN CUT HERE

// END CUT HERE
#line 5 "TrianglesContainOrigin.cpp"
#include<bits/stdc++.h>
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

class TrianglesContainOrigin {
	public:
	long long count(vector <int> x, vector <int> y) {
		int n = (int) x.size();
		vector<ld> as(n);
		vector<ld> sas(n);
		for (int i = 0; i < n; ++i) {
			as[i] = atan2((ld) y[i], (ld) x[i]) + PI;
			sas[i] = as[i];
		}
		sort(sas.begin(), sas.end());
		for (int i = 0; i < n; ++i) {
			sas.push_back(sas[i] + 2 * PI);
		}
		li ans = 0;
		for (int i = 0; i < n; ++i) {
			for (int j = i + 1; j < n; ++j) {
				ld fm = as[i], to = as[j];
				if (fm > to)
					swap(fm, to);
				if (to - fm > PI + EPS) {
					fm += 2 * PI;
					if (fm > to)
						swap(fm, to);
				}
				fm += PI;
				to += PI;
				if (fm > 4 * PI + EPS || to > 4 * PI + EPS) {
					fm -= 2 * PI;
					to -= 2 * PI;
				}
				int cnt = upper_bound(sas.begin(), sas.end(), to) - lower_bound(sas.begin(), sas.end(), fm);
				//printf("%d, %d => %d\n", i, j, cnt);
				if (cnt > 0)
					ans += cnt;
			}
		}
		//assert(ans % 3 == 0);
		return ans / 3;
	}
};
