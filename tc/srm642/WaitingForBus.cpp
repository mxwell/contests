#line 5 "WaitingForBus.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

class WaitingForBus {
	public:
	double whenWillBusArrive(vector <int> time, vector <int> prob, int s) {
		int len = 3 * 1e5;
		vector<ld> d(len, 0);
		d[0] = 1;
		int n = (int) time.size();
		for (int j = 0; j < s; ++j) {
			for (int i = 0; i < n; ++i) {
				d[j + time[i]] += d[j] * prob[i] / 100;
			}
		}
		double ans = 0;
		for (int i = s; i < len; ++i)
			ans += d[i] * (i - s);
		return ans;
	}
};

int main() {
	vector<int> t, p;
	t.push_back(5);
	t.push_back(10);
	p.push_back(50);
	p.push_back(50);
	WaitingForBus w;
	cout << w.whenWillBusArrive(t, p, 8888) << endl;
	return 0;
}

// Powered by FileEdit


// Powered by FileEdit
