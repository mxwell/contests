#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;
const int MODULO = 1e9 + 7;

class NarrowPassage2 {
public:
	int count(vector <int> size, int maxSizeSum) {
		int n = (int) size.size();
		vector<pt> a(n);
		for (int i = 0; i < n; ++i) {
			a[i] = pt(size[i], i);
		}
		vector<bool> present(n, false);
		sort(a.begin(), a.end());
		int ans = 1;
		for (int i = n - 1; i >= 0; --i) {
			int val = a[i].first;
			int pos = a[i].second;
			li b = 1;
			for (int j = pos - 1; j >= 0; --j)
				if (present[j]) {
					if (size[j] + val > maxSizeSum)
						break;
					++b;
				}
			for (int j = pos + 1; j < n; ++j)
				if (present[j]) {
					if (size[j] + val > maxSizeSum)
						break;
					++b;
				}	
			ans = (ans * b) % MODULO;
			present[pos] = true;
		}
		return ans;			
	}
};

int main () {
	const int a[] = {2,1};
	NarrowPassage2 np2;
	cout << np2.count(vector<int>(a, a + 2), 2) << endl;
	return 0;
}