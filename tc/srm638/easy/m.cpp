#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 11;

typedef tuple<int, int, int> tr;

int a[N][N][N];
int u[N][N][N];
int umark;

const int dx[] = {-1, 0, 0, 0, 0, 1};
const int dy[] = {0, -1, 0, 0, 1, 0};
const int dz[] = {0, 0, -1, 1, 0, 0};

const char *no = "Impossible";
const char *yes = "Possible";	

void add(tr &src, int x, int y, int z) {
	a[x][y][z] = 2;
	src = make_tuple(x, y, z);
}

class ShadowSculpture {
	
public:
	string convert(bool ans) {
		if (ans)
			return yes;
		return no;
	}
	bool go(tr src, vector<string> &XY, vector<string> &YZ, vector<string> &ZX) {
		int n = (int) XY.size();
		queue<tr> q;
		++umark;
		u[get<0>(src)][get<1>(src)][get<2>(src)] = umark;
		q.push(src);
		while (!q.empty()) {
			tr nx = q.front();
			q.pop();
			for (int id = 0; id < 6; ++id) {
				int x = get<0>(nx) + dx[id];
				int y = get<1>(nx) + dy[id];
				int z = get<2>(nx) + dz[id];
				if (0 <= x && x < n && 0 <= y && y < n && 0 <= z && z < n && u[x][y][z] != umark) {
					if (a[x][y][z] > 0) {
						q.push(tr(x, y, z));
						u[x][y][z] = umark;
					}
				}
			}
		}
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				int cxy = 0, cyz = 0, czx = 0;
				for (int k = 0; k < n; ++k) {
					cxy += u[i][j][k] == umark;
					cyz += u[k][i][j] == umark;
					czx += u[j][k][i] == umark;
				}
				if ((XY[i][j] == 'Y') != (cxy != 0) ||
					(YZ[i][j] == 'Y') != (cyz != 0) ||
					(ZX[i][j] == 'Y') != (czx != 0))
					return false;
			}
		}
		return true;
	}
	string possible(vector <string> XY, vector <string> YZ, vector <string> ZX) {
		int n = (int) XY.size();
		bool present = false;
		tr src(-1, -1, -1);
		for (int x = 0; x < n; ++x) {
			for (int y = 0; y < n; ++y) {
				for (int z = 0; z < n; ++z) {
					if (XY[x][y] == 'Y' ||
						YZ[y][z] == 'Y' ||
						ZX[z][x] == 'Y')
						present = true;
					if (XY[x][y] == 'N' || 
						YZ[y][z] == 'N' || 
						ZX[z][x] == 'N') {
						a[x][y][z] = -1;
					} else {
						a[x][y][z] = 1;
					}
				}
			}
		}
		if (!present)
			return yes;
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				int cxy = 0, cyz = 0, czx = 0;
				int lastxy = 0, lastyz = 0, lastzx = 0;
				for (int k = 0; k < n; ++k) {
					if (a[i][j][k] >= 1)
						++cxy, lastxy = k;
					if (a[k][i][j] >= 1)
						++cyz, lastyz = k;
					if (a[j][k][i] >= 1)
						++czx, lastzx = k;
				}
				if ((XY[i][j] == 'Y' && cxy == 0) ||
					(YZ[i][j] == 'Y' && cyz == 0) ||
					(ZX[i][j] == 'Y' && czx == 0)) {
					return no;
				}
				if (XY[i][j] == 'Y' && cxy == 1)
					add(src, i, j, lastxy);
				if (YZ[i][j] == 'Y' && cyz == 1)
					add(src, lastyz, i, j);
				if (ZX[i][j] == 'Y' && czx == 1)
					add(src, j, lastzx, i);
			}
		}
		if (get<0>(src) >= 0)
			return convert(go(src, XY, YZ, ZX));
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					if (a[i][j][k] > 0 && u[i][j][k] == 0) {
						if (go(make_tuple(i, j, k), XY, YZ, ZX))
							return yes;
					}
				}
			}
		}
		return no;
	}
};

int main () {
	vector<string> xy, yz, zx;
	const char *a[] = {"YNN","NNN","NNN"};
	const char *b[] = {"NNY","NNN","NNN"};
	const char *c[] = {"NNN","NNN","YNN"};
	int count = 3;
	ShadowSculpture ss;
	cout << ss.possible(vector<string>(a, a + count),
						vector<string>(b, b + count),
						vector<string>(c, c + count)) << endl;
	return 0;
}