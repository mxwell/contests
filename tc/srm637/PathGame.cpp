#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

pt trans[3][3];
int z[4][4][N];

void init() {
	memset(z, -1, sizeof z);
	trans[0][0] = pt(0, 0);
	trans[2][0] = trans[0][2] = trans[1][0] = trans[0][1] = pt(0, 1);
	trans[2][2] = trans[1][1] = pt(1, 1);
	trans[2][1] = trans[1][2] = pt(1, 2);

}

class PathGame {
	public:
	int sg(int in, int out, int len) {
		pt &to = trans[in][out];
		in = to.first;
		out = to.second;
		int &tg = z[in][out][len];
		if (tg < 0) {
			if (len == 1) {
				if (in == 1 && out == 2)
					tg = 0;
				else
					tg = 1;
			} else {
				vector<int> vals;
				if ((in & 1) == 0)
					vals.push_back(sg(2, out, len - 1));
				if ((in & 2) == 0)
					vals.push_back(sg(1, out, len - 1));
				for (int i = 1; i + 1 < len; ++i) {
					vals.push_back(sg(in, 1, i) ^ sg(1, out, len - i - 1));
					vals.push_back(sg(in, 2, i) ^ sg(2, out, len - i - 1));
				}
				if ((out & 1) == 0)
					vals.push_back(sg(in, 2, len - 1));
				if ((out & 2) == 0)
					vals.push_back(sg(in, 1, len - 1));
				sort(vals.begin(), vals.end());
				int j = 0;
				for (int i = 0; ; ++i) {
					while (j < int(vals.size()) && vals[j] < i)
						++j;
					if (j >= int(vals.size()) || vals[j] > i) {
						tg = i;
						break;
					}
				}
			}
			//printf("%d => %d, len %d :: %d\n", in, out, len, tg);
		}
		return tg;
	}
	int make_mask(char x, char y) {
		int mask = 0;
		if (x == '#')
			mask |= 1;
		if (y == '#')
			mask |= 2;
		return mask;
	}
	string judge(vector <string> board) {
		init();
		int n = (int) board[0].size();
		int prev = -1;
		int in = 0;
		int ans = 0;
		for (int i = 0; i < n; ++i) {
			int cur_mask = make_mask(board[0][i], board[1][i]);
			if (cur_mask) {
				if (prev + 1 < i) {
					int t = sg(in, cur_mask, i - prev - 1);
					ans ^= t;
				}
				prev = i;
				in = cur_mask;
			}
		}
		if (prev + 1 < n) {
			int t = sg(in, 0, n - prev - 1);
			ans ^= t;
		}
		return ans ? "Snuke" : "Sothe";
	}
};
