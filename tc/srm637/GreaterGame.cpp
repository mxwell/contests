#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
#ifndef LL
#	define LL "%I64d"
#endif
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 11;

class GreaterGame {
	public:
	double calc(vector <int> hand, vector <int> sothe) {
		int n = (int) hand.size();
		vector<int> rest;
		for (int i = 1; i <= n * 2; ++i) {
			rest.push_back(i);			
		}
		for (int i = 0; i < n; ++i) {
			rest[hand[i] - 1] = -1;
			if (sothe[i] >= 0)
				rest[sothe[i] - 1] = -1;
		}
		int j = 0;
		for (int i = 0; i < int(rest.size()); ++i) {
			if (rest[i] > 0)
				rest[j++] = rest[i];			
		}
		rest.resize(j);
		sort(rest.begin(), rest.end());
		sort(hand.begin(), hand.end());
		sort(sothe.begin(), sothe.end());
		j = 0;
		double ans = 0;
		for (int i = 0; i < n; ++i) {
			if (sothe[i] > 0) {
				while (j < n && hand[j] < sothe[i])
					++j;
				if (j < n) {
					ans += 1.0;
					hand[j] = -1;
					++j;
				}
			}
		}
		int cnt = 0;
		for (int i = 0; i < n; ++i) {
			if (hand[i] > 0)
				++cnt;
		}
		cnt -= rest.size();
		j = 0;
		while (cnt-- > 0) {
			while (j < n && hand[j] < 0)
				++j;
			hand[j] = -1;
			++j;
		}
		if (rest.size())
			for (int i = 0; i < n; ++i) {
				if (hand[i] > 0) {
					int less = lower_bound(rest.begin(), rest.end(), hand[i]) - rest.begin();
					ans += double(less) / rest.size();
				}
			}
		return ans;
	}
};

// Powered by FileEdit
