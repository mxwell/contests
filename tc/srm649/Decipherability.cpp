// BEGIN CUT HERE

// END CUT HERE
#line 5 "Decipherability.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 51;

int d[N][N][N][N][2];
string gstr;

bool rec(int x, int y, int a, int b, int flag) {
  int &tg = d[x][y][a][b][flag];
  if (!tg) {
    tg = -1;
    int n = int(gstr.size());

    if (a <= 0 && b <= 0) {
      tg = (x == y && flag) ? 1 : -1;
      return tg > 0;
    }

    if (x >= n && y >= n) {
      return tg > 0;
    }

    if (a && x < n) {
      if (rec(x + 1, y, a - 1, b, flag))
        tg = 1;
      if (b && y < n) {
        if (rec(x + 1, y + 1, a - 1, b - 1, flag))
          tg = 1;
      }
    } else if (b && y < n) {
      if (rec(x, y + 1, a, b - 1, flag))
        tg = 1;
    }

    if (x != y)
      flag = 1;
    if (x < n && y < n && gstr[x] == gstr[y] && rec(x + 1, y + 1, a, b, flag))
      tg = 1;
  }
  return tg > 0;
}

class Decipherability {
	public:
	string check(string s, int K) {
    gstr = s;
    return rec(0, 0, K, K, 0) ? "Uncertain" : "Certain";    		
	}
};
