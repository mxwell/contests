// BEGIN CUT HERE

// END CUT HERE
#line 5 "XorSequence.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 51;

li first0[N], first1[N];

void GetPairs(vector<int> &a, int fm, int to, int b) {
  if (b < 0)
    return;
  if (fm > to)
    return;
  li zeros = 0, ones = 0;
  int gwb = 1 << b;
  vector<int> save;
  save.reserve(to - fm + 1);
  int p = fm;
  li &f0 = first0[b];
  li &f1 = first1[b];
  for (int i = fm; i <= to; ++i) {
    int num = a[i];
    if (num & gwb)
      save.push_back(num), ++ones, f0 += zeros;
    else
      a[p++] = num, ++zeros, f1 += ones;
  }
  GetPairs(a, fm, p - 1, b - 1);
  int pos = p;
  for (int num : save)
    a[p++] = num;
  GetPairs(a, pos, to, b - 1);
}

class XorSequence {
  public:
  long long getmax(int N, int sz, int A0, int A1, int P, int Q, int R) {
    vector<int> a(sz);
    a[0] = A0;
    a[1] = A1;
    for (int i = 2; i < sz; ++i) {
      a[i] = (li(a[i - 2]) * P + li(a[i - 1]) * Q + R) % N;
    }
    int h = 1;
    while ((1 << h) < N)
      ++h;
    GetPairs(a, 0, sz - 1, h - 1);
    li ans = 0;
    for (int i = 0; i < h; ++i) {
      ans += max(first0[i], first1[i]);
    }
    return ans;
  }
};
