// BEGIN CUT HERE

// END CUT HERE
#line 5 "WakingUp.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 51;

li gcd(li a, li b) {
  while (a) {
    b %= a;
    swap(a, b);
  }
  return b;
}

class WakingUp {
  public:
  int maxSleepiness(vector <int> period, vector <int> start, vector <int> volume, int D) {
    int n = int(period.size());
    li nok = period[0];
    for (int i = 1; i < n; ++i) {
      li nod = gcd(nok, period[i]);
      nok *= period[i];
      nok /= nod;      
    }
    li effect = D * nok;
    for (int i = 0; i < n; ++i)
      effect -= volume[i] * (nok / period[i]);
    if (effect < 0)
      return -1;
    int minv = 0;
    int cur = 0;
    for (int i = 1; i < 10000; ++i) {
      cur += D;
      for (int j = 0; j < n; ++j) {
        if ((i - start[j]) % period[j] == 0) {
          cur -= volume[j];
        }
      }
      minv = min(minv, cur);
    }
    return -minv;
  }
};
