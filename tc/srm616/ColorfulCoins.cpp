// BEGIN CUT HERE

// END CUT HERE
#line 5 "ColorfulCoins.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 51;

class ColorfulCoins {
  public:
  int minQueries(vector<long long> values) {
    int n = int(values.size());
    if (n == 1)
      return 1;
    vector<li> quo(n - 1);
    for (int i = 0; i < n - 1; ++i) {
      quo[i] = values[i + 1] / values[i];
    }
    sort(quo.begin(), quo.end());
    int ans = 1;
    int prev = 0;
    for (int i = 0; i < n - 1; ) {
      int j = i + 1;
      for (; j < n - 1 && quo[i] == quo[j]; ++j) { }
      if (quo[i] < 60) {
        int cnt = j - i;
        li qpow = quo[i];
        int nans = 1;
        while (qpow - prev - 1 < cnt) {
          qpow *= quo[i];
          ++nans;
        }
        ans = max(ans, nans);
      }
      prev += j - i;
      i = j;
    }
    return ans;
  }
};
