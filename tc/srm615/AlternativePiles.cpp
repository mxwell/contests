// BEGIN CUT HERE

// END CUT HERE
#line 5 "AlternativePiles.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 52;
constexpr int MODULO = 1e9 + 7;

li d[5010][NMAX][NMAX];

inline void AddLi(li &tg, li x) {
  tg = (tg + x) % MODULO;
}

class AlternativePiles {
	public:
	int count(string C, int M) {
    d[0][0][0] = 1;
    int n = int(C.size());
    for (int i = 0; i < n; ++i) {
      char c = C[i];
      int ni = i + 1;
      for (int r = 0; r < M; ++r) {
        int nr = (r + 1) % M;
        for (int b = 0; b <= M; ++b) {
          li val = d[i][r][b];
          if (c == 'W' || c == 'B')
            AddLi(d[ni][r][b], val);
          if ((c == 'W' || c == 'R') && b < M)
            AddLi(d[ni][nr][b + 1], val);
          if ((c == 'W' || c == 'G') && b > 0)
            AddLi(d[ni][r][b - 1], val);
        }
      }
    }
    return int(d[n][0][0] % MODULO);
	}
};
