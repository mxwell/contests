// BEGIN CUT HERE

// END CUT HERE
#line 5 "AmebaDiv1.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 51;

class AmebaDiv1 {
	public:
	int count(vector <int> X) {
    int n = int(X.size());
    set<int> s;
    for (int i = 0; i < n; ++i) {
      int x = X[i];
      if (s.count(x))
        continue;
      s.insert(x);
      int t = x * 2;
      if (s.count(t)) {
        s.erase(s.find(t));
      }
    }
    return int(s.size());
	}
};
