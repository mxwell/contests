// BEGIN CUT HERE

// END CUT HERE
#line 5 "LongLongTripDiv1.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 51;
constexpr int DMAX = 1e4;

li dist[NMAX][DMAX * 2];

class LongLongTripDiv1 {
	public:
	string isAble(int N, vector <int> A, vector <int> B, vector <int> D, long long T) {
    int d2 = 1e9;
    int m = int(A.size());
    for (int i = 0; i < m; ++i) {
      if (A[i] > B[i])
        swap(A[i], B[i]);
      if (A[i] == 0 && D[i] * 2 < d2)
        d2 = D[i] * 2;
    }
    if (d2 > 1e8)
      return "Impossible";
    memset(dist, 127, sizeof dist);
    dist[0][0] = 0;
    set<pair<int, pt>> heap;
    heap.insert({0, {0, 0}});
    for (;;) {
      pt v;
      li cur_dist;
      if (heap.size()) {
        auto it = heap.begin();
        cur_dist = it->first;
        v = it->second;
        heap.erase(it);
      } else {
        break;
      }
      for (int id = 0; id < m; ++id) {
        int to = -1;
        if (A[id] == v.first)
          to = B[id];
        else if (B[id] == v.first)
          to = A[id];
        if (to < 0)
          continue;
        int d = (D[id] + v.second) % d2;
        li next_dist = cur_dist + D[id];
        if (next_dist > T)
          continue;
        if (dist[to][d] > next_dist) {
          auto it = heap.find({dist[to][d], {to, d}});
          if (it != heap.end())
            heap.erase(it);
          dist[to][d] = next_dist;
          heap.insert({dist[to][d], {to, d}});
        }
      }
    }
    li t = dist[N - 1][T % d2];
    return (t <= T && t % d2 == T % d2) ? "Possible" : "Impossible";
	}
};
