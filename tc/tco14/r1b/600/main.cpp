#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

bool st[500];

class WolvesAndSheep {
public:
	int getmin(vector <string> field) {
		int n = (int) field.size();
		int m = (int) field[0].size();
		int res = 50;
		int msn = 1 << (n - 1);
		forn (ms, msn) {
			vector<pair<int, pt> > sg;
			int prev = -1;
			bool ok = true;
			int cnt = 0;
			int top = 0;
			forn (r, n) {
				int smsk = 0;
				int wmsk = 0;
				if (r == n - 1 || (ms & (1 << r))) {
					if (r != n - 1)
						++cnt;
					for (int i = prev + 1; (smsk & wmsk) == 0 && i <= r; ++i)
						forn (j, m) {
							if (field[i][j] == 'S') {
								smsk |= 1 << j;
							} else if (field[i][j] == 'W') {
								wmsk |= 1 << j;
							}
						}
					prev = r;
				}
				if (smsk & wmsk) {
					ok = false;
					break;
				}
				char last = 0;
				int pos = -1;
				forn (j, m) {
					if (smsk & (1 << j)) {
						if (last == 'W') {
							sg.pb(mp(pos, pt(0, top)));
							sg.pb(mp(j - 1, pt(1, top)));
							++top;
						}
						last = 'S';
						pos = j;
					} else if (wmsk & (1 << j)) {
						if (last == 'S') {
							sg.pb(mp(pos, pt(0, top)));
							sg.pb(mp(j - 1, pt(1, top)));
							++top;
						}
						last = 'W';
						pos = j;
					}
				}
			}
			if (!ok)
				continue;
			sort(all(sg));
			memset(st, 0, top * sizeof (bool));
			int opened = 0;
			int closed = 0;
			queue<int> op;
			forn (i, sg.size()) {
				pair<int, pt> &c = sg[i];
				if (c.second.first == 0) {
					++opened;
					op.push(c.second.second);
				} else {
					if (closed < opened) {
						if (st[c.second.second])
							continue;
						while (!op.empty()) {
							st[op.front()] = true;
							op.pop();
							++closed;
						}
						++cnt;
					}
				}
			}
			res = min(res, cnt);
		}
		return res;
	}
};

#ifdef LOCAL_RUN
int main() {
	int n, m;
	scanf("%d%d\n", &n, &m);
	vector<string> vs;
	forn (i, n) {
		string s;
		getline(cin, s);
		assert(m == (int) s.size());
		vs.pb(s);
	}
	if (0) {
		vector<string> t(m);
		forn (j, m)
			forn (i, n)
				t[j].pb(vs[i][j]);
		vs = t;
	}
	WolvesAndSheep w;
	cout << w.getmin(vs) << endl;
  
    return 0;
}
#endif