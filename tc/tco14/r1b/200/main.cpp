#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

class SpamChecker {
public:
	string spamCheck(string judgeLog, int good, int bad) {
		int s = 0;
		forn (i, judgeLog.size()) {
			if (judgeLog[i] == 'o')
				s += good;
			else
				s -= bad;
			if (s <	0)
				return "SPAM";
		}
		return "NOT SPAM";
	}
};

#ifdef LOCAL_RUN
int main() {
    freopen("input.txt", "rt", stdin);
	//freopen("output.txt", "wt", stdout);
  
    return 0;
}
#endif