// BEGIN CUT HERE

// END CUT HERE
#line 5 "CycleColoring.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 1e6 + 10;
constexpr li MODULO = 1e9 + 7;

li same[NMAX];
li dif[NMAX];
int gK;

inline li MultLi(li x, li y) {
  return (x * y) % MODULO;
}

inline void AddToLi(li &tg, li x) {
  tg = (tg + x) % MODULO;
}

void init(int k) {
  same[1] = 1;
  dif[2] = 1;
  same[3] = k - 1;
  dif[3] = k - 2;
  for (int i = 4; i < NMAX; ++i) {
    same[i] = MultLi(k - 1, dif[i - 1]);
    dif[i] = MultLi(k - 2, dif[i - 1]);
    AddToLi(dif[i], same[i - 1]);
  }
}

li d[110][2];

vector<int> vcount, fromv, tov;
li Rec(int lf, int rg, bool left_zero) {
  li &tg = d[lf][left_zero];
  if (tg < 0) {
    tg = 0;
    int fm = fromv[lf], to = tov[lf];
    if (fm > to)
        swap(fm, to);
    int u = to - fm;
    int v = vcount[lf] - u;
    if (lf == rg) {
      if (left_zero) {        
        AddToLi(tg, MultLi(same[u + 1], same[v + 1]));
      } else {
        if (fm != to)
          AddToLi(tg, MultLi(dif[u + 1], dif[v + 1]));
      }
    } else {
      if (left_zero) {
        if (fm != to) {
          li t = Rec(lf + 1, rg, false);
          t = MultLi(t, gK - 1);
          t = MultLi(t, dif[u + 1]);
          t = MultLi(t, dif[v + 1]);
          AddToLi(tg, t);
        }
        li t = Rec(lf + 1, rg, true);
        t = MultLi(t, same[u + 1]);
        t = MultLi(t, same[v + 1]);
        AddToLi(tg, t);
      } else {
        if (fm != to) {
          li t = Rec(lf + 1, rg, true);
          t = MultLi(t, dif[u + 1]);
          t = MultLi(t, dif[v + 1]);
          AddToLi(tg, t);
        }
        li t = Rec(lf + 1, rg, false);
        li t1 = MultLi(t, same[u + 1]);
        t1 = MultLi(t1, same[v + 1]);
        AddToLi(tg, t1);
        if (gK > 2 && fm != to) {
          li t2 = MultLi(t, gK - 2);
          t2 = MultLi(t2, dif[u + 1]);
          t2 = MultLi(t2, dif[v + 1]);
          AddToLi(tg, t2);
        }
      }
    }
  }
  return tg;
}

class CycleColoring {
	public:
	int countColorings(vector <int> vertexCount, vector <int> fromVertex, vector <int> toVertex, int K) {
		vcount = vertexCount;
    int n = int(vcount.size());
    for (int i = 0; i < n; ++i) {
      fromv.push_back(toVertex[(i - 1 + n) % n]);
      tov.push_back(fromVertex[i]);
    }
    gK = K;
    
    init(K);
    memset(d, -1, sizeof d);
    li ans = Rec(0, n - 1, true);
    ans = MultLi(ans, K);
    return int(ans);
	}
};
