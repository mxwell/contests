// BEGIN CUT HERE

// END CUT HERE
#line 5 "MinimumSquare.cpp"
#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int NMAX = 51;

class MinimumSquare {
	public:
	long long minArea(vector <int> x, vector <int> y, int K) {
    int n = int(x.size());
    int mx = -1e9, my = -1e9;
    for (int i = 0; i < n; ++i) {
      mx = max(mx, x[i]);
      my = max(my, y[i]);
    }
    li side = 2e9 + 2;
    for (int u = 0; u < n; ++u) {
      int bx = x[u] - 1;
      for (int v = 0; v < n; ++v) {
        int by = y[v] - 1;
        int lf = 2, rg = min(side, (li) max(mx + 1 - bx, my + 1 - by));
        while (lf + 1 < rg) {
          li mid = (li(lf) + rg) / 2;
          li tx = bx + mid;
          li ty = by + mid;
          int cur = 0;
          for (int i = 0; i < n; ++i) {
            int px = x[i], py = y[i];
            if (bx < px && px < tx && by < py && py < ty)
              ++cur;
          }
          if (cur < K)
            lf = mid;
          else
            rg = mid;
        }
        for (; lf <= rg; ++lf) {
          li tx = bx + li(lf);
          li ty = by + li(lf);
          int cur = 0;
          for (int i = 0; i < n; ++i) {
            int px = x[i], py = y[i];
            if (bx < px && px < tx && by < py && py < ty)
              ++cur;
          }
          if (cur >= K) {
            side = min(side, (li) lf);
            break;
          }
        }
      }
    }
    return sqr(side);
	}
};
