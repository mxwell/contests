#line 5 "ChristmasTreeDecoration.cpp"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

class ChristmasTreeDecoration {
	public:
	int solve(vector <int> col, vector <int> x, vector <int> y) {
		int n = (int) col.size();
		int m = (int) x.size();
		vector<bool> used(n, false);
		used[0] = true;
		int ans = 0;
		for (int it = 0; it < n - 1; ++it) {
			int e = -1;
			int bestw = 10;
			for (int j = 0; j < m; ++j) {
				int from = x[j] - 1, to = y[j] - 1;
				int w = col[from] == col[to];
				if ((used[from] && !used[to]) || (used[to] && !used[from])) {
					if (e == -1 || bestw > w) {
						bestw = w;
						e = j;
					}
				}
			}
			if (used[x[e] - 1]) {
				used[y[e] - 1] = true;
			} else {
				used[x[e] - 1] = true;
			}
			ans += bestw;
		}
		return ans;
	}
};


// Powered by FileEdit
