#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1000100;

li ans = 0;
vector<pt> g[N];
li d[N];

void dfs(int vx, int from = -1) {
	li &tg = d[vx];
	tg = 0;
	li u = -1e18, v = -1e18;
	for (pt e: g[vx]) {
		if (e.first == from)
			continue;
		dfs(e.first, vx);
		li val = e.second + d[e.first];
		if (val > u) {
			v = u;
			u = val;
		} else if (val > v) {
			v = val;
		}
		tg = max(tg, val);
	}
	ans = max(ans, tg);
	if (v > -1e17)
		ans = max(ans, u + v);
}

int main() {
	int n;
	scanf("%d", &n);
	for (int i = 0; i < n - 1; ++i) {
		int x, y, w;
		scanf("%d%d%d", &x, &y, &w);
		--x, --y;
		g[x].push_back(pt(y, w));
		g[y].push_back(pt(x, w));
	}
	dfs(0);
	cout << ans << endl;	
    return 0;
}
