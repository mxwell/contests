#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100010;

int last[N];

void go(const vector<int> &a, vector<int> &b) {
	memset(last, -1, sizeof last);
	b.resize(a.size());
	for (int i = 0; i < int(a.size()); ++i) {
		int num = a[i];
		if (last[num] < 0) {
			b[i] = num;
		} else {
			b[i] = a[last[num]];
		}
		last[1] = last[num] = i;
		int d;
		for (d = 2; d * d < num; ++d) {
			if (num % d == 0) {
				last[d] = i;
				last[num / d] = i;
			}
		}
		if (d * d == num) {
			last[d] = i;
		}
	}
}

int main() {
	int n;
	scanf("%d", &n);
	vector<int> a(n);
	for (int i = 0; i < n; ++i) {
		scanf("%d", &a[i]);
	}
	vector<int> b;
	go(a, b);
	reverse(a.begin(), a.end());
	vector<int> c;
	go(a, c);
	reverse(c.begin(), c.end());
	li ans = 0;
	for (int i = 0; i < int(b.size()); ++i)
		ans += b[i] * c[i];
	cout << ans << endl;
    return 0;
}
