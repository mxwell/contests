#include<bits/stdc++.h>
  
using namespace std;

template<typename TYPE> inline TYPE sqr(const TYPE& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 33;

bool wt[N][N];

const int dx[] = {-1, 0, 0, 1};
const int dy[] = {0, -1, 1, 0};

int main() {
	int tests;
	scanf("%d\n", &tests);
	for (int it = 0; it < tests; ++it) {
		int h, w, s, n;
		scanf("%d%d%d%d\n", &h, &w, &s, &n);
		string line;
		memset(wt, 0, sizeof wt);
		while (n-- > 0) {
			getline(cin, line);
			int x = 0, y = 0;
			wt[x][y] = true;
			for (char c : line) {
				if (c == 'd')
					++x;
				else if (c == 'u')
					--x;
				else if (c == 'l')
					--y;
				else 
					++y;
				wt[x][y] = true;
				//assert(0 <= x && x < h && 0 <= y && y < w);
			}
		}
		int ans = 0;
		for (int x = 0; x < h; ++x) {
			for (int y = 0; y < w; ++y) {
				if (!wt[x][y]) {
					queue<pt> q;
					q.push(pt(x, y));
					wt[x][y] = true;
					int cnt = 1;
					while (!q.empty()) {
						pt fm = q.front();
						q.pop();
						for (int id = 0; id < 4; ++id) {
							pt nx(fm.first + dx[id], fm.second + dy[id]);
							if (0 <= nx.first && nx.first < h && 0 <= nx.second && nx.second < w && !wt[nx.first][nx.second]) {
								wt[nx.first][nx.second] = true;
								q.push(nx);
								++cnt;
							}
						}
					}
					ans += cnt >= s;
				}
			}
		}
		printf("%d\n", ans);
	}
	
    return 0;
}
