#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 1600;

int a[N];
int d[N];

int main() {
  int tests;
  scanf("%d", &tests);
  while (tests-- > 0) {
    int n;
    scanf("%d", &n);
    int s = 0;
    for (int i = 1; i <= n; ++i) {
      scanf("%d", &a[i]);
      s += a[i];
    }
    memset(d, 127, sizeof d);
    d[0] = 0;
    int cur = s;
    for (int i = 0; i <= n; ++i) {
      cur -= a[i];
      int t = 0;
      int val = d[i];
      if (val > 1e8)
        continue;
      for (int j = i + 1; j <= n; ++j) {
        val += t;
        d[j] = min(d[j], val + cur - t - a[j]);
        t += a[j];
      }
    }
    printf("%d\n", d[n]);
  }
  return 0;
}
