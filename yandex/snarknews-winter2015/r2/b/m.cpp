#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

constexpr int SIZE = 1e7;
char s[SIZE];

int f(int digits, int rg, int divid) {
  int res = 0;
  int top = 0;
  for (int i = 1; i <= rg; ++i) {
    if (top + 10 > SIZE) {
      int ntop = 0;
      for (int j = top - 10; j < top; ++j) {
        s[ntop++] = s[j];
      }
      s[ntop] = '\0';
      top = ntop;
    }

    sprintf(s + top, "%d", i);
    while (s[top] != '\0') {
      assert(top < SIZE);
      ++top;
    }
    if (i & 1)
      continue;
    int t = 0;
    for (int j = max(0, top - digits); j < top; ++j)
      t = t * 10 + (s[j] - '0');
    if (t % divid == 0)
      ++res;
  }
  return res;
}

int main() {
  li m;
  int n;
  cin >> m >> n;
  li ans = 0;
  if (n == 1) {
    ans = m / 2;
  } else {
    li rg = 1;
    for (int i = 1; i <= n; ++i)
      rg *= 10;
    int d = 1 << n;
    ans = f(n, int(min(m, rg)), d);
    li from = (rg / d) * d;
    if (m > from)
      ans += (m - from) / d;
  }
  cout << ans << endl;
  return 0;
}
