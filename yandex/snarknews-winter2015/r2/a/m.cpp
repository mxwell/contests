#include<bits/stdc++.h>
  
using namespace std;

template<class T> inline T sqr(const T& a) { return a * a; }
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
constexpr ld EPS = 1e-9;
constexpr ld PI = 2 * acos(0.0);
constexpr int N = 100;

int main() {
  int n;
  scanf("%d", &n);
  while (n-- > 0) {
    int x;
    scanf("%d", &x);
    int y;
    if (x > 180)
      y = (x - 180 + 5) / 10;
    else
      y = (x + 180 + 5) / 10;
    x = (x + 5) / 10;
    if (x <= 0)
      x = 1e9;
    if (y <= 0)
      y = 1e9;
    printf("%02d\n", min(x, y));
  }
  return 0;
}
