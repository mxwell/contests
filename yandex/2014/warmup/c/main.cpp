#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

const int MEGA = 1000 * 1000;

int gcd(int a, int b) {
	if (a == 0)
		return b;
	return gcd(b % a, a);
}

pt getr(int a, int b, int c) {
	int p = a + b + c;
	a *= 2;
	b *= 2;
	c *= 2;
	int num = (p - a) * (p - b) * (p - c);
	int den = 4 * p;
	int d = gcd(num, den);
	return pt(num / d, den / d);
}

int main() {
	map<pt, int> cnt;
	int n;
	scanf("%d", &n);
	forn (i, n) {
		int a, b, c;
		scanf("%d%d%d", &a, &b, &c);
		pt r = getr(a, b, c);
		if (cnt.count(r) == 0)
			cnt.insert(mp(r, 1));
		else
			cnt[r] = cnt[r] + 1;
	}
	int ans = 0;
	for (auto p = cnt.begin(); p != cnt.end(); ++p) {
		ans = max(ans, p->second);
	}
	printf("%d\n", ans);

    return 0;
}
