#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<li, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

const int MEGA = 1000 * 1000;

pt fact(int n, int k) {
	li r = 1;
	int z = 0;
	for1 (i, n) {
		r *= i;
		while (r % 10 == 0) {
			++z;
			r /= 10;
		}
		r %= MEGA;
	}
	li p = 1;
	z *= k;
	forn (i, k) {
		p *= r;
		while (p % 10 == 0) {
			++z;
			p /= 10;
		}
		p %= MEGA;
	}
	return pt(p, z);
}

int solve(int n, int k) {
	int s = 0, minz = INF;

	forn (i, n + 1) {
		pt f = fact(i, k);
		//printf("%d: digit %d, %d zero(s)\n", i, f.first, f.second);
		if (f.second < minz) {
			minz = f.second;
			s = f.first;
		} else if (f.second == minz) {
			s += f.first;
			s %= MEGA;
		}
	}
	while (s % 10 == 0)
		s /= 10;
	s %= 10;
	assert(s != 0);
	return static_cast<int>(s);
}

int main() {
	int n, k;
	scanf("%d%d", &n, &k);
	printf("%d\n", solve(n, k));

    return 0;
}
