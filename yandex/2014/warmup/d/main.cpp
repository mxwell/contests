#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 1010;

vector<pt> g[N];
int d[N];
int c[N];
bool used[N];

int main() {
	int n, m;
	scanf("%d%d", &n, &m);

	forn (i, m) {
		int x, y, len;
		scanf("%d%d%d", &x, &y, &len);
		--x, --y;
		g[x].pb(pt(y, len));
		g[y].pb(pt(x, len));
	}

	memset(d, 127, sizeof d);
	d[0] = 0;
	for (;;) {
		int fm = -1;
		int dist = INF;
		int cnt = 0;
		forn (i, n)
			if (!used[i] && d[i] < INF) {
				if (d[i] < dist) {
					fm = i;
					dist = d[i];
					cnt = c[i];
				} else if (d[i] == dist && c[i] > cnt) {
					fm = i;
					cnt = c[i];
				}
			}
		if (fm == -1)
			break;
		used[fm] = true;
		vector<pt> &gh = g[fm];
		++cnt;
		forn (i, gh.size()) {
			int to = gh[i].first;
			int ndist = dist + gh[i].second;
			if (d[to] > ndist) {
				d[to] = ndist;
				c[to] = cnt;
			} else if (d[to] == ndist && c[to] < cnt) {
				c[to] = cnt;
			}
		}
	}

	printf("%d %d\n", d[n - 1], c[n - 1]);


    return 0;
}
