#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

const int dx[] = {-1, 0, 0, 1, -1, -1, 1, 1};
const int dy[] = {0, -1, 1, 0, -1, 1, -1, 1};

char buf[8];
pt read() {
	scanf("%s", buf);
	return pt(buf[0] - 'a', buf[1] - '1');
}

/* 0 - safe, 1 - wk, 2 - wr */
int dangerous(const pt &wk, const pt &wr, int bx, int by) {
	forn (id, 8) {
		int x = wk.first + dx[id],
			y = wk.second + dy[id];
		if (!correct(x, y, 8, 8))
			continue;
		if (x == bx && y == by) {
			return 1;
		}
	}
	forn (id, 4) {
		int x = wr.first + dx[id],
			y = wr.second + dy[id];
		while (correct(x, y, 8, 8)) {
			if (x == wk.first && y == wk.second)
				break;
			if (x == bx && y == by) {
				return 2;
			}
			x += dx[id];
			y += dy[id];
		}
	}
	return 0;
}

int main() {
	pt wk = read();
	pt wr = read();
	pt bk = read();

	int under_attack = dangerous(wk, wr, bk.first, bk.second);
	if (under_attack == 1) {
		puts("Strange");
		return 0;
	}
	int move = 0;
	forn (id, 8) {
		int x = bk.first + dx[id],
			y = bk.second + dy[id];
		if (!correct(x, y, 8, 8))
			continue;
		if (dangerous(wk, wr, x, y) == 0)
			++move;
	}
	if (under_attack == 0) {
		if (move > 0)
			puts("Normal");
		else
			puts("Stalemate");
	} else {
		if (move > 0)
			puts("Check");
		else
			puts("Checkmate");
	}

    return 0;
}
