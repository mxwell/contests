#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

int main() {
	int n;
	scanf("%d", &n);
	set<int> s[2];
	int cur = 0, next = 1;
	s[cur].insert(0);
	forn (i, n) {
		int a;
		scanf("%d", &a);
		s[next].clear();
		while (s[cur].size()) {
			auto first = s[cur].begin();
			int fm = *first;
			s[cur].erase(first);
			int to = fm + a;
			vector<int> digits;
			while (to > 0) {
				digits.pb(to % 10);
				to /= 10;
			}
			sort(all(digits));
			do {
				int val = 0;
				forn (i, digits.size()) {
					val = val * 10 + digits[i];
				}
				s[next].insert(val);
			} while (next_permutation(digits.begin(), digits.end()));
		}
		swap(cur, next);
	}
	while (s[cur].size() > 1)
		s[cur].erase(s[cur].begin());
	printf("%d\n", *(s[cur].begin()));
    return 0;
}
