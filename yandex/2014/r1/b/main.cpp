#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, li> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100100;

int a[N];
/* pt: val, cost */
pt d[N];

int main() {
	int n;	  
	scanf("%d", &n);
	forn (i, n) {
		scanf("%d", &a[i]);
	}
	sort(a, a + n);
	d[1] = pt(a[1], abs(a[1] - a[0]));
	if (n > 2)
		d[2] = pt(a[1], abs(a[1] - a[0]) + abs(a[1] - a[2]));
	for (int i = 3; i < n; ++i) {
		int diff = abs(a[i] - a[i - 1]);
		if (d[i - 2].second < d[i - 1].second)
			d[i] = pt(a[i], d[i - 2].second + diff);
		else
			d[i] = pt(a[i - 1], d[i - 1].second + diff);
	}
	cout << d[n - 1].second << endl;

    return 0;
}
