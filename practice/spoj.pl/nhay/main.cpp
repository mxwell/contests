#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);

void kmp(const string &s) {
	int n = (int) s.size();
	vector<int> pi(n);
	pi[0] = 0;
	int k = 0;
	for (int i = 1; i < n; ++i) {
		while (k > 0 && s[i] != s[k])
			k = pi[k - 1];
		if (s[i] == s[k])
			++k;
		pi[i] = k;
	}
	k = 0;
	for (int i = 1; ; ++i) {
		int intc = getchar();
		if (intc == '\n' || intc == '\r' || intc == EOF)
			break;
		char c = (char) intc;
		while (k > 0 && (k >= n || c != s[k])) {
			k = pi[k - 1];
		}
		if (c == s[k])
			++k;
		if (k == n)
			printf("%d\n", i - n);
	}
	pi.clear();
}

int main() {
	int n;
	bool first = false;
	while (scanf("%d\n", &n) == 1) {
		if (first)
			puts("");
		else
			first = true;
		string needle;
		needle.resize(n);
		getline(cin, needle);
		kmp(needle);
		needle.clear();
	}
    return 0;
}
