#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cassert>
#include <memory.h>
#include <ctype.h>
  
#include <iostream>
  
#include <string>
#include <complex>
#include <numeric>
#include <algorithm>
#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <sstream>
  
using namespace std;
  
template<typename TYPE> inline TYPE sqr(const TYPE& a) { return (a) * (a); }
  
#define forn(i, n) for(int i = 0; i < int(n); ++i)
#define for1(i, n) for(int i = 1; i <= int(n); ++i)
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()
#define correct(x, y, n, m) (0 <= (x) && (x) < (n) && 0 <= (y) && (y) < (m))
  
typedef long long li;
typedef long double ld;
typedef pair<int, int> pt;
  
const int INF = 1000 * 1000 * 1000;
const ld EPS = 1e-9;
const ld PI = 2 * acos(0.0);
const int N = 100;

vector<int> to_vector(const string &s, int from, int to) {
	vector<int> v;
	for (int i = to - 1; i >= from; --i)
		v.pb(s[i] - '0');
	return v;
}

void add_number(int spaces, char sign, const vector<int> &number) {
	string s;
	forn (i, spaces)
		s.pb(' ');
	if (sign > 0)
		s.pb(sign);
	for (int i = number.size() - 1; i >= 0; --i)
		s.pb(number[i] + '0');
	printf("%s\n", s.c_str());
}

void add_line(int spaces, int dashes) {
	string s;
	forn (i, spaces)
		s.pb(' ');
	forn (i, dashes)
		s.pb('-');
	printf("%s\n", s.c_str());
}

void process_sum(const string &in, int sign) {
	vector<int> a = to_vector(in, 0, sign);
	vector<int> b = to_vector(in, sign + 1, in.size());
	vector<int> c;
	size_t n = max(a.size(), b.size());
	int r = 0;
	for (size_t i = 0; i < n || r; ++i) {
		if (i < a.size())
			r += a[i];
		if (i < b.size())
			r += b[i];
		c.pb(r % 10);
		r /= 10;
	}
	size_t wid = max(max(a.size(), b.size() + 1), c.size());
	add_number(wid - a.size(), 0, a);
	add_number(wid - b.size() - 1, '+', b);
	add_line(0, wid);
	add_number(wid - c.size(), 0, c);
}

void remove_zeros(vector<int> &c) {
	int lz = c.size() - 1;
	for (; lz >= 0; --lz)
		if (c[lz] != 0)
			break;
	lz = max(lz, 0);
	c.resize(lz + 1);
}

void process_sub(const string &in, int sign) {
	vector<int> a = to_vector(in, 0, sign);
	vector<int> b = to_vector(in, sign + 1, in.size());
	vector<int> c;
	int r = 0;
	int n = a.size();
	forn (i, n) {
		int rnext = 0;
		r += a[i];
		if (i < int(b.size()))
			r -= b[i];
		while (r < 0) {
			r += 10;
			--rnext;
		}
		c.pb(r);
		r = rnext;
	}
	remove_zeros(c);	
	int wid = max(max(a.size(), b.size() + 1), c.size());
	add_number(wid - a.size(), 0, a);
	add_number(wid - b.size() - 1, '-', b);
	int dashes = max(b.size() + 1, c.size());
	add_line(wid - dashes, dashes);
	add_number(wid - c.size(), 0, c);
}

void process_mul(const string &in, int sign) {
	vector<int> a = to_vector(in, 0, sign);
	vector<int> b = to_vector(in, sign + 1, in.size());
	vector<int> c;
	vector<vector<int> > d(b.size());
	size_t n = 0;
	for (size_t i = 0; i < b.size(); ++i) {
		int m = b[i];
		int r = 0;
		for (size_t j = 0; j < a.size() || r; ++j) {
			if (j < a.size())
				r += m * a[j];
			d[i].pb(r % 10);
			r /= 10;
		}
		remove_zeros(d[i]);
		//printf("d[%d].size = %d\n", (int) i, (int) d[i].size());
		n = max(n, d[i].size() + i);
	}
	int r = 0;
	for (size_t i = 0; i < n || r; ++i) {
		for (size_t j = 0; j <= i && j < d.size(); ++j) {
			size_t offset = i - j;
			if (offset < d[j].size()) {
				r += d[j][offset];
			}
		}
		//printf("pos %d:\t%d\n", (int) i, r % 10);
		c.pb(r % 10);
		r /= 10;
	}
	size_t wid = max(a.size(), b.size() + 1);
	wid = max(wid, max(n, c.size()));
	add_number(wid - a.size(), 0, a);
	add_number(wid - b.size() - 1, '*', b);
	size_t dashes = max(b.size() + 1, d[0].size());
	add_line(wid - dashes, dashes);
	forn (i, d.size())
		add_number(wid - i - d[i].size(), 0, d[i]);
	if (d.size() < 2)
		return;
	dashes = c.size();
	assert(dashes >= d[d.size() - 1].size());
	add_line(wid - dashes, dashes);
	add_number(wid - c.size(), 0, c);
}

int main() {
	int n;
	scanf("%d\n", &n);
	forn (i, n) {
		string s;
		getline(cin, s);
		size_t sign;
		if ((sign = s.find('+')) != string::npos)
			process_sum(s, sign);
		else if ((sign = s.find('-')) != string::npos)
			process_sub(s, sign);
		else if ((sign = s.find('*')) != string::npos)
			process_mul(s, sign);
		puts("");
	}  
    return 0;
}
